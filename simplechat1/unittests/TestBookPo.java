package Gui;

import junit.framework.Assert;
import junit.framework.TestCase;


	public class TestBookPo extends TestCase {
	    //�
	    public void testSimplehist1() {
	        ShowHist result= new ShowHist("winnie the pooh", "2010"); 
	        ShowHist expected= new ShowHist("winnie the pooh", "2010");
	       Assert.assertTrue(expected.equals(result));    
	    }
	    public void testEquals() {
	        ShowHist sh1= new ShowHist("Revenge", "2011");    
	        Assert.assertTrue(!sh1.equals(null));
	        Assert.assertEquals(sh1, new ShowHist("Revenge", "2011"));
	        Assert.assertTrue(sh1.equals(sh1));
	        
	    }

	    public void testNotEquals() {
	        ShowHist sh1= new ShowHist("Harry Potter and the Order of the Phoenix", "2011");  	   
	        ShowHist sh2= new ShowHist("The Chronicles of Narnia", "2009");  
	        ShowHist sh3= new ShowHist("Harry Potter and the Goblet of Fire", "2010");  
	        ShowHist sh4= new ShowHist("Throwing the elephant ", "2008");  
	        Assert.assertTrue(!sh1.equals(sh2));
	        Assert.assertTrue(!sh1.equals(sh3));
	        Assert.assertTrue(!sh1.equals(sh4));
	        Assert.assertTrue(!sh3.equals(sh4));
	        Assert.assertTrue(!sh2.equals(sh3));
	        
	    }  
	    
	    
	    
	}

	
