
package client;

public class User
{
	static private String Username; // Username
	static private String Password; //password
	static private String First_name; //First Name
	static private String Last_name; //Last Name
	static private String Address; // Address 
	static private int Online; // Online?
	static private int priv; //privilege
	static private String Birthdate; // Birth date
	static private String Host; // host
    static private int AccountType; // Account Type
	static private int msg;
    
    public User(){
    	
    }
    
    public User(String Username,String Password,
    		String First_name,String Last_name,
    		String Address,int Online,
    		int priv,String Birthdate,String Host)
    	{	
        this.Username = Username;
        this.Password = Password; 
        this.First_name=First_name;
        this.Last_name=Last_name;
        this.Address=Address;
        this.Online = Online;
        this.priv = priv;
        this.Birthdate = Birthdate;
    	this.Host = Host;
    	} 
    public User(int message){
    	this.msg = message;
    }
    
    
    
    /* Getters */
	public String getUser() {
		return Username;
		}
	
	public String getPassword() {
	    return Password;
		}
  
	public int getMsg() {
	    return msg;
		}
	
	public int getOnline() {
		return Online;
		}
	
    public int getPriv() {
		return priv;
	}
    public String getHost(){
    	return Host;
    }
    public int getAccount(){
    	return AccountType;
    }
    public String getFirst_name() {
		return First_name;
		}
	
	public String getLast_name() {
	    return Last_name;
		}
		    public void setUser(String u)
    {
    	Username = u;
    }
    public void setPass(String p)
    {
    	Password = p;
    }
    
} // end class User