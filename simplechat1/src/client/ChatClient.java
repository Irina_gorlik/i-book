// This file contains material supporting section 3.7 of the textbook:
// "Object Oriented Software Engineering" and is issued under the open-source
// license found at www.lloseng.com 

package client;
import ocsf.client.*;
import common.*;
import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import ocsf.client.*;
import Gui.*;
import common.*;
import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.Serializable;
import javax.swing.JOptionPane;
import java.net.*;

import logic.*;

/**
 * This class overrides some of the methods defined in the abstract
 * superclass in order to give more functionality to the client.
 *
 * @author Dr Timothy C. Lethbridge
 * @author Dr Robert Lagani&egrave;
 * @author Fran&ccedil;ois B&eacute;langer
 * @version July 2000
 */
public class ChatClient extends AbstractClient
{
  //Instance variables **********************************************
  
  /**
   * The interface type variable.  It allows the implementation of 
   * the display method in the client.
   */
  ChatIF clientUI; 
  String Host;
  
  //Constructors ****************************************************
  
  /**
   * Constructs an instance of the chat client.
   *
   * @param host The server to connect to.
   * @param port The port number to connect on.
   * @param clientUI The interface type variable.
   */
  
  public ChatClient(String host, int port, ChatIF clientUI)
    throws IOException  
  {
    super(host, port); //Call the superclass constructor
    this.clientUI = clientUI;
    this.Host = host;
    
    

    openConnection();
  }

  
  //Instance methods ************************************************
    
  /**
   * This method handles all data that comes in from the server.
   *
   * @param msg The message from the server.
   */
  public void handleMessageFromServer(Object msg) 
  {
	 int i;
	
	 //String[] msg2 = (String[])msg;
	 int err=0;
     User u = new User();
	 //System.out.println("obj send from server");
	 
	 /* vitali */
	  //System.out.println("obj send from server");
	 if (msg instanceof ArrayList)
	 {   ArrayList arr = (ArrayList)msg;
        if (arr.get(0).equals("Download FILE"))	 
		 {
		 System.out.println("*************************** picture Client");
		 
		 int bytesRead;
		 int current = 0;
		 try { 
		 byte [] mybytearray  = new byte [Integer.parseInt(arr.get(1).toString())];
		                  InputStream is =new ByteArrayInputStream( (byte[]) arr.get(2));
		     FileOutputStream fos;
			
			 fos = new FileOutputStream(arr.get(3).toString());
			
		     BufferedOutputStream bos = new BufferedOutputStream(fos);
		     bytesRead = is.read(mybytearray,0,mybytearray.length);
		     current = bytesRead;

		     // thanks to A. for the bug fix
		     do {
		        bytesRead =
		           is.read(mybytearray, current, (mybytearray.length-current));
		        if(bytesRead >= 0) current += bytesRead;
		     } while(bytesRead > -1);

		     bos.write(mybytearray, 0 , current);
		     bos.flush();
		     
		     bos.close();
		     
		     if (arr.get(4).equals("pic"))
		     {
                 System.out.println("*************>"+arr.get(3).toString());
		    	 File file = new File(arr.get(3).toString());
		    	 
		    	 File dir = new File("c:"+File.separator+"Pic"+File.separator);
                 dir.mkdir();
					// Move file to new directory
					boolean success = file.renameTo(new File(dir, file.getName()));
		     }
		     
     	     MainWindow mm = new MainWindow();
		     
		 } catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

 catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    
    BookBuy showBuy = new BookBuy();
    
		 }
	 }
	 else
	 {


	  String[] msg2 = (String[])msg;
if(msg2[0].equals("search Book")){
    	 
    	 System.out.println("ChatClient: search Book");
    	 
    	 
    	 
    	 if(msg2[1].equals("end1")){
    		 Book b = new Book();
    		// b.setResults(new String("no results"),0);
    		 b.noResults();
    		 SearchBook sb = new SearchBook();
    	 }//if msg2[1] null : no results
    	 
    	 else{
    		 Book b = new Book();
    		 b.setResults(msg2);
    		
    		 SearchBook sb = new SearchBook();
    	 }// end else
     
     
     }// end if search reviews
    if(msg2[0].equals("show BookAll")){     //if Show BOOK
 	System.out.println("Chat Client Show Book : isbn= " + msg2[1]);
 	
 	 Book b = new Book(msg2);
// 	 ShowReview sr = new ShowReview();
 	 MainWindow mm = new MainWindow();
  }
    if(msg2[0].equals("AdSearch Book")){
   	 
   	 System.out.println("ChatClient: Adsearch Book");
   	 
   	 
   	 
   	 if(msg2[1].equals("end1")){
   		 Book b = new Book();
   		// b.setResults(new String("no results"),0);
   		 b.noResults();
   		 AdvanceSearch asb = new AdvanceSearch();
   	 }//if msg2[1] null : no results
   	 
   	 else{
   		 Book b = new Book();
   		 b.setResults(msg2);
   		
  		 AdvanceSearch asb = new AdvanceSearch();
   	 }// end else
    
    
    }// end if search reviews   show BookAll
   
   if(msg2[0].equals("BuyBook ShowAccounts")){     //if Show BOOK
		System.out.println("Chat Client BuyBook ShowAccounts : Accounts = " + msg2[1]);
		 
		 Accounts a = new Accounts(msg2);
		
     	 MainWindow mm = new MainWindow();
		 }
	 
   
   
   if(msg2[0].equals("MngShow Account")){     //if Show BOOK
	 	System.out.println("Chat Client MngShow Account : Users = " + msg2[1]);
	 	 if(msg2[1].equals("end1")){
	 		AccountM amm = new AccountM();
   		// b.setResults(new String("no results"),0);
   		 amm.noResults();
   		AccountMng am = new AccountMng();
   		
   	 }//if msg2[1] null : no results
   	 
   	 else{
	 	 AccountM amm = new AccountM();
	 	 amm.setResults(msg2);
	 	AccountMng am = new AccountMng();
   	 }


	  }
   if(msg2[0].equals("Has Account")){     //if has account 
	 	
	 	AccountM amm = new AccountM();
 		amm.setAct((String)msg2[1]);
 		AccountMng am = new AccountMng();
 	  }
   if(msg2[0].equals("Search reviews isbn back"))         
   {

		 if(msg2[1].equals(null)||msg2[1].equals("end1")){
			 System.out.println("***************************");
  		 Review r = new Review();
  		 r.setResults("no results",0);
           BookShowReview showR = new BookShowReview();
  	 }//if msg2[1] null : no results
  	 
  	 else{
  		 Review r = new Review();
  		 r.setResults(msg2);
  		 BookShowReview showR = new BookShowReview();
  	 }// end else
   
  	 
   }// end if Search reviews isbn	
	 /* end vitali */

/**
      **************************** Manage Book *********************************
      */
     if(msg2[0].equals("***Uplaod File***"))
     {        
    	 if(msg2[1] == null)
    	 { // if wasn't successful
    		 Errors error= new Errors();
    		 error.setConfirmErr(1); // set confirm error
    	 }
    	 JOptionPane.showMessageDialog(null,"File Uplaoded","Updated",JOptionPane.CLOSED_OPTION);
    	 ManageBooks mb = new ManageBooks();
    }
     if(msg2[0].equals("***save book***"))
     {        
    	 if(msg2[1] == null)
    	 { // if wasn't successful
    		 Errors error= new Errors();
    		 error.setConfirmErr(1); // set confirm error
    	 }
    	 ManageBooks mb = new ManageBooks();
    }
     if(msg2[0].equals("***delete book***"))
     {        
    	 if(msg2[1] == null)
    	 { // if wasn't successful
    		 Errors error= new Errors();
    		 error.setConfirmErr(1); // set confirm error
    	 }
    	 ManageBooks mb = new ManageBooks();
    }
     if(msg2[0].equals("***add book author***"))
     {        
    	 if(msg2[1].equals("Author Exists"))
    	 { // if wasn't successful
    		 Errors error= new Errors();
    		 error.setConfirmErr(1); // set confirm error
    	 }
    	 ManageBooks mb = new ManageBooks();
    }
     if(msg2[0].equals("***remove book author***"))
     {        
    	 if(msg2[1].equals("Author Not Deleted"))
    	 { // if wasn't successful
    		 Errors error= new Errors();
    		 error.setConfirmErr(1); // set confirm error
    	 }
    	 ManageBooks mb = new ManageBooks();
    }
     if(msg2[0].equals("***add book field***"))
     {        
    	 if((msg2[1].equals("No field code found")) || (msg2[1].equals("Field Exists")))
    	 { // if wasn't successful
    		 Errors error= new Errors();
    		 error.setConfirmErr(1); // set confirm error
    	 }
    	 ManageBooks mb = new ManageBooks();
    }
     if(msg2[0].equals("***remove book field***"))
     {        
    	 if((msg2[1].equals("field Not Deleted")) || (msg2[1].equals("No field code found")))
    	 { // if wasn't successful
    		 Errors error= new Errors();
    		 error.setConfirmErr(1); // set confirm error
    	 }
    	 ManageBooks mb = new ManageBooks();
    }
     if(msg2[0].equals("new book"))
     {        
    	 if(msg2[1].equals("can't create book"))
    	 { // if wasn't successful
    		 Errors error= new Errors();
    		 error.setConfirmErr(1); // set confirm error
    	 }
    	 bookInfo b=new bookInfo();    	
    	 b.setIsbn(msg2[1]);
    	 ManageBooks mb = new ManageBooks();
    }
     if(msg2[0].equals("get book field list"))
     {        
    	 i=1;
    	 if(msg2[1].equals("no book fields found"))
    	 { // if wasn't successful
    		 Errors error= new Errors();
    		 error.setConfirmErr(1); // set confirm error
    	 }
    	 book_field bf = new book_field();    	
    	 while (!msg2[i].equals("***end***"))    	
    	 {
    		bf.addBookField(msg2[i++]);     		
    	 }
    	 System.out.println("Rerieving Fields");
    	 ManageBooks mb = new ManageBooks();
    }
     if(msg2[0].equals("search manage book"))
     {
    	 System.out.println("ChatClient: search manage book");
    	 /**
    	  * Handling no results
    	  */
    	 if(msg2[1].equals(null)){
    		 bookInfo b = new bookInfo();
    		 b.setResults("no results",0);
    		 ManageBooks mb = new ManageBooks();
    	 }//if msg2[1] null : no results
    	 else
    	 {
    		 bookInfo b = new bookInfo();
    		 b.setResults(msg2);
    		 ManageBooks mb = new ManageBooks();
    	 }// end else
     }//end search manage book
     /**
      ********************** Manage Book Part End ******************************
      */
     /**
      ********************** Manage Catalog Start******************************
      */
     if(msg2[0].equals("***get book catalog info***"))
     {
    	 System.out.println("ChatClient: get book catalog info started");
    	 System.out.println("catalog DATA :" + msg2[0] + " " + msg2[1]);
    	 /**
    	  * Handling no results
    	  */
    	 if((msg2[1].equals(null)) || (msg2[1].equals("***Book Not In Catalog***")))
    	 {
    		 CatalogBook cb = new CatalogBook();
    		 cb.setCatalogResults("no results",0);
    		 cb.setInCatalog(false);
    		 ManageBooks mb = new ManageBooks();
    	 }//if msg2[1] null : no results
    	 else
    	 {
    		 CatalogBook cb = new CatalogBook();
    		 cb.setCatalogResults(msg2);
    		 cb.setInCatalog(true);
    		 ManageBooks mb = new ManageBooks();
    	 }// end else
     }
     if(msg2[0].equals("***get catalog field list***"))
     {        
	   	 i=1;
	   	 if(msg2[1].equals("no catalog fields found"))
	   	 { // if wasn't successful
	   		 Errors error= new Errors();
	   		 error.setConfirmErr(1); // set confirm error
	   	 }
	   	 CatalogList cl = new CatalogList();    	
	   	 while (!msg2[i].equals("***end***"))    	
	   	 {
	   		cl.addCataloglist(msg2[i++]);     		
	   	 }
	   	 System.out.println("Rerieving catalog Fields");
	   	 ManageBooks mb = new ManageBooks();
      }
     if(msg2[0].equals("***add book catalog field***"))
     {        
    	 if((msg2[1].equals("No field code found")) || (msg2[1].equals("Field Exists") || (msg2[1] == null)))
    	 { // if wasn't successful
    		 Errors error= new Errors();
    		 error.setConfirmErr(1); // set confirm error
    		 try
    		 {
    			 JOptionPane.showMessageDialog(null,msg2[1],"Can't Update DB",JOptionPane.CLOSED_OPTION);
    		 }catch (NullPointerException ex)
    		 {
    			 JOptionPane.showMessageDialog(null,"NULL YOU IDIOT!!!","Can't Update DB",JOptionPane.CLOSED_OPTION);
    		 }
    	 }
    	 ManageBooks mb = new ManageBooks();
    }
     if(msg2[0].equals("***remove book from catalog***"))
     {        
    	 if(msg2[1] == null)
    	 { // if wasn't successful
    		 Errors error= new Errors();
    		 error.setConfirmErr(1); // set confirm error
    	 }
    	 ManageBooks mb = new ManageBooks();
    }
     
     if(msg2[0].equals("***remove catalog book field***"))
     {        
    	 if((msg2[1].equals("field Not Deleted")) || (msg2[1].equals("No field code found")) || (msg2[1] == null))
    	 { // if wasn't successful
    		 Errors error= new Errors();
    		 error.setConfirmErr(1); // set confirm error
    	 }
    	 ManageBooks mb = new ManageBooks();
    }
     
     if(msg2[0].equals("***get catalog subject list***"))
     {        
    	 i=1;
	   	 if(msg2[1].equals("no catalog subjects found"))
	   	 { // if wasn't successful
	   		 Errors error= new Errors();
	   		 error.setConfirmErr(1); // set confirm error
	   	 }
	   	 CatalogList cl = new CatalogList();    	
	   	 while (!msg2[i].equals("***end***"))    	
	   	 {
	   		cl.addCataloglist(msg2[i++]);     		
	   	 }
	   	 System.out.println("Rerieving catalog Subjects");
	   	 ManageBooks mb = new ManageBooks();
    }
     if(msg2[0].equals("***add catalog book subject***"))
     {        
    	 if((msg2[1].equals("No subject code found")) || (msg2[1].equals("subject Exists") || (msg2[1] == null)))
    	 { // if wasn't successful
    		 Errors error= new Errors();
    		 error.setConfirmErr(1); // set confirm error
    		 try
    		 {
    			 JOptionPane.showMessageDialog(null,msg2[1],"Can't Update DB",JOptionPane.CLOSED_OPTION);
    		 }catch (NullPointerException ex)
    		 {
    			 JOptionPane.showMessageDialog(null,"Subject NULL, YOU IDIOT!!!","Can't Update DB",JOptionPane.CLOSED_OPTION);
    		 }
    	 }
    	 ManageBooks mb = new ManageBooks();
    }
     if(msg2[0].equals("***remove catalog book subject***"))
     {        
    	 if((msg2[1].equals("subject Not Deleted")) || (msg2[1].equals("No subject code found")) || (msg2[1] == null))
    	 { // if wasn't successful
    		 Errors error= new Errors();
    		 error.setConfirmErr(1); // set confirm error
    	 }
    	 ManageBooks mb = new ManageBooks();
    }
     /**
      ********************** Manage Catalog End ******************************
      */
     /**
      * *************** Account Settings Start ********************************
      */
     if(msg2[0].equals("checkAcc"))
     {
    	 AccountStg as = new AccountStg();
    	// System.out.println(msg2[1]);
    	 if(msg2[1].equals("empty"))
    		 as.Available(true);
    	 else
    		 as.Available(false);

     }// end if check
     if(msg2[0].equals("***update user name***"))
     {        
    	 if(msg2[1] == null)
    	 { // if wasn't successful
    		 Errors error= new Errors();
    		 error.setConfirmErr(1); // set confirm error
    	 }
    	 AccountStg as = new AccountStg();
    }
     if(msg2[0].equals("***update user pass***"))
     {        
    	 if(msg2[1] == null)
    	 { // if wasn't successful
    		 Errors error= new Errors();
    		 error.setConfirmErr(1); // set confirm error
    	 }
    	 AccountStg as = new AccountStg();
    }
     if(msg2[0].equals("*** check Account State ***"))
     {
    	 AccountStg as = new AccountStg();
    	// System.out.println(msg2[1]);
    	 if(msg2[1].equals("empty"))
    		 as.validateAcount(true);
    	 else
    		 as.validateAcount(false);

     }// end if check
     
     if(msg2[0].equals("***create account***"))
     {        
    	 if((msg2[1].equals("User Have Account"))  || (msg2[1] == null))
    	 { // if wasn't successful
    		 Errors error= new Errors();
    		 error.setConfirmErr(1); // set confirm error
    		 try
    		 {
    			 JOptionPane.showMessageDialog(null,msg2[1],"Can't Update DB",JOptionPane.CLOSED_OPTION);
    		 }catch (NullPointerException ex)
    		 {
    			 JOptionPane.showMessageDialog(null,"Subject NULL, YOU IDIOT!!!","Can't Update DB",JOptionPane.CLOSED_OPTION);
    		 }
    	 }
    	 AccountStg as = new AccountStg();
    }
     /**
      * *************** Account Settings End********************************
      */


	if(msg2[0].equals("login"))         
     {

    	 if(msg2[1].equals("not found"))
			{
			err=1;
			Login L=new Login();
			L.error(err);
			}
    		//System.out.println("from chatclient:"+err);
    	 	//	System.out.println(msg2[1]);
    		else{
    			User user=new User(msg2[1],msg2[2],msg2[3],msg2[4],msg2[5],Integer.parseInt(msg2[6]),Integer.parseInt(msg2[7]),msg2[8],Host);
    			Login L=new Login(user,"localhost");
    		//System.out.println("chatclient: right username  " + msg2[1] );
    		}// end else
    	 
    	 
     }// end if login
     
     if(msg2[0].equals("AddUser"))
     {
    //	 System.out.println(+msg2[1]);
    	if(msg2[1].equals(null)) // failed to create account
    	{// if2
    		AddUser add1 = new AddUser(); 
        	add1.sendform(false);
    	}// end if2
    	else{
    	User user=new User(msg2[2],msg2[3],msg2[4],msg2[5],msg2[6],0,1,msg2[9],Host);
    	AddUser add1 = new AddUser(); // new account created
    	add1.sendform(true);
    	}// end else
     }

     
     if(msg2[0].equals("check"))
     {
    	 AddUser add = new AddUser();
    	// System.out.println(msg2[1]);
    	 if(msg2[1].equals("empty"))
    		 add.Available(true);
    	 else
    		 add.Available(false);

     }// end if check

     if(msg2[0].equals("new review")){
    //	 System.out.println(msg2[1]+msg2[2]);    	 
    	
    	 if(msg2[1].equals("0")){
    	//	 System.out.println("chat client: no reviews");    	 
    	Review r = new Review("","","","",0,"",0);
    	ManageReviews m=new ManageReviews();
    	 }// end if
    	 else{
    	 Review r = new Review(msg2[1],msg2[2],msg2[3],msg2[4],Integer.parseInt(msg2[5]),msg2[6],Integer.parseInt(msg2[7]));
    	 ManageReviews m=new ManageReviews();
     
    	 }// end else
     }// end new review
     
 
     
     
     if(msg2[0].equals("check review")){
     Review r = new Review();
     r.setLeft(Integer.parseInt(msg2[1]));
     ManageReviews m=new ManageReviews();	 
    	 
     }// end if check review

     if(msg2[0].equals("confirm review")){
     
    	 if(!msg2[5].equals("success")){ // if wasn't successful
    		 Errors error= new Errors();
    		 error.setConfirmErr(1); // set confirm error
    	 }
    	 ManageReviews m=new ManageReviews();
     
     }// end confirm review
     
     if(msg2[0].equals("search reviews")){
    	 
    	 System.out.println("ChatClient: search reviews");
    	 
    	 
    	 
    	 if(msg2[1].equals(null)){
    		 Review r = new Review();
    		 r.setResults("no results",0);
    		 SearchReview sr = new SearchReview();
    	 }//if msg2[1] null : no results
    	 
    	 else{
    		 int index3;
    		 Review r = new Review();
    		 r.setResults(msg2);
    		 SearchReview sr = new SearchReview();
    	 }// end else
     
     
     }// end if search reviews
     
     
     if(msg2[0].equals("show review")){
    	System.out.println("Chat Client Show Review: isbn= "+msg2[1]);
    	 Review r = new Review(msg2[1],msg2[2],msg2[3],msg2[4],1,msg2[6],Integer.parseInt(msg2[5]));
//    	 ShowReview sr = new ShowReview();
    	 MainWindow mm = new MainWindow();
     }// end show review
    	 
    if(msg2[0].equals("get users")){
    	int index2=0;
    	int index=1;
    	

    	for(index2=0 ; !msg2[index2].equals("null") ; index2++);
    	String temp[] = new String[index2]; // set size of temp as length of users
    	for(index=1 ; !msg2[index].equals("null") ; index++){
    		temp[index-1] = msg2[index];

    	}// end for
    	
    	Orders o = new Orders(temp);
    	MainWindow mm = new MainWindow();
     
     }// end get users
    

    if(msg2[0].equals("order")){
    	int index2=0;
    	int index=1;

    	for(index2=0 ; !msg2[index2].equals("null") ; index2++);
    	String temp[] = new String[index2]; // set size of temp as length of users
    	
    	for(index=1 ; !msg2[index].equals("null") ; index++){
    		temp[index-1] = msg2[index];
    		System.out.println("Chat Client"+temp[index-1]);

    	}// end for
    	

    	Orders o = new Orders();
    	o.setOrders(temp);
    	
    	OrderList order = new OrderList();


    }// end get orders
    
    
    if(msg2[0].equals("get books")){
    	int index2=0;
    	int index=1;
    	

    	for(index2=0 ; !msg2[index2].equals("null") ; index2++);
    	String temp[] = new String[index2]; // set size of temp as length of users
    	for(index=1 ; !msg2[index].equals("null") ; index++){
    		temp[index-1] = msg2[index];

    	}// end for
    	
    	Orders o = new Orders(temp,1);
    	MainWindow mm = new MainWindow();
     
     }// end get books
    
    if(msg2[0].equals("show book")){
    	System.out.println("1chat client:show book");
    	int index2=0;
    	int index=1;
    	

    	for(index2=0 ; !msg2[index2].equals("null") ; index2++);
    	String temp[] = new String[index2]; // set size of temp as length of users
    	for(index=1 ; !msg2[index].equals("null") ; index++){
    		temp[index-1] = msg2[index];
    		System.out.println("2chat client:show book"+temp[index-1]);
    	}// end for
    	
    	Orders o = new Orders(temp,1);
    	System.out.println("3chat client:show book:");
    	MainWindow mm = new MainWindow();
    
    }// end show book
    
	if(msg2[0].equals("get hist")){
		
	   	System.out.println("1chat client:get hist:msg2[0]:msg2[1]:"+msg2[0]+msg2[1]);
    	int index2=0;
    	int index=1;
    	
    	
    	for(index2=0 ; !msg2[index2].equals("null") ; index2++);
    	String temp[] = new String[index2]; // set size of temp as length of users

    	if(msg2[1].equals("zero")){
    		temp[0]="no data";
    	}//end if not suc
    	else{

 
        	for(index=1 ; index<index2 ; index++){
    	   
    	        		temp[index-1] = msg2[index];
    	        		System.out.println("3chat client:get hist"+temp[index-1]);
    	        	}// end for

    	}// end else
    	Orders o = new Orders(temp,1,1);
    	System.out.println("4chat client:get hist:");
    	MainWindow mm = new MainWindow();
		
		
		
	}// end get hist
	
	if(msg2[0].equals("check review1")){ // check review if exixst
		System.out.println("dsdfsdf"+msg2[1]);
		ShowRev showr = new ShowRev(msg2[1],1); //  update answer if yes or no
		MainWindow mm = new MainWindow();
	}// end check review1
	
	

    
    
    	if(msg2[0].equals("get books po")){
		   	int index2=0;
			int index=1;
			
		
			for(index2=0 ; !msg2[index2].equals("null") ; index2++);
			String temp[] = new String[index2]; // set size of temp as length of users
			for(index=1 ; !msg2[index].equals("null") ; index++){
				temp[index-1] = msg2[index];
		
			}// end for
			
			Orders o = new Orders(temp,2); // load books population info
			MainWindow mm = new MainWindow();
    	}// end get books po
    }// end else
  }// end handle...

  /**
   * This method handles all data coming from the UI            
   *
   * @param message The message from the UI.    
   */
  public void handleMessageFromClientUI(Object message)
  {
	

	  try
    {
		 
		 if(message instanceof obj)
		 { /* Transforming Object to String[] */
          String[] data = new String[20];
          int i;
          data[0]=((obj) message).getType();
          for(i=0;((obj) message).getParam(i)!=null;i++){
        	  data[i+1]=((obj) message).getParam(i);
          }
    	//  System.out.println(data[0]+data[1]);
          data[i+1]=null; // mark end of information transfered 
      //    System.out.println(data[0]+data[1]);
          sendToServer(data); // sending data to server
		 }
		if(message instanceof ArrayList)
		 {
			 sendToServer(message);
		 }
		 if(message instanceof String) 
			 sendToServer(message); 
		 if(message instanceof String[]) 
			 sendToServer(message); 

    }
    catch(IOException e)
    {
      clientUI.display
        ("Could not send message to server.  Terminating client.");
      quit();
    }
  }
  
  /**
   * This method terminates the client.
   */
  public void quit()
  {
    try
    {
      closeConnection();
    }
    catch(IOException e) {}
    System.exit(0);
  }
   

  
  
  
}
//End of ChatClient class
