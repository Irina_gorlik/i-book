package Gui;

import java.awt.Color;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Rectangle;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Rectangle;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;

import common.ChatIF;

import Gui.*;
import logic.*;
import client.*;



public class ManageReviews extends JPanel {
	  private JLabel jLabel666 = null;
	private javax.swing.JLabel jLabel1;
	public javax.swing.JButton jButton1;
    public javax.swing.JButton jButton2;
    public javax.swing.JButton jButton3;
    public javax.swing.JButton jButton4;
    public javax.swing.JButton jButton5;
    public javax.swing.JButton jButton6;
    public javax.swing.JButton jButton7;
    public javax.swing.JButton jButton8;
    User u = new User();
    Review r = new Review();
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton jButton11;
    public javax.swing.JButton jButton21;
    public javax.swing.JButton jButton31;
    public javax.swing.JButton jButton41;
    public javax.swing.JLabel jLabel11;
    public javax.swing.JLabel jLabel2;
    public javax.swing.JLabel jLabel21;
    public javax.swing.JLabel jLabel31;
    public javax.swing.JLabel jLabel41;
    public javax.swing.JLabel jLabel51;
    public javax.swing.JLabel jLabel3;
    public java.awt.TextArea textArea1;
    public javax.swing.JLabel jLabel61;
    public javax.swing.JButton jButton10;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JLabel jLabel14;
    
    
    public int count=0; // the first review to display
    public int first=1; // if its the first request for new review
    public static ChatClient client;
    private  ChatIF ChatIF = null;
    public int connected;
    // End of variables declaration//GEN-END:variables

    Color C=new java.awt.Color(153, 0, 0);
	
	/**
	 * This is the default constructor
	 */
	public ManageReviews() {
		super();
		initialize();
	}

	/**
	 * This method initializes StudentForm
	 */
	private void initialize() {
		
		
		
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
       
        
        jButton31 = new javax.swing.JButton();
        jLabel21 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        textArea1 = new java.awt.TextArea();
        jButton21 = new javax.swing.JButton();
        jButton11 = new javax.swing.JButton();
        jLabel41 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        jButton41 = new javax.swing.JButton();
        jLabel31 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel61 = new javax.swing.JLabel();
        jButton10 = new javax.swing.JButton();        
        jScrollPane1 = new javax.swing.JScrollPane();
        jLabel14 = new javax.swing.JLabel();
        
        
        
        
        this.setSize(1000, 700);
        this.setLayout(null);
        jButton1.setText("Search Book");
        jButton1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton1,null);
        jButton1.setBounds(30, 20, 140, 40);
		
        jButton2.setText("Search Review");
        jButton2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton2,null);
        jButton2.setBounds(30, 60, 140, 40);

        jButton3.setText("Manage Books");
        jButton3.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton3,null);
        jButton3.setBounds(30, 100, 140, 40);

        jButton4.setText("Manage Catalog");
        jButton4.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton4,null);
        jButton4.setBounds(30, 140, 140, 40);

        jButton5.setText("Manage Reviews");
        jButton5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 0, 0), 3));
        this.add(jButton5,null);
        jButton5.setBounds(30, 180, 140, 40);

        jButton6.setText("Logout");
        jButton6.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton6,null);
        jButton6.setBounds(30, 220, 140, 40);

        jButton7.setText("jButton7");
        jButton7.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton7,null);
        jButton7.setBounds(30, 260, 140, 40);

        jButton8.setText("Logout");
        jButton8.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton8,null);
        jButton8.setBounds(30, 300, 140, 40);	
		
		privinit();
		
		
    	/* Setup Connection to server */
        
	       connected = 1;
	   	    try  // trying to connect to server 
	   	    {
	   	    	 client= new ChatClient(u.getHost(),5555,ChatIF);
	   	    } // end try
	   	    catch(IOException exception)
	   	    {
	   	    	JOptionPane.showMessageDialog(null,"Error: Can't setup connection!","Error",JOptionPane.CLOSED_OPTION);
	   	    	connected = 0; // failed to connect
	   	    } // end catch
	        
	   	    /* end setup connection */
		
		


    	
		
		/* manage buttons and labels */	
  
        jLabel51.setFont(new java.awt.Font("Tahoma", 1, 18));
        jLabel51.setText("Reviews To Confirm");
        this.add(jLabel51);
        jLabel51.setBounds(450, 50, 300, 22);
		
        jLabel61.setText("Press Next To Get a New Review");
        add(jLabel61);
        jLabel61.setBounds(448, 107, 300, 22);	
        
        
        
        
        jLabel11.setText("From: ");
        add(jLabel11);
        jLabel11.setBounds(410, 150, 130, 14);	
        
        jLabel2.setText("Date: ");
        add(jLabel2);
        jLabel2.setBounds(540, 150, 150, 14);
        
        jLabel21.setText("Book Details:");
        add(jLabel21);
        jLabel21.setBounds(410, 180, 240, 14);
        
        jLabel31.setText("Title: ");
        add(jLabel31);
        jLabel31.setBounds(410, 210, 500, 14);     
        
        jLabel41.setText("ISBN: ");
        add(jLabel41);
        jLabel41.setBounds(410, 240, 250, 14);
        
        
        jLabel14.setText("Rating: ");
        add(jLabel14);
        jLabel14.setBounds(570,240,200,14);
        
        
        jButton41.setText("Prev");
        jButton41.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton41ActionPerformed(evt);
            }
        });

        add(jButton41);
        jButton41.setBounds(410, 270, 80, 23);
        
        jLabel3.setText("Review: ");
        add(jLabel3);
        jLabel3.setBounds(515, 270, 80, 23);
        
        
        this.jButton31.setText("Next");
        add(jButton31);
        jButton31.setBounds(601, 270, 80, 23);
        jButton31.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton31ActionPerformed(evt);
            }
        });
        
        
        add(textArea1);
        textArea1.setBounds(410, 300, 270, 180);

        
        
        

        jButton11.setText("Confirm");
        add(jButton11);
        jButton11.setBounds(430, 510, 90, 23);
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });
        
        
        
        jButton21.setText("Delete");
        add(jButton21);
        jButton21.setBounds(570, 510, 90, 23);        
        jButton21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton21ActionPerformed(evt);
            }
        });


		
		/* End manage*/
	
		/* Create Background */
		
        jLabel1 = new javax.swing.JLabel();
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Blue1.jpg"))); // NOI18N
        jLabel1.setText("jLabel1");
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
		/* end Background */
	} // end initialize

	void privinit(){
	   	 /* Reader */
	   	if(u.getPriv() == 0){  
	   		jButton3.setText("Account Settings");
	   		jButton4.setText("Logout");
	   		jButton5.setVisible(false);
	   		jButton6.setVisible(false);
	   		jButton7.setVisible(false);
	   		jButton8.setVisible(false);
	   	}
	   	/* Librarian */
	   	else if(u.getPriv() == 1){
	   		jButton7.setVisible(false);
	   		jButton8.setVisible(false);
	   	
	   	
	   	}
	   	/* Library Manager */
	   	/* Library Manager */
	   	else if(u.getPriv() == 2){
	   		jButton6.setText("Manage Reports");
	   		jButton7.setText("Logout");
	   		jButton8.setVisible(false);
	   	
	   	}
	   	/* System Manager */
	   	else if(u.getPriv() == 3){
	      		jButton6.setText("Manage Reports");
	   		jButton7.setText("Account Management");
	   		jButton8.setText("Logout");
	   	}
	}// end privinit
	
	public void getReview(int count){
		
		/* sql */
		
    	obj logdt = new obj();
    	logdt.setType("new review");
    	logdt.setParam(0,Integer.toString(count)); // number of review to display


   	    
   	    if(connected == 1)	
   	    	client.handleMessageFromClientUI(logdt);
    	
	    try {
            Thread.sleep(100); 
            } // end try
	    catch (InterruptedException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } // end catch

    	
    	/* end sql  */
		

	} // end getReview
	public int checkReview(){
		
		/* sql */
		
    	obj logdt = new obj();
    	logdt.setType("check review");

   	    if(connected == 1)	
   	    	client.handleMessageFromClientUI(logdt);
    	
	    try {
            Thread.sleep(100); 
            } // end try
	    catch (InterruptedException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } // end catch

    	
    	/* end sql  */
		Review r = new Review();
		System.out.println("getleft = "+r.getLeft());
		return r.getLeft();

	} // end getReview
	
	private void jButton31ActionPerformed(java.awt.event.ActionEvent evt) {// next button was pressed

		int num = checkReview();
	
		if(num==0) {// no reviews at all, show that to the user
		
	
			jLabel41.setText("ISBN: ");
			jLabel31.setText("Title: ");
			jLabel2.setText("Date: ");
			jLabel11.setText("From: ");
			jLabel14.setText("Rating: ");
			jLabel3.setText("Review: 0/0");
			jLabel3.setForeground(C);
			count=0;
		}// end if(r.getLeft()==0)
		else if(count<num){
			getReview(++count);
		
			jLabel3.setText("Review: "+count+"/"+r.getLeft());
			jLabel3.setForeground(Color.black);
			jLabel41.setText("ISBN: "+r.getIsbn());
			jLabel31.setText("Title: "+r.getTitle());
			jLabel2.setText("Date: "+r.getDate());
			textArea1.setText(r.getReview());
			jLabel11.setText("From: "+r.getUsername());
			jLabel14.setText("Rating: "+r.getRating());
		
		}// end if else
		
		
		
	/*	
		if(count<r.getLeft() || first == 1){ // continue only if we didnt reach the limit of reviews
			
		if(first==1){ // now its not the first time a user requests review
			this.first=0;
			System.out.println("1");
			getReview(1); // check if there is any reviews in
			System.out.println("2");
			//System.out.println("e. = "+e.getZeroReviews());
			if(r.getLeft()==0) {// no reviews at all, show that to the user
				jLabel3.setText("Review: 0/0");
				jLabel3.setForeground(C);
				System.out.println("3");
				count=0;
				}//end if(r.getLeft()==0)
		}// end if(first==1)
		System.out.println("4");
		System.out.println("4");
		if(first==0){
	
		getReview(1); // check if there is any reviews in
		System.out.println("4.5");
		if(r.getLeft()!=0){
			getReview(++count);
			jLabel3.setText("Review: "+count+"/"+r.getLeft());
			jLabel3.setForeground(Color.black);
			jLabel41.setText("ISBN: "+r.getIsbn());
			jLabel31.setText("Title: "+r.getTitle());
			jLabel2.setText("Date: "+r.getDate());
			textArea1.setText(r.getReview());
			jLabel11.setText("From: "+r.getUsername());
			jLabel14.setText("Rating: "+r.getRating());
			System.out.println("5");
			}// end if r.getLeft()
	
		if(r.getLeft()==0){// else there are no reviews, show that to the user
			jLabel3.setText("Review: 0/0");
			jLabel3.setForeground(C);
			count=0;
			System.out.println("6");
			}// end else if
		}// if first==0
		}//if(count<..)   
		*/
	}//end event next button
	
	private void jButton41ActionPerformed(java.awt.event.ActionEvent evt) {// prev button was pressed
        // TODO add your handling code here:
		if(count > 1){ // continue only if we didnt reach the limit of reviews	
		getReview(--count);
		
		jLabel3.setText("Review: "+count+"/"+r.getLeft());
		jLabel41.setText("ISBN: "+r.getIsbn());
		jLabel31.setText("Title: "+r.getTitle());
		jLabel2.setText("Date: "+r.getDate());
	    textArea1.setText(r.getReview());
		jLabel11.setText("From: "+r.getUsername());
		jLabel14.setText("Rating: "+r.getRating());
		}// end if
		
		
    }//end event prev button
	
	private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {// confirm button was pressed
        // TODO add your handling code here:

		
		if(count > 0){ // if any review is presented
		
			if(confirmReview(count,textArea1.getText())){// if the confirm was successful	
					count=0;// reset count
					jLabel41.setText("ISBN: ");
					jLabel31.setText("Title: ");
					jLabel2.setText("Date: ");
				    textArea1.setText("");
					jLabel11.setText("From: ");		
					jLabel3.setText("Review: ");
					jLabel14.setText("Rating: ");
			}// end if return from confirmReview
			
			else { // if the confirm was unsuccessful
				
				JOptionPane.showMessageDialog(null,"Unable To Confirm Review","ERROR",JOptionPane.ERROR_MESSAGE); // present error if so.
				
			}// end else

		}// end if (count...
		
		
    }//end event confirm button
	
	private void jButton21ActionPerformed(java.awt.event.ActionEvent evt) {// delete button was pressed
        // TODO add your handling code here:
		if(count > 0){ // continue only if we didnt reach the limit of reviews
			
		deleteReview(count);
		count=0; // reset count
		jLabel41.setText("ISBN: ");
		jLabel31.setText("Title: ");
		jLabel2.setText("Date: ");
	    textArea1.setText("");
		jLabel11.setText("From: ");
		jLabel3.setText("Review: ");
		jLabel14.setText("Rating: ");
		}// end if
		
		
    }//end event delete button

	
	private boolean confirmReview(int id,String review){
		
		/* sql */
		
    	obj logdt = new obj();
    	logdt.setType("confirm review");
    	logdt.setParam(0,Integer.toString(id)); // set number of review to confirm
    	logdt.setParam(1,review); // set review
    	logdt.setParam(2,r.getIsbn()); // get isbn
   	    if(connected == 1)	
   	    	client.handleMessageFromClientUI(logdt);
    	
	    try {
            Thread.sleep(100); 
            } // end try
	    catch (InterruptedException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } // end catch
    
    	/* end sql  */
		
	    Errors error= new Errors();
	    if(error.getConfirmErr()==1) // if there was an error in the process then return false
	    	return false;	
	    else
	    	return true; // else return true
	}// end confirmReview
	
	private void deleteReview(int id){
		
		/* sql */
		
    	obj logdt1 = new obj();
    	logdt1.setType("delete review");
    	logdt1.setParam(0,Integer.toString(id)); // set number of review to confirm
       	if(connected == 1)	
   	    	client.handleMessageFromClientUI(logdt1);
    	
	    try {
            Thread.sleep(1000); 
            } // end try
	    catch (InterruptedException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } // end catch
    
    	/* end sql  */
		
		
		
	}// end deleteReview
	
	
} // end Class
