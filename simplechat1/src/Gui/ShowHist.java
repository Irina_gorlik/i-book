package Gui;

import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.ScrollPane;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Rectangle;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.TableModel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Rectangle;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import java.util.EventListener;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.ListSelectionModel;
import common.ChatIF;

import Gui.*;
import logic.*;
import client.*;



public class ShowHist extends JPanel {
	  private JLabel jLabel666 = null;
	private javax.swing.JLabel jLabel1;
	public javax.swing.JButton jButton1;
    public javax.swing.JButton jButton2;
    public javax.swing.JButton jButton3;
    public javax.swing.JButton jButton4;
    public javax.swing.JButton jButton5;
    public javax.swing.JButton jButton6;
    public javax.swing.JButton jButton7;
    public javax.swing.JButton jButton8;
    public javax.swing.JLabel jLabel51; //big title
    public javax.swing.JLabel jLabel52; //choose user
    User u = new User();
    Orders o = new Orders();
//    public DrawHistApplet ap; //choose user
   public DrawHistApplet ap = null;
    public int starts=0;
    public int first=1; //  if first time
    
    public javax.swing.JTable jTable1;
    public javax.swing.JScrollPane jScrollPane1=null;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel2=null;
    private javax.swing.JLabel jLabel3=null;
    public javax.swing.table.DefaultTableModel model;
    public javax.swing.JScrollPane jScrollPane2=null;
    public javax.swing.JTable table1;
    public javax.swing.JTable table=null;
	public javax.swing.JButton Button1; // order list
    public javax.swing.JButton Button2; // book statistics
    public javax.swing.JButton Button3; // book popularity
    public javax.swing.JComboBox jComboBox1=null; // user choise
    public javax.swing.JComboBox jComboBox2=null; // year choise
    public String[] List= new String[200]; // for books
    public String[] List2= new String[200]; // for order counters
    
    public static ChatClient client;
    private  ChatIF ChatIF = null;
    public int connected=1;
    public int rows=0;
    public String str[] = new String[200];
    public String tmpStr = new String("");
    public String currentBook = new String();
    public String currentYear = new String();
    
    public String stri[] = new String[200];
    public String[] HistDownloads; // number of downloads from hist2
    public String[] HistOrders; // number if orders from hist1
    
 
    /**
	 * This is the default constructor
	 */
	public ShowHist() {
		super();
		initialize();
	}
	
	public ShowHist(String book,String year) {
		super();
		String[] tmp2 = new String[13];
		String[] tmp = new String[13];
		for(int i=0;i<13;i++) // reset tmp
			tmp[i]="0";
		String[] List2= new String[200]; // for order counters
		
		MainWindow mm = new MainWindow();
		mm.setCurrentBook(book);
		mm.setCurrentYear(year);
		mm.getHist();
		Orders order = new Orders();
		order =(Orders) mm.order;
		
		List2= (String[]) order.getCounter();
		
		
		if(!List2[0].equals("no data")){
		for(int i=0;i<List2.length-1;i=i+2){ //show tmp
			tmp[Integer.parseInt(List2[i])]=Integer.toString(Integer.parseInt(List2[i+1])*Integer.parseInt(List2[i]));
			System.out.println("tmp: :"+tmp[Integer.parseInt(List2[i])]);
		
			}//end for
		}// end if
		else{
			tmp[0]="no data";
		}// end else

		tmp2=(String [])tmp; // first histogram
	
		mm.getHist2(); 
		order =(Orders) mm.order;
		
		String[] List3 = new String[200];
		List3= (String[]) order.getCounter();
	
		System.out.println("mm: hist2222:List3[0]:"+List3[0]);
		if(!List3[0].equals("no data")){
		tmp[0]=List3[0];
		for(int i=0;i<List3.length-1;i=i+2){ //show tmp
			tmp[Integer.parseInt(List3[i])]=Integer.toString(Integer.parseInt(List3[i+1])*Integer.parseInt(List3[i]));
			System.out.println("tmp: :"+tmp[Integer.parseInt(List3[i])]);
		
			}//end for
		}// end if
		else{
			tmp[0]="no data";
		}// end else
		
		this.HistDownloads=(String[])tmp;
		this.HistOrders=(String[])tmp2;
	}// end constr

	public boolean equals(Object anObject) {
	    if (anObject instanceof ShowHist) {
	        ShowHist show= (ShowHist)anObject;
	       System.out.println("***********+ "+show.CheckD()[0]+"  "+CheckD()[0]);
	        if(Arrays.equals(CheckD(),show.CheckD())&& Arrays.equals(show.CheckO(),(CheckO())))
	        	return true;
	        
	    }
	    return false;
	}

	public String[] CheckD() {
		return HistDownloads;
	}

	public String[] CheckO() {
		return HistOrders;
	}
	
	/**
	 * This method initializes StudentForm
	 */
	private void initialize() {
		
	        
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jLabel51 = new javax.swing.JLabel();
        jLabel52 = new javax.swing.JLabel();
        Button1 = new javax.swing.JButton();
        Button2 = new javax.swing.JButton();
        Button3 = new javax.swing.JButton();

        
  

    
      this.add(getFacComboBox(), null);
      this.add(yearCombo(), null);

      
      /* histogram ScrollPane1 */
      
      	JLabel dummyLab = new JLabel("Connecting to the database. Please wait...");      
      	dummyLab.setVisible(true);
     
     	jScrollPane1 = new javax.swing.JScrollPane(dummyLab);   
    	add(jScrollPane1);
    	jScrollPane1.setVisible(true);
    	jScrollPane1.setBounds(200, 360, 340,250);	// set table size
    	jScrollPane1.setViewportView(new JLabel("asdfasdf"));

    	/* end histogram1 */
    	
        /* histogram ScrollPane2 */
        
      	JLabel dummyLab2 = new JLabel("Connecting to the database. Please wait...");      
      	dummyLab.setVisible(true);
     
     	jScrollPane2 = new javax.swing.JScrollPane(dummyLab);   
    	add(jScrollPane2);
    	jScrollPane2.setVisible(true);
    	jScrollPane2.setBounds(560, 360, 340,250);	// set table size
    	jScrollPane2.setViewportView(new JLabel("22"));

    	/* end histogram2 */
    	
    	this.add(getjLabel2(),null);
    	this.add(getjLabel3(),null);
    
    	
        this.setSize(1000, 700);
        this.setLayout(null);
        jButton1.setText("Search Book");
        jButton1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton1,null);
        jButton1.setBounds(30, 20, 140, 40);
		
        jButton2.setText("Search Review");
        jButton2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton2,null);
        jButton2.setBounds(30, 60, 140, 40);

        jButton3.setText("Manage Books");
        jButton3.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton3,null);
        jButton3.setBounds(30, 100, 140, 40);

        jButton4.setText("Manage Catalog");
        jButton4.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton4,null);
        jButton4.setBounds(30, 140, 140, 40);

        jButton5.setText("Manage Reviews");
        jButton5.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton5,null);
        jButton5.setBounds(30, 180, 140, 40);

        jButton6.setText("Logout");
        jButton6.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton6,null);
        jButton6.setBounds(30, 220, 140, 40);

        jButton7.setText("jButton7");
        jButton7.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton7,null);
        jButton7.setBounds(30, 260, 140, 40);

        jButton8.setText("Logout");
        jButton8.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton8,null);
        jButton8.setBounds(30, 300, 140, 40);	
		
        System.out.println("2");
        /*
         *  	Manage Reprts Buttons
         */
        
        jLabel51.setFont(new java.awt.Font("Tahoma", 1, 18));
        jLabel51.setText("Operational monitoring");
        this.add(jLabel51);
        jLabel51.setBounds(420, 35, 250, 22);
        
        
        Button1.setText("Orders List");
        Button1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(Button1);
        Button1.setBounds(350,95, 115, 28);
        
        Button2.setText("Book Statistics");
        Button2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 0, 0), 3));
        this.add(Button2);
        Button2.setBounds(465,95, 115, 28);
        
        Button3.setText("Book Popularity");
        Button3.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(Button3);
        Button3.setBounds(580,95, 115, 28);
        
        jLabel52.setFont(new java.awt.Font("Tahoma", 1, 14));
        jLabel52.setText("Book Orders ");
        this.add(jLabel52);
        jLabel52.setBounds(450, 230, 250, 22);
        jLabel52.setVisible(false);
//        this.add(getFacComboBox(), null);
//        this.add(yearCombo(), null);
        System.out.println("3");
        
		privinit();

		/* Create Background */
		
        jLabel1 = new javax.swing.JLabel();
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Blue1.jpg"))); // NOI18N
        jLabel1.setText("jLabel1");
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
		/* end Background */
	} // end initialize

	void privinit(){
	   	 /* Reader */
	   	if(u.getPriv() == 0){  
	   		jButton3.setText("Account Settings");
	   		jButton4.setText("Logout");
	   		jButton5.setVisible(false);
	   		jButton6.setVisible(false);
	   		jButton7.setVisible(false);
	   		jButton8.setVisible(false);
	   	}
	   	/* Librarian */
	   	else if(u.getPriv() == 1){
	   		jButton7.setVisible(false);
	   		jButton8.setVisible(false);
	   	
	   	
	   	}
	   	/* Library Manager */
	   	else if(u.getPriv() == 2){
	   		jButton6.setText("Manage Reports");
	   		jButton7.setText("Logout");
	   		jButton8.setVisible(false);
	   	
	   	}
	   	/* System Manager */
	   	else if(u.getPriv() == 3){
	      		jButton6.setText("Manage Reports");
	   		jButton7.setText("Account Management");
	   		jButton8.setText("Logout");
	   	}
	}// end privinit
	
	
	/**
	 * This method initializes facComboBox	
	 */
	private JComboBox getFacComboBox() {
		
		if (jComboBox1 == null) {
			jComboBox1 = new JComboBox();
					jComboBox1.setBounds(new Rectangle(280,175, 280, 27));
					jComboBox1.addItem("Choose a Book");
			jComboBox1.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					
					if(!jComboBox1.getSelectedItem().equals("Choose a Book")){
						currentBook=(String)jComboBox1.getSelectedItem();
						jComboBox2.setVisible(true);
					}
					else {
						jComboBox2.setVisible(false);
						jScrollPane1.setVisible(false);
						jScrollPane2.setVisible(false);
						jLabel2.setVisible(false);
						jLabel3.setVisible(false);
					}// end else 
				}
			});
		}// end if null
		return jComboBox1;
	}
	
	
	/**
	 * This method initializes yearCombo	
	 */
	private JComboBox yearCombo() {
		
		if (jComboBox2 == null) {
			jComboBox2 = new JComboBox();
			jComboBox2.setBounds(new Rectangle(600,175, 100, 27));
			jComboBox2.addItem("Year");
			jComboBox2.addItem("2007");
			jComboBox2.addItem("2008");
			jComboBox2.addItem("2009");
			jComboBox2.addItem("2010");
			jComboBox2.addItem("2011");
			jComboBox2.setVisible(false);
			
			
			jComboBox2.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Object[] objectRow = new Object[3];
					
					int i,j;
					int index=0;
					if(!jComboBox2.getSelectedItem().equals("Year")){
						currentYear=(String)jComboBox2.getSelectedItem();

						

					
					}// end if
				}
			});
		}
		return jComboBox2;
	}

	

	/*
	 * 		loads all books
	 */
	void loadBooks(Orders Or,String[] temp,int start){
		String[] tmp = new String[13];
		this.o=Or;
		this.List2=(String[]) temp;
		starts=start;
		if(temp[0].equals("no data")){
			String[] noData = new String[5];

			for(int i=0; i<5;i++)
				noData[i]="0";
		
			DrawHistApplet npp = new DrawHistApplet(noData);

			this.jScrollPane1.setViewportView(npp);
			if(start==0){
				this.jScrollPane1.setVisible(false);
				jLabel2.setVisible(false);
				jLabel3.setVisible(false);
			}
			else{
				this.jScrollPane1.setVisible(true);
				jLabel2.setVisible(true);
				jLabel3.setVisible(true);

			}
		}// end if
	
		else{
		DrawHistApplet pp = new DrawHistApplet(temp);
		this.jScrollPane1.setViewportView(pp);
		this.jScrollPane1.setVisible(true);
		jLabel2.setVisible(true);
		jLabel3.setVisible(true);
		}// end else
		

		this.List = (String[]) o.getBooks();

	if(this.first==1){
		for (int i = 0; i < List.length-1; i++) {
			this.first =0;
			jComboBox1.addItem((Object)List[i]);
		}// end for
	}// end if
		
		System.out.println("sh:loadBooks2");
	} // end loadBooks()
	
	/*
	 * 		loads Histogram2
	 */

	void loadHist2(String[] temp,int start){
		String[] tmp = new String[13];
	
		this.List2=(String[]) temp;
		System.out.println("load Hist2 : temp[0]"+temp[0]);
		starts=start;
		if(temp[0].equals("no data")){
			String[] noData = new String[5];

			for(int i=0; i<5;i++)
				noData[i]="0";
		
			DrawHistApplet npp = new DrawHistApplet(noData);

			this.jScrollPane2.setViewportView(npp);
			if(start==0)
				this.jScrollPane2.setVisible(false);
			else
				this.jScrollPane2.setVisible(true);
		}// end if
	
		else{
			temp[0]="0";
		DrawHistApplet pp = new DrawHistApplet(temp);
		this.jScrollPane2.setViewportView(pp);
		this.jScrollPane2.setVisible(true);
		}// end else
		


		
		
		System.out.println("sh:loadBooks2");
	} // end loadBooks()
	
	
	/** jLabel2  setup
	 * */
	private JLabel getjLabel2() {
		if(jLabel2==null){
			jLabel2 = new JLabel("Book Order Histogram");
			jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14));
			jLabel2.setBounds(new Rectangle(300,320, 200, 27));
//			jLabel2.setVisible(false);
		}
	return jLabel2;
	}// end jLabel2

	/** jLabel3  setup
	 * */
	private JLabel getjLabel3() {
		if(jLabel3==null){
		jLabel3 = new JLabel("Book Search Histogram");
		jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14));
		jLabel3.setBounds(new Rectangle(650,320, 200, 27));
//		jLabel3.setVisible(false);
		}
	return jLabel3;
	}// end jLabel2
	
	
} // end Class
