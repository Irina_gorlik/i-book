package Gui;


import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import java.awt.Window;
import client.ChatClient;
import common.ChatIF;
import javax.swing.table.TableModel;
import javax.swing.JTable;
import javax.swing.table.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Rectangle;
import javax.swing.JLabel;
import java.awt.Dimension;

import logic.*;
import client.*;
import Gui.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;




public class MainWindow extends JFrame {
/* vitali */
	public BookBuy showBuy;

	public BookShowReview showR;




/* end vitali*/



	public MainMenu mm;
	public SearchBook sb;
	public SearchReview sr;
	public AccountStg as;
	public ManageBooks mb;
	public ManageCatalog mc;
	public ManageReviews mr;
	public ManageReports mrp;
	public AccountMng am;
	public ShowReview shr;
	public ShowReview shr1;
	public WriteReview wr;
	public OrderList o;
	public BookSta bs;
	public BookPo bp;

	public Overview ov;
	public DrawHistApplet draw;
	public ShowHist sh;
	public ErrorWrite ew;
	public AdvanceSearch asb;
		private int form = 1; //1 = search 2 = advance search
	
	
	private JPanel FirstPanel = null;
	private JButton StudentsButton = null;
	private JLabel jLabel1 = null;  //  @jve:decl-index=0:visual-constraint="203,703" //background jlabel
	User u = new User();
	Orders order = new Orders();
	
	  private  ChatIF ChatIF = null;
	  public static ChatClient client;
	
	  public String isbn=null;
	  public String user=null;
	  public String currentYear=null; // for show hist
	  public String currentBook=null; // for show hist
	
	/**
	 * This is the default constructor
	 */
	public MainWindow() {
		super();
		initialize();
	}

	/**
	 * This method initializes AcademicFrame 
	 */
	private void initialize() {

		
		this.setSize(1000, 700);
		this.setContentPane(getFirstPanel());
		this.setTitle("Undefined");

		 
		this.addWindowListener(new java.awt.event.WindowAdapter() { // add to closing event log off user
		    public void windowClosing(WindowEvent winEvt) {
		      Logout();
		    }
		});
// *************************   vitaly
	      		this.showBuy = new BookBuy();  
				this.showR = new BookShowReview();
//***************************   vitaly
		this.mm=new MainMenu();
		this.sb=new SearchBook();
		this.sr=new SearchReview();
		this.as=new AccountStg();
		this.mb=new ManageBooks();
		this.mc=new ManageCatalog();
		this.mr=new ManageReviews();
		this.mrp=new ManageReports();
		this.am=new AccountMng();
		this.shr=new ShowReview();
		this.wr=new WriteReview();
		this.o=new OrderList();
		this.bs= new BookSta();
		this.bp= new BookPo();
		this.ov= new Overview();
		this.sh = new ShowHist();
		this.ew = new ErrorWrite();
			this.asb= new AdvanceSearch();
		
		setContentPane(mm);
		
		/* vitali */
				/* ActionListeners for JButtons in asr */
		asb.jButton1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				setContentPane(sb);
				sb.jButton9.setText("Advance Search");
				//setContentPane(sb); // do nothing if already in sb
			}});
		asb.jButton2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
					
				setContentPane(sr);
			}});
		asb.jButton3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:setContentPane(as);break;
	            case 1:setContentPane(mb);break;
	            case 2:setContentPane(mb);break;
	            case 3:setContentPane(mb);break;
	        } // end switch	
			}});
		asb.jButton4.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:Logout();break;
	            case 1:setContentPane(mc);break;
	            case 2:setContentPane(mc);break;
	            case 3:setContentPane(mc);break;
	        } // end switch	
			}});
		asb.jButton5.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
			            case 0:break;
			            case 1:setContentPane(mr);break;
			            case 2:setContentPane(mr);break;
			            case 3:setContentPane(mr);break;
			        } // end switch	
					}});
		asb.jButton6.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:Logout();break;
	            case 2:setContentPane(mrp);break;
	            case 3:setContentPane(mrp);break;
	        } // end switch	
			}});
		asb.jButton7.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:break;
	            case 2:Logout();break;
	            case 3:setContentPane(am);break;
	        } // end switch	
			}});
		asb.jButton8.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:break;
	            case 2:break;
	            case 3:Logout();break;
	        } // end switch	
			}});
		
		asb.jButton9.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    setContentPane(asb);           
	        } // end switch	
			});
		asb.table.addMouseListener(new MouseAdapter() {
	         public void mouseClicked(MouseEvent e) {
	             form = 2;
	        	 isbn=(String)asb.model.getValueAt(asb.table.getSelectedRow(), 0);
	        	 Orders isb = new Orders(isbn,1,2,3);// save isbn for write review

	        	 
	        	 ShowBook(isbn); 
	          	 Orders sbb = new Orders();
	        	 ov.loadBook(sbb);	        	
	        	 setContentPane(ov);
	        	}
	     });
		
		
		/**
		 *  Back button returns to search book
		 */
	showBuy.jButton1.addActionListener(new java.awt.event.ActionListener() {
 			public void actionPerformed(java.awt.event.ActionEvent e) {
 				if (form == 1)
 					setContentPane(sb);
 					else
 					setContentPane(asb);
 			}});
		/**
		 *  Overview button returns to overview panel
		 */
	showBuy.Button1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
		       	
//				isbn=(String)sb.model.getValueAt(sb.table.getSelectedRow(), 0);
		    	 Orders isb = new Orders(isbn,1,2,3);// save isbn for write review

		    	 
		    	 ShowBook(isbn); 
		      	 Orders sbb = new Orders();
		    	 ov.loadBook(sbb);	        	
		    	 setContentPane(ov);
			}});
	
	/**
	 *  Write Review button returns to Write review
	 */
	showBuy.Button3.addActionListener(new java.awt.event.ActionListener() {
		public void actionPerformed(java.awt.event.ActionEvent e) {
			Orders sbb1 = new Orders();
       	 ShowBook(sbb1.getIsbn()); // get current viewed book 
         	 
       	 wr.loadBook(sbb1);	 
       	 
       	
			
			
			setContentPane(wr);
		}});
	
		
		
		/* end vitali */
		
		
		
		
		/* ActionListeners for JButtons in mm */
		
		mm.jButton1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				setContentPane(sb);
			
			}});
		mm.jButton2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
					
				setContentPane(sr);
			}});
		mm.jButton3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:setContentPane(as);break;
	            case 1:setContentPane(mb);break;
	            case 2:setContentPane(mb);break;
	            case 3:setContentPane(mb);break;
	        } // end switch	
			}});
		mm.jButton4.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:Logout();break;
	            case 1:setContentPane(mc);break;
	            case 2:setContentPane(mc);break;
	            case 3:setContentPane(mc);break;
	        } // end switch	
			}});
		mm.jButton5.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
			            case 0:break;
			            case 1:setContentPane(mr);break;
			            case 2:setContentPane(mr);break;
			            case 3:setContentPane(mr);break;
			        } // end switch	
					}});
		mm.jButton6.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:Logout();break;
	            case 2:setContentPane(mrp);break;
	            case 3:setContentPane(mrp);break;
	        } // end switch	
			}});
		mm.jButton7.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:break;
	            case 2:Logout();break;
	            case 3:setContentPane(am);break;
	        } // end switch	
			}});
		mm.jButton8.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:break;
	            case 2:break;
	            case 3:Logout();break;
	        } // end switch	
			}});
		
		/* ActionListeners for JButtons in sb */
		
//		sb.jButton1.addActionListener(new java.awt.event.ActionListener() {
//			public void actionPerformed(java.awt.event.ActionEvent e) {
//				
//	        	 setContentPane(sb);
//
//				
//			}});
//		
		
		
//		sb.jButton1.addActionListener(new java.awt.event.ActionListener() {
//			public void actionPerformed(java.awt.event.ActionEvent e) {
//				//setContentPane(sb); // do nothing if already in sb
//			}});
		sb.jButton2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
					
				setContentPane(sr);
			}});
		sb.jButton3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:setContentPane(as);break;
	            case 1:setContentPane(mb);break;
	            case 2:setContentPane(mb);break;
	            case 3:setContentPane(mb);break;
	        } // end switch	
			}});
		sb.jButton4.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:Logout();break;
	            case 1:setContentPane(mc);break;
	            case 2:setContentPane(mc);break;
	            case 3:setContentPane(mc);break;
	        } // end switch	
			}});
		sb.jButton5.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
			            case 0:break;
			            case 1:sb.initialize();
			            setContentPane(mr);break;
			            case 2:setContentPane(mr);break;
			            case 3:setContentPane(mr);break;
			        } // end switch	
					}});
		sb.jButton6.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:Logout();break;
	            case 2:setContentPane(mrp);break;
	            case 3:setContentPane(mrp);break;
	        } // end switch	
			}});
		sb.jButton7.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:break;
	            case 2:Logout();break;
	            case 3:setContentPane(am);break;
	        } // end switch	
			}});
		sb.jButton8.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:break;
	            case 2:break;
	            case 3:Logout();break;
	        } // end switch	
			}});
		sb.jButton9.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:setContentPane(asb);break;
	            case 1:setContentPane(asb);break;
	            case 2:setContentPane(asb);break;
	            case 3:setContentPane(asb);break;
	        } // end switch	
			}});
	     sb.table.addMouseListener(new MouseAdapter() {
	         public void mouseClicked(MouseEvent e) {
	      
	        	 isbn=(String)sb.model.getValueAt(sb.table.getSelectedRow(), 0);
	        	 Orders isb = new Orders(isbn,1,2,3);// save isbn for write review

	        	 updateBookSearch(isbn);
	        	 ShowBook(isbn); 
	          	 Orders sbb = new Orders();
	        	 ov.loadBook(sbb);	        	
	        	 setContentPane(ov);
	         }
	     });
		
		/* ActionListeners for JButtons in sr */
		
		
		sr.jButton1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				sb.repaint();
				setContentPane(sb);
			}});
		sr.jButton2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
					
				//setContentPane(sr);
			}});
		sr.jButton3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:setContentPane(as);break;
	            case 1:setContentPane(mb);break;
	            case 2:setContentPane(mb);break;
	            case 3:setContentPane(mb);break;
	        } // end switch	
			}});
		sr.jButton4.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:Logout();break;
	            case 1:setContentPane(mc);break;
	            case 2:setContentPane(mc);break;
	            case 3:setContentPane(mc);break;
	        } // end switch	
			}});
		sr.jButton5.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
			            case 0:break;
			            case 1:setContentPane(mr);break;
			            case 2:setContentPane(mr);break;
			            case 3:setContentPane(mr);break;
			        } // end switch	
					}});
		sr.jButton6.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:Logout();break;
	            case 2:setContentPane(mrp);break;
	            case 3:setContentPane(mrp);break;
	        } // end switch	
			}});
		sr.jButton7.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:break;
	            case 2:Logout();break;
	            case 3:setContentPane(am);break;
	        } // end switch	
			}});
		sr.jButton8.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:break;
	            case 2:break;
	            case 3:Logout();break;
	        } // end switch	
			}});

	     sr.table.addMouseListener(new MouseAdapter() {
	         public void mouseClicked(MouseEvent e) {
	            form = 1;
	        	 isbn=(String)sr.model.getValueAt(sr.table.getSelectedRow(), 0);
	        	 user=(String)sr.model.getValueAt(sr.table.getSelectedRow(), 1);

	        	 ShowRev show = new ShowRev(isbn,user);
	        	 ShowReview();
	        	 Review revi = new Review();
	        	 System.out.println("almost working isbn"+revi.getIsbn());
//	        	         	 setContentPane(shr=new ShowReview(isbn,user));
	        	 	shr.loadReview(revi);
	        	 setContentPane(shr);
	         }
	     });
		
	
		/* ActionListeners for JButtons in as */
		
		
		as.jButton1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				setContentPane(sb);
			}});
		as.jButton2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
					
				setContentPane(sr);
			}});
		as.jButton3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				/*    switch (u.getPriv()) {
	           case 0:setContentPane(as);break;
	            case 1:setContentPane(mb);break;
	            case 2:setContentPane(mb);break;
	            case 3:setContentPane(mb);break; 
	        } // end switch	*/
			}});
		as.jButton4.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				switch (u.getPriv()) {
	            case 0:Logout();break;
	            case 1:setContentPane(mc);break;
	            case 2:setContentPane(mc);break;
	            case 3:setContentPane(mc);break;
	        } // end switch	
			}});
		as.jButton5.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
			            case 0:break;
			            case 1:setContentPane(mr);break;
			            case 2:setContentPane(mr);break;
			            case 3:setContentPane(mr);break;
			        } // end switch	
					}});
		as.jButton6.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:Logout();break;
	            case 2:setContentPane(mrp);break;
	            case 3:setContentPane(mrp);break;
	        } // end switch	
			}});
		as.jButton7.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:break;
	            case 2:Logout();break;
	            case 3:setContentPane(am);break;
	        } // end switch	
			}});
		as.jButton8.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:break;
	            case 2:break;
	            case 3:Logout();break;
	        } // end switch	
			}});
		
		/* ActionListeners for JButtons in mb */
		
		
		mb.jButton1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				setContentPane(sb);
			}});
		mb.jButton2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
					
				setContentPane(sr);
			}});
		mb.jButton3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			/*    switch (u.getPriv()) {
	            case 0:setContentPane(as);break;
	            case 1:setContentPane(mb);break;
	            case 2:setContentPane(mb);break;
	            case 3:setContentPane(mb);break;
	        } // end switch	 */
			}});
		mb.jButton4.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:Logout();break;
	            case 1:setContentPane(mc);break;
	            case 2:setContentPane(mc);break;
	            case 3:setContentPane(mc);break;
	        } // end switch	
			}});
		mb.jButton5.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
			            case 0:break;
			            case 1:setContentPane(mr);break;
			            case 2:setContentPane(mr);break;
			            case 3:setContentPane(mr);break;
			        } // end switch	
					}});
		mb.jButton6.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:Logout();break;
	            case 2:setContentPane(mrp);break;
	            case 3:setContentPane(mrp);break;
	        } // end switch	
			}});
		mb.jButton7.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:break;
	            case 2:Logout();break;
	            case 3:;setContentPane(am);break;
	        } // end switch	
			}});
		mb.jButton8.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:break;
	            case 2:break;
	            case 3:Logout();break;
	        } // end switch	
			}});
		
		/* ActionListeners for JButtons in mc */
		
		
		
		mc.jButton1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				setContentPane(sb);
			}});
		mc.jButton2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
					
				setContentPane(sr);
			}});
		mc.jButton3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:setContentPane(as);break;
	            case 1:setContentPane(mb);break;
	            case 2:setContentPane(mb);break;
	            case 3:setContentPane(mb);break;
	        } // end switch	 
			}});
		mc.jButton4.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			 /*   switch (u.getPriv()) {
	    //        case 0:Logout();break;
	            case 1:setContentPane(mc);break;
	            case 2:setContentPane(mc);break;
	            case 3:setContentPane(mc);break;
	        } // end switch	*/
			}});
		mc.jButton5.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
			            case 0:break;
			            case 1:setContentPane(mr);break;
			            case 2:setContentPane(mr);break;
			            case 3:setContentPane(mr);break;
			        } // end switch	
					}});
		mc.jButton6.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:Logout();break;
	            case 2:setContentPane(mrp);break;
	            case 3:setContentPane(mrp);break;
	        } // end switch	
			}});
		mc.jButton7.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:break;
	            case 2:Logout();break;
	            case 3:setContentPane(am);break;
	        } // end switch	
			}});
		mc.jButton8.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:break;
	            case 2:break;
	            case 3:Logout();break;
	        } // end switch	
			}});
		
		/* ActionListeners for JButtons in mr */
		
		
		
		mr.jButton1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				setContentPane(sb);
			}});
		mr.jButton2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
					
				setContentPane(sr);
			}});
		mr.jButton3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:setContentPane(as);break;
	            case 1:setContentPane(mb);break;
	            case 2:setContentPane(mb);break;
	            case 3:setContentPane(mb);break;
	        } // end switch	 
			}});
		mr.jButton4.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:Logout();break;
	            case 1:setContentPane(mc);break;
	            case 2:setContentPane(mc);break;
	            case 3:setContentPane(mc);break;
	        } // end switch	
			}});
		mr.jButton5.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			 /*   switch (u.getPriv()) {
			            case 0:break;
			            case 1:setContentPane(mr);break;
			            case 2:setContentPane(mr);break;
			            case 3:setContentPane(mr);break;
			        } // end switch	 */
					}});
		mr.jButton6.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:Logout();break;
	            case 2:setContentPane(mrp);break;
	            case 3:setContentPane(mrp);break;
	        } // end switch	
			}});
		mr.jButton7.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:break;
	            case 2:Logout();break;
	            case 3:setContentPane(am);break;
	        } // end switch	
			}});
		mr.jButton8.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:break;
	            case 2:break;
	            case 3:Logout();break;
	        } // end switch	
			}});

		
		/* ActionListeners for JButtons in mrp */
		
		
		
		mrp.jButton1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				setContentPane(sb);
			}});
		mrp.jButton2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
					
				setContentPane(sr);
			}});
		mrp.jButton3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:setContentPane(as);break;
	            case 1:setContentPane(mb);break;
	            case 2:setContentPane(mb);break;
	            case 3:setContentPane(mb);break;
	        } // end switch	 
			}});
		mrp.jButton4.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:Logout();break;
	            case 1:setContentPane(mc);break;
	            case 2:setContentPane(mc);break;
	            case 3:setContentPane(mc);break;
	        } // end switch	
			}});
		mrp.jButton5.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
			            case 0:break;
			            case 1:setContentPane(mr);break;
			            case 2:setContentPane(mr);break;
			            case 3:setContentPane(mr);break;
			        } // end switch	
					}});
		mrp.jButton6.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			/*    switch (u.getPriv()) {
	            case 0:break;
	      //      case 1:Logout();break;
	            case 2:setContentPane(mrp);break;
	            case 3:setContentPane(mrp);break;
	        } // end switch	 */
			}});
		mrp.jButton7.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:break;
	            case 2:Logout();break;
	            case 3:setContentPane(am);break;
	        } // end switch	
			}});
		mrp.jButton8.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:break;
	            case 2:break;
	            case 3:Logout();break;
	        } // end switch	
			}});
		
				/*	Order List	*/
		mrp.Button1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
	        	 getUsers();
	        	 o.loadUsers(order,0); // load all users
	        	 setContentPane(o); // goto OrderList.java
				
				
				
			}});
		mrp.Button2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
//				 getContentPane().remove(sh);
				
				String[] tmp = new String[13];
				for(int i=0;i<13;i++) // reset tmp
					tmp[i]="0";
				String[] List2= new String[200]; // for order counters

			
					currentYear="1000";
					currentBook="w";
					
					getBooks();
					getHist(); 

					List2= (String[]) order.getCounter();
					List2[0]="no data";
					
					if(!List2[0].equals("no data")){
					for(int i=0;i<List2.length-1;i=i+2){ //show tmp
						tmp[Integer.parseInt(List2[i])]=Integer.toString(Integer.parseInt(List2[i+1])*Integer.parseInt(List2[i]));
						System.out.println("tmp: :"+tmp[Integer.parseInt(List2[i])]);
					
						}//end for
					}// end if
					
						tmp[0]="no data";
				


					System.out.println("befor load 1");
					sh.loadBooks(order,tmp,0); // load all books andq histogram
					sh.loadHist2(tmp, 0);
		        	 setContentPane(sh);

			}
		});

		mrp.Button3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			 	 getBookPo();
	        	 bp.loadBooks(order); // load all users
	        	 setContentPane(bp); // goto OrderList.java
				
				
				
			}});
		
		
		
		/* ActionListeners for JButtons in am */
		
		
		
		am.jButton1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				setContentPane(sb);
			}});
		am.jButton2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
					
				setContentPane(sr);
			}});
		am.jButton3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:setContentPane(as);break;
	            case 1:setContentPane(mb);break;
	            case 2:setContentPane(mb);break;
	            case 3:setContentPane(mb);break;
	        } // end switch	 
			}});
		am.jButton4.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:Logout();break;
	            case 1:setContentPane(mc);break;
	            case 2:setContentPane(mc);break;
	            case 3:setContentPane(mc);break;
	        } // end switch	
			}});
		am.jButton5.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
			            case 0:break;
			            case 1:setContentPane(mr);break;
			            case 2:setContentPane(mr);break;
			            case 3:setContentPane(mr);break;
			        } // end switch	
					}});
		am.jButton6.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:Logout();break;
	            case 2:setContentPane(mrp);break;
	            case 3:setContentPane(mrp);break;
	        } // end switch	
			}});
		am.jButton7.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				/*	    switch (u.getPriv()) {
	            case 0:break;
	            case 1:break;
	            case 2:Logout();break;
	            case 3:am.ShowActionPerformed("None", "None");setContentPane(am);break;
	        } // end switch	*/
			}});
		am.jButton8.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:break;
	            case 2:break;
	            case 3:Logout();break;
	        } // end switch	
			}});
		
		
		
		/* ActionListeners for JButtons in shr */
		
		shr.jButton1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				setContentPane(sb);
			}});
		shr.jButton2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
					
				//setContentPane(sr);
			}});
		shr.jButton3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:setContentPane(as);break;
	            case 1:setContentPane(mb);break;
	            case 2:setContentPane(mb);break;
	            case 3:setContentPane(mb);break;
	        } // end switch	
			}});
		shr.jButton4.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:Logout();break;
	            case 1:setContentPane(mc);break;
	            case 2:setContentPane(mc);break;
	            case 3:setContentPane(mc);break;
	        } // end switch	
			}});
		shr.jButton5.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
			            case 0:break;
			            case 1:setContentPane(mr);break;
			            case 2:setContentPane(mr);break;
			            case 3:setContentPane(mr);break;
			        } // end switch	
					}});
		shr.jButton6.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:Logout();break;
	            case 2:setContentPane(mrp);break;
	            case 3:setContentPane(mrp);break;
	        } // end switch	
			}});
		shr.jButton7.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:break;
	            case 2:Logout();break;
	            case 3:setContentPane(am);break;
	        } // end switch	
			}});
		shr.jButton8.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:break;
	            case 2:break;
	            case 3:Logout();break;
	        } // end switch	
			}});


        shr.jButton11.addActionListener(new java.awt.event.ActionListener() { //back button
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	System.out.println("Back Clicked");
            	setContentPane(sr);
            }
        });
        
        /* ActionListeners for JButtons in o */
        
		o.jButton1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				setContentPane(sb);
			}});
		o.jButton2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
					
				setContentPane(sr);
			}});
		o.jButton3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:setContentPane(as);break;
	            case 1:setContentPane(mb);break;
	            case 2:setContentPane(mb);break;
	            case 3:setContentPane(mb);break;
	        } // end switch	
			}});
		o.jButton4.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:Logout();break;
	            case 1:setContentPane(mc);break;
	            case 2:setContentPane(mc);break;
	            case 3:setContentPane(mc);break;
	        } // end switch	
			}});
		o.jButton5.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
			            case 0:break;
			            case 1:setContentPane(mr);break;
			            case 2:setContentPane(mr);break;
			            case 3:setContentPane(mr);break;
			        } // end switch	
					}});
		o.jButton6.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:Logout();break;
	            case 2:setContentPane(mrp);break;
	            case 3:setContentPane(mrp);break;
	        } // end switch	
			}});
		o.jButton7.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:break;
	            case 2:Logout();break;
	            case 3:setContentPane(am);break;
	        } // end switch	
			}});
		o.jButton8.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:break;
	            case 2:break;
	            case 3:Logout();break;
	        } // end switch	
			}});        
		o.Button2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				
				String[] tmp = new String[13];
				for(int i=0;i<13;i++) // reset tmp
					tmp[i]="0";
				String[] List2= new String[200]; // for order counters

			
					currentYear="1000";
					currentBook="w";
					
					getBooks();
					getHist(); 

					List2= (String[]) order.getCounter();
					List2[0]="no data";
					
					if(!List2[0].equals("no data")){
					for(int i=0;i<List2.length-1;i=i+2){ //show tmp
						tmp[Integer.parseInt(List2[i])]=Integer.toString(Integer.parseInt(List2[i+1])*Integer.parseInt(List2[i]));
						System.out.println("tmp: :"+tmp[Integer.parseInt(List2[i])]);
					
						}//end for
					}// end if
					
						tmp[0]="no data";
				


					System.out.println("befor load 1");
					sh.loadBooks(order,tmp,0); // load all books andq histogram
					sh.loadHist2(tmp, 0);
		        	 setContentPane(sh);

			}
		});
				
				
		

		o.Button3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			 	 getBookPo();
	        	 bp.loadBooks(order); // load all users
	        	 setContentPane(bp); // goto OrderList.java

				
				
			}});     
    
      /* ActionListeners for JButtons in bs: Book Statistics */
        
		bs.jButton1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				setContentPane(sb);
			}});
		bs.jButton2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
					
				//setContentPane(sr);
			}});
		bs.jButton3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:setContentPane(as);break;
	            case 1:setContentPane(mb);break;
	            case 2:setContentPane(mb);break;
	            case 3:setContentPane(mb);break;
	        } // end switch	
			}});
		bs.jButton4.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:Logout();break;
	            case 1:setContentPane(mc);break;
	            case 2:setContentPane(mc);break;
	            case 3:setContentPane(mc);break;
	        } // end switch	
			}});
		bs.jButton5.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
			            case 0:break;
			            case 1:setContentPane(mr);break;
			            case 2:setContentPane(mr);break;
			            case 3:setContentPane(mr);break;
			        } // end switch	
					}});
		bs.jButton6.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:Logout();break;
	            case 2:setContentPane(mrp);break;
	            case 3:setContentPane(mrp);break;
	        } // end switch	
			}});
		bs.jButton7.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:break;
	            case 2:Logout();break;
	            case 3:setContentPane(am);break;
	        } // end switch	
			}});
		bs.jButton8.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:break;
	            case 2:break;
	            case 3:Logout();break;
	        } // end switch	
			}});        
		bs.Button1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			 	 getUsers();
	        	 o.loadUsers(order,0); // load all users
	        	 setContentPane(o); // goto OrderList.java
			}});
		bs.Button3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			 	 getBookPo();
	        	 bp.loadBooks(order); // load all users
	        	 setContentPane(bp); // goto OrderList.java
			}});     
		bs.jComboBox2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				String[] tmp = new String[13];
				for(int i=0;i<13;i++) // reset tmp
					tmp[i]="0";
				String[] List2= new String[200]; // for order counters

//				 	 getBooks();
//	        	 sh.loadBooks(order); // load all users
//	        	 getContentPane().removeAll();
////	        	 getContentPane().add(sh);
//	        	 setContentPane(sh); // goto BookSta.java
				if(!bs.jComboBox2.getSelectedItem().equals("Year")){
					
					
				
					
					
					
					
					currentYear=(String)bs.jComboBox2.getSelectedItem();
					currentBook=(String)bs.jComboBox1.getSelectedItem();
//					System.out.println("bs.jComboBox2"); 
					getBooks();
					getHist(); 
//					getContentPane().removeAll();
					List2= (String[]) order.getCounter();
				
					
					if(!List2[0].equals("no data")){
					for(int i=0;i<List2.length-1;i=i+2){ //show tmp
						tmp[Integer.parseInt(List2[i])]=Integer.toString(Integer.parseInt(List2[i+1])*Integer.parseInt(List2[i]));
						System.out.println("tmp: :"+tmp[Integer.parseInt(List2[i])]);
					
						}//end for
					}// end if
					else{
						tmp[0]="no data";
					}// end else


//					DrawHistApplet ap = new DrawHistApplet(tmp);
//					app.loadData(tmp);
					System.out.println("befor load 1");
					sh.loadBooks(order,tmp,1); // load all books andq histogram
					
//					sh.loadBooks(order); // load all books andq histogram
//				
//					resetAll();
//				
					
					getContentPane().removeAll();
//					getContentPane().add(bs);

					
//					getContentPane().add(sh);
				

		        	 setContentPane(sh);
				}// end if
			}
		});
		
		
       /* ActionListeners for JButtons in bp: Book Popularity */
        
		bp.jButton1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				setContentPane(sb);
			}});
		bp.jButton2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
					
				setContentPane(sr);
			}});
		bp.jButton3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:setContentPane(as);break;
	            case 1:setContentPane(mb);break;
	            case 2:setContentPane(mb);break;
	            case 3:setContentPane(mb);break;
	        } // end switch	
			}});
		bp.jButton4.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:Logout();break;
	            case 1:setContentPane(mc);break;
	            case 2:setContentPane(mc);break;
	            case 3:setContentPane(mc);break;
	        } // end switch	
			}});
		bp.jButton5.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
			            case 0:break;
			            case 1:setContentPane(mr);break;
			            case 2:setContentPane(mr);break;
			            case 3:setContentPane(mr);break;
			        } // end switch	
					}});
		bp.jButton6.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:Logout();break;
	            case 2:setContentPane(mrp);break;
	            case 3:setContentPane(mrp);break;
	        } // end switch	
			}});
		bp.jButton7.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:break;
	            case 2:Logout();break;
	            case 3:setContentPane(am);break;
	        } // end switch	
			}});
		bp.jButton8.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:break;
	            case 2:break;
	            case 3:Logout();break;
	        } // end switch	
			}});        
		bp.Button1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			 	 getUsers();
	        	 o.loadUsers(order,0); // load all users
	        	 setContentPane(o); // goto OrderList.java
			}});
		bp.Button2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				
				
				
				String[] tmp = new String[13];
				for(int i=0;i<13;i++) // reset tmp
					tmp[i]="0";
				String[] List2= new String[200]; // for order counters

			
					currentYear="1000";
					currentBook="w";
					
					getBooks();
					getHist(); 

					List2= (String[]) order.getCounter();
					List2[0]="no data";
					
					if(!List2[0].equals("no data")){
					for(int i=0;i<List2.length-1;i=i+2){ //show tmp
						tmp[Integer.parseInt(List2[i])]=Integer.toString(Integer.parseInt(List2[i+1])*Integer.parseInt(List2[i]));
						System.out.println("tmp: :"+tmp[Integer.parseInt(List2[i])]);
					
						}//end for
					}// end if
					
						tmp[0]="no data";
				


					System.out.println("befor load 1");
					sh.loadBooks(order,tmp,0); // load all books andq histogram
					sh.loadHist2(tmp, 0);
		        	 setContentPane(sh);

			}
		});
//			 	 getBookPo();
//	        	 bp.loadBooks(order); // load all users
//	        	 setContentPane(bs); // goto OrderList.java
//			}});     
        
       /* ActionListeners for JButtons in ov: Overview */
        
		ov.Button5.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
                if (form==2) 
                	setContentPane(asb);
                else
				     setContentPane(sb);
			}});
		ov.Button2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
//				setContentPane(rrs);
			}});
		ov.Button3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				
				Orders sbb1 = new Orders();
	        	 ShowBook(sbb1.getIsbn()); // get current viewed book 
	          	 
	        	 wr.loadBook(sbb1);	 
	        	 
	        	
				
				
				setContentPane(wr);
			}});
		/* buybook listener  */
		ov.Button4.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {

				
				String UserName=(String)u.getUser();
				 String bbook=(String)ov.jText4.getText();// get isbn
				 System.out.println("ov.Button4:isbn"+bbook);
				 CheckAccounts(UserName,bbook);
		
				 Accounts a = new Accounts();
				 System.out.println("----------a:"+a.IshasTheBook());
	        	 showBuy.InitBook(a);
					Orders sbb1 = new Orders();
		        	 ShowBook(sbb1.getIsbn()); // get current viewed book 
		          	 Book bb = new Book (ov.jText1.getText(),bbook,ov.jText7.getText());
		        	 showBuy.loadBook(sbb1,bb);	 
	        	 setContentPane(showBuy);	
				
				
				
				
			}});
		
		/* ActionListeners for JButtons in sh: Show Histogram */
		
		sh.jButton1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				setContentPane(sb);
			}});
		sh.jButton2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
					
				setContentPane(sr);
			}});
		sh.jButton3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:setContentPane(as);break;
	            case 1:setContentPane(mb);break;
	            case 2:setContentPane(mb);break;
	            case 3:setContentPane(mb);break;
	        } // end switch	
			}});
		sh.jButton4.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:Logout();break;
	            case 1:setContentPane(mc);break;
	            case 2:setContentPane(mc);break;
	            case 3:setContentPane(mc);break;
	        } // end switch	
			}});
		sh.jButton5.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
			            case 0:break;
			            case 1:setContentPane(mr);break;
			            case 2:setContentPane(mr);break;
			            case 3:setContentPane(mr);break;
			        } // end switch	
					}});
		sh.jButton6.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:Logout();break;
	            case 2:setContentPane(mrp);break;
	            case 3:setContentPane(mrp);break;
	        } // end switch	
			}});
		sh.jButton7.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:break;
	            case 2:Logout();break;
	            case 3:setContentPane(am);break;
	        } // end switch	
			}});
		sh.jButton8.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			    switch (u.getPriv()) {
	            case 0:break;
	            case 1:break;
	            case 2:break;
	            case 3:Logout();break;
	        } // end switch	
			}});        
		sh.Button1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			 
				
					getUsers();
	        	 o.loadUsers(order,0); // load all users
	        	 setContentPane(o); // goto OrderList.java
			}});
		sh.Button3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
			 	 getBookPo();
	        	 bp.loadBooks(order); // load all users
	        	 setContentPane(bp); // goto OrderList.java
			}});     
		sh.jComboBox2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				
				String[] tmp = new String[13];
				for(int i=0;i<13;i++) // reset tmp
					tmp[i]="0";
				String[] List2= new String[200]; // for order counters


				if(!sh.jComboBox2.getSelectedItem().equals("Year")){
					currentYear=(String)sh.jComboBox2.getSelectedItem();
					currentBook=(String)sh.jComboBox1.getSelectedItem();

					getBooks();
					getHist(); 

					List2= (String[]) order.getCounter();
				
					
					if(!List2[0].equals("no data")){
					for(int i=0;i<List2.length-1;i=i+2){ //show tmp
						tmp[Integer.parseInt(List2[i])]=Integer.toString(Integer.parseInt(List2[i+1])*Integer.parseInt(List2[i]));
						System.out.println("tmp: :"+tmp[Integer.parseInt(List2[i])]);
					
						}//end for
					}// end if
					else{
						tmp[0]="no data";
					}// end else

					System.out.println("befor load 1");
					sh.loadBooks(order,tmp,1); // load all books and histogram 1
					
					
							/*	histogram 2	*/
					
					getHist2(); 
					String[] List3 = new String[200];
					List3= (String[]) order.getCounter();
				
					System.out.println("mm: hist2222:List3[0]:"+List3[0]);
					if(!List3[0].equals("no data")){
					tmp[0]=List3[0];
					for(int i=0;i<List3.length-1;i=i+2){ //show tmp
						tmp[Integer.parseInt(List3[i])]=Integer.toString(Integer.parseInt(List3[i+1])*Integer.parseInt(List3[i]));
						System.out.println("tmp: :"+tmp[Integer.parseInt(List3[i])]);
					
						}//end for
					}// end if
					else{
						tmp[0]="no data";
					}// end else

					System.out.println("befor load 2");
					
					sh.loadHist2(tmp,1); // load histogram 2

				}// end if
			}
		});
        
		/* ActionListeners for JButtons in wr: Write Review */
		
		
			/** Overview Button ( returns to overview panel )
			 * 
			 * */
		wr.Button1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {

	        	 
	        	 Orders isb = new Orders(isbn,1,2,3);// save isbn for write review

	        	 
	        	 ShowBook(isbn); 
	          	 Orders sbb = new Orders();
	        	 ov.loadBook(sbb);	        	
	        	 setContentPane(ov);
		
			}});
		
		wr.Button4.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				String UserName=(String)u.getUser();
				 String bbook=(String)ov.jText4.getText();// get isbn
				 System.out.println("ov.Button4:isbn"+bbook);
				 CheckAccounts(UserName,bbook);
		
				 Accounts a = new Accounts();
				 System.out.println("----------a:"+a.IshasTheBook());
	        	 showBuy.InitBook(a);
					Orders sbb1 = new Orders();
		        	 ShowBook(sbb1.getIsbn()); // get current viewed book 
		          	 Book bb = new Book (ov.jText1.getText(),bbook,ov.jText7.getText());
		        	 showBuy.loadBook(sbb1,bb);	 
	        	 setContentPane(showBuy);	
				
			}});
		/**Back button returns to panel search book
		 * 
		 */
		wr.Button5.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				wr.TextArea01.setText(""); // reset text field
				if (form == 1)
 					setContentPane(sb);
 					else
 					setContentPane(asb);
			
			}});
		
		wr.Button8.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				String[] List2= new String[200]; //
				String rate = new String();
				Orders o = new Orders();
			
				/* check if the user already has a review wirtten on that book */
				checkReview(u.getUser(),wr.jText4.getText());
				ShowRev shor = new ShowRev();
				List2[0]=(String)shor.gethasReview();
				System.out.println("wr.button8: "+List2[0]);
				if(List2[0].equals("no")){
				/*sent review to server (isbn,username,review,rate,) */
				sentReview(o.getIsbn(),u.getUser(),(String)wr.TextArea01.getText(),(String)wr.Combo04.getSelectedItem());
				wr.TextArea01.setText(""); // reset text field
				}
				else{
					Orders sbb1 = new Orders();
		        	 ShowBook(sbb1.getIsbn()); // get current viewed book 
		          	 
		        	 ew.loadBook(sbb1);	 
					setContentPane(ew);
		
				}// end else

			
			}});
		
		/* ActionListeners for JButtons in ew: Error Write */
		
		ew.Button5.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {

				wr.TextArea01.setText(""); // reset text field
				setContentPane(ov);
			}});
		ew.Button1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {

				wr.TextArea01.setText(""); // reset text field
				setContentPane(ov);
			}});
		
		
		
		
		
		
		/* 
		 *  
		 *  		end listeners 
		 *  
		 *  */
		
		
		/* jLabel1 (The Background) */
		jLabel1 = new javax.swing.JLabel();
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Blue1.jpg"))); // NOI18N
        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        getContentPane().add(jLabel1);
        jLabel1.setBounds(0, 0, 1600, 1200);
		

		privinit();
	}// end initialize()

        
        
	

	/**
	 * This method initializes FirstPanel
	 */
	private JPanel getFirstPanel() {
		if (FirstPanel == null) {
			FirstPanel = new JPanel();
		
			FirstPanel.setLayout(new BorderLayout());
			FirstPanel.setBounds(new Rectangle(20	, 23, 172, 29));
		
		//	FirstPanel.add(getStudentsButton());
		}
		return FirstPanel; 
	}



	/**
	 * This method initializes jLabel1	
	 * 	
	 * @return javax.swing.JLabel	
	 */
	private JLabel getJLabel1() {
		if (jLabel1 == null) {
			jLabel1 = new JLabel();
			jLabel1.setText("JLabel");

		}
		return jLabel1;
	}// end getJLabel1
	
	
	void privinit(){
	   	 /* Reader */
	   	if(u.getPriv() == 0){  
	   		setTitle("I-Book : "+u.getUser());


	   	}
	   	/* Librarian */
	   	else if(u.getPriv() == 1){
	   		setTitle("I-Book : Librarian :"+u.getUser());

	   	
	   	
	   	}
	   	/* Library Manager */
	   	else if(u.getPriv() == 2){
	   		setTitle("I-Book : Library Manager :"+u.getUser());

	   	
	   	}
	   	/* System Manager */
	   	else if(u.getPriv() == 3){
	   		setTitle("I-Book :System Manager :"+u.getUser());

	}
	
	}// end privinit

	
	public void Logout(){
		int connected = 1;
		obj logdt = new obj();
    	logdt.setType("Offline");
    	logdt.setParam(0,u.getUser());//user to disconnect
    	
   	 try
	    {
	    	 client= new ChatClient(u.getHost(),5555,ChatIF);
	    } // end try
	    catch(IOException exception)
	    {
	    	JOptionPane.showMessageDialog(null,"Error: Can't setup connection!","Error",JOptionPane.CLOSED_OPTION);
	    	connected = 0; // failed to connect
	    } // end catch
	    if(connected == 1)
	    	client.handleMessageFromClientUI(logdt);

	   System.exit(0);
		
	} // end Logout
	
	
	public void ShowReview(){
		int connected = 1;
		int i;
    	obj logdt1 = new obj();
    	ShowRev show = new ShowRev();

    
     	 try
 	    {
 	    	 client= new ChatClient(u.getHost(),5555,ChatIF);
 	    } // end try
 	    catch(IOException exception)
 	    {	    	
 	    	JOptionPane.showMessageDialog(null,"Error: Can't setup connection!","Error",JOptionPane.CLOSED_OPTION);
	    	connected = 0; // failed to connect
	    } // end catch
 	   System.out.println("start");
    	logdt1.setType("show");
    	logdt1.setParam(0,isbn); // isbn
    	logdt1.setParam(1,user); // username
    	
       	if(connected == 1)	
       			client.handleMessageFromClientUI(logdt1);
   	  
	    try {
            Thread.sleep(100); 
            } // end try
	    catch (InterruptedException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } // end catch

    	/* end sql  */
	    Review r = new Review();

	}// end ShowReview
	
	public void ShowBook(String name){
		int connected = 1;
		int i;
    	obj logdt1 = new obj();
    	

    
     	 try
 	    {
 	    	 client= new ChatClient(u.getHost(),5555,ChatIF);
 	    } // end try
 	    catch(IOException exception)
 	    {	    	
 	    	JOptionPane.showMessageDialog(null,"Error: Can't setup connection!","Error",JOptionPane.CLOSED_OPTION);
	    	connected = 0; // failed to connect
	    } // end catch
 	   System.out.println("show book");
    	logdt1.setType("show book");
    	logdt1.setParam(0,name); // isbn
 
    	
       	if(connected == 1)	
       			client.handleMessageFromClientUI(logdt1);
   	  
	    try {
            Thread.sleep(100); 
            } // end try
	    catch (InterruptedException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } // end catch

    	/* end sql  */
		
	}//end
	
	
	public void writeReview(){
		int connected = 1;
		int i;
    	obj logdt1 = new obj();
//    	Book b = new Book();

    
     	 try
 	    {
 	    	 client= new ChatClient(u.getHost(),5555,ChatIF);
 	    } // end try
 	    catch(IOException exception)
 	    {	    	
 	    	JOptionPane.showMessageDialog(null,"Error: Can't setup connection!","Error",JOptionPane.CLOSED_OPTION);
	    	connected = 0; // failed to connect
	    } // end catch
 	   System.out.println("start show book in mm");
    	
 	   logdt1.setType("write review");
    	logdt1.setParam(0,isbn); // isbn
    
    	
       	if(connected == 1)	
       			client.handleMessageFromClientUI(logdt1);
   	  
	    try {
            Thread.sleep(100); 
            } // end try
	    catch (InterruptedException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } // end catch

    	/* end sql  */


//   System.out.println("from writeReview: Isbn: "+b.getIsbn());
	}// end writeReview

	
	
	public void getUsers(){
	
		int connected = 1;
		int i;
    	obj logdt1 = new obj();
    	Orders or = new Orders();

    
     	 try
 	    {
 	    	 client= new ChatClient(u.getHost(),5555,ChatIF);
 	    } // end try
 	    catch(IOException exception)
 	    {	    	
 	    	JOptionPane.showMessageDialog(null,"Error: Can't setup connection!","Error",JOptionPane.CLOSED_OPTION);
	    	connected = 0; // failed to connect
	    } // end catch
 	   System.out.println("mm: getUsers");
    	
 	   logdt1.setType("get users");

       	if(connected == 1)	
       			client.handleMessageFromClientUI(logdt1);
   	  
	    try {
            Thread.sleep(100); 
            } // end try
	    catch (InterruptedException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } // end catch

    	/* end sql  */
	    this.order = or;
   System.out.println("MainWindow or: "+or.getUser(0)+or.getUser(1));
   System.out.println("MainWindow order: "+order.getUser(0)+order.getUser(1));
	
	}// end getUsers()
	
	
	
	/* get all books */
	public void getBookPo(){
		
		int connected = 1;
		int i;
    	obj logdt1 = new obj();
    	Orders or = new Orders();

    
     	 try
 	    {
 	    	 client= new ChatClient(u.getHost(),5555,ChatIF);
 	    } // end try
 	    catch(IOException exception)
 	    {	    	
 	    	JOptionPane.showMessageDialog(null,"Error: Can't setup connection!","Error",JOptionPane.CLOSED_OPTION);
	    	connected = 0; // failed to connect
	    } // end catch

    	
 	   logdt1.setType("get books po");

       	if(connected == 1)	
       			client.handleMessageFromClientUI(logdt1);
   	  
	    try {
            Thread.sleep(100); 
            } // end try
	    catch (InterruptedException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } // end catch

    	/* end sql  */
	    
	    this.order = or;

	}// end getBookPo()
	
	
	public void setCurrentBook(String book)
	{
		this.currentBook = book;
	}
	public void setCurrentYear(String year)
	{
		this.currentYear = year;
	}
	
	public void getHist(){
		
		int connected = 1;
		int i;
    	obj logdt1 = new obj();
    	Orders or = new Orders();

    
     	 try
 	    {
 	    	 client= new ChatClient(u.getHost(),5555,ChatIF);
 	    } // end try
 	    catch(IOException exception)
 	    {	    	
 	    	JOptionPane.showMessageDialog(null,"Error: Can't setup connection!","Error",JOptionPane.CLOSED_OPTION);
	    	connected = 0; // failed to connect
	    } // end catch
 	   System.out.println("mm: currtentbook,currentYear:"+currentBook+currentYear);
    	
 	   logdt1.setType("get hist");
 	   logdt1.setParam(0, currentBook);
 	  logdt1.setParam(1, currentYear);

       	if(connected == 1)	
       			client.handleMessageFromClientUI(logdt1);
   	  
	    try {
            Thread.sleep(100); 
            } // end try
	    catch (InterruptedException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } // end catch

    	/* end sql  */
	    this.order = or;
//   System.out.println("MainWindow or: "+or.getUser(0)+or.getUser(1));
//   System.out.println("MainWindow order: "+order.getUser(0)+order.getUser(1));
	
	}// end getHist()
	
	
	public void getHist2(){
		
		int connected = 1;
		int i;
    	obj logdt1 = new obj();
    	Orders or = new Orders();

    
     	 try
 	    {
 	    	 client= new ChatClient(u.getHost(),5555,ChatIF);
 	    } // end try
 	    catch(IOException exception)
 	    {	    	
 	    	JOptionPane.showMessageDialog(null,"Error: Can't setup connection!","Error",JOptionPane.CLOSED_OPTION);
	    	connected = 0; // failed to connect
	    } // end catch
 	   System.out.println("mm: currtentbook,currentYear:"+currentBook+currentYear);
    	
 	   logdt1.setType("get hist2");
 	   logdt1.setParam(0, currentBook);
 	  logdt1.setParam(1, currentYear);

       	if(connected == 1)	
       			client.handleMessageFromClientUI(logdt1);
   	  
	    try {
            Thread.sleep(100); 
            } // end try
	    catch (InterruptedException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } // end catch

    	/* end sql  */
	    this.order = or;
//   System.out.println("MainWindow or: "+or.getUser(0)+or.getUser(1));
//   System.out.println("MainWindow order: "+order.getUser(0)+order.getUser(1));
	
	}// end getHist2()
	
	
	
	
	
	
	public void getBooks(){
		
		int connected = 1;
		int i;
    	obj logdt1 = new obj();
    	Orders or = new Orders();

    
     	 try
 	    {
 	    	 client= new ChatClient(u.getHost(),5555,ChatIF);
 	    } // end try
 	    catch(IOException exception)
 	    {	    	
 	    	JOptionPane.showMessageDialog(null,"Error: Can't setup connection!","Error",JOptionPane.CLOSED_OPTION);
	    	connected = 0; // failed to connect
	    } // end catch
// 	   System.out.println("mm: getUsers");
    	
 	   logdt1.setType("get books");

       	if(connected == 1)	
       			client.handleMessageFromClientUI(logdt1);
   	  
	    try {
            Thread.sleep(100); 
            } // end try
	    catch (InterruptedException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } // end catch

    	/* end sql  */
	    this.order = or;
//   System.out.println("MainWindow or: "+or.getUser(0)+or.getUser(1));
//   System.out.println("MainWindow order: "+order.getUser(0)+order.getUser(1));
	
	}// end getUsers()
	
	
	public void sentReview(String isbn,String username,String review,String rating){
		
		int connected = 1;
		int i;
    	obj logdt1 = new obj();
    	
     	 try
 	    {
 	    	 client= new ChatClient(u.getHost(),5555,ChatIF);
 	    } // end try
 	    catch(IOException exception)
 	    {	    	
 	    	JOptionPane.showMessageDialog(null,"Error: Can't setup connection!","Error",JOptionPane.CLOSED_OPTION);
	    	connected = 0; // failed to connect
	    } // end catch
// 	   System.out.println("mm: getUsers");
    	
 	   logdt1.setType("sent review");
 	   logdt1.setParam(0, isbn);
 	   logdt1.setParam(1, username);
 	   logdt1.setParam(2, review);
 	   logdt1.setParam(3, rating);
 	   
       	if(connected == 1)	
       			client.handleMessageFromClientUI(logdt1);
   	  
	    try {
            Thread.sleep(100); 
            } // end try
	    catch (InterruptedException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } // end catch

    	/* end sql  */
	    
	}// end sentReview()
	
	public void checkReview(String user,String isbn){
		
		int connected = 1;
		int i;
    	obj logdt1 = new obj();
    	
     	 try
 	    {
 	    	 client= new ChatClient(u.getHost(),5555,ChatIF);
 	    } // end try
 	    catch(IOException exception)
 	    {	    	
 	    	JOptionPane.showMessageDialog(null,"Error: Can't setup connection!","Error",JOptionPane.CLOSED_OPTION);
	    	connected = 0; // failed to connect
	    } // end catch

    	
 	   logdt1.setType("check review1");
 	   logdt1.setParam(0, isbn);
 	   logdt1.setParam(1, user);

 	   System.out.println("check review1fefe");
       	if(connected == 1)	
       			client.handleMessageFromClientUI(logdt1);
   	  
	    try {
            Thread.sleep(100); 
            } // end try
	    catch (InterruptedException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } // end catch

    	/* end sql  */
		
		
	}// end checkReview()
	public void CheckAccounts(String UserName,String bbook)
	{   int connected = 1;
	    try
	    {
	    	 client= new ChatClient(u.getHost(),5555,ChatIF);
	    } // end try
	    catch(IOException exception)
	    {	    	
	    	JOptionPane.showMessageDialog(null,"Error: Can't setup connection!","Error",JOptionPane.CLOSED_OPTION);
	    	connected = 0; // failed to connect
	    } // end catch
	    
		obj logdt1 = new obj();
		logdt1.setType("BuyBook ShowAccounts");
		logdt1.setParam(0,UserName); // set input from text field
		logdt1.setParam(1,bbook);
		
		if(connected == 1)	
		    	client.handleMessageFromClientUI(logdt1);
		
	    try {
	        Thread.sleep(1000); 
	        } // end try
	    catch (InterruptedException ex) {
	        Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
	    } // end catch

		/* end sql  */
		


	}
	public void ShowBookIn(String isbn){
		int connected = 1;
		int i;
    	obj logdt1 = new obj();
    	

    
     	 try
 	    {
 	    	 client= new ChatClient(u.getHost(),5555,ChatIF);
 	    } // end try
 	    catch(IOException exception)
 	    {	    	
 	    	JOptionPane.showMessageDialog(null,"Error: Can't setup connection!","Error",JOptionPane.CLOSED_OPTION);
	    	connected = 0; // failed to connect
	    } // end catch
 	   System.out.println("start");
    	logdt1.setType("showBookIn");

    	logdt1.setParam(0,isbn); // isbn
    	
       	if(connected == 1)	
       			client.handleMessageFromClientUI(logdt1);
   	  
	    try {
            Thread.sleep(100); 
            } // end try
	    catch (InterruptedException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } // end catch

    	/* end sql  */
	   Book b = new Book();

   System.out.println("from ShowBookIn: Isbn: "+b.getIsbn());
	}// end ShowBookIn
	
	public void updateBookSearch(String isbn){
		
		int connected = 1;
		int i;
    	obj logdt1 = new obj();
    	

    
     	 try
 	    {
 	    	 client= new ChatClient(u.getHost(),5555,ChatIF);
 	    } // end try
 	    catch(IOException exception)
 	    {	    	
 	    	JOptionPane.showMessageDialog(null,"Error: Can't setup connection!","Error",JOptionPane.CLOSED_OPTION);
	    	connected = 0; // failed to connect
	    } // end catch
 	   System.out.println("update search");
    	logdt1.setType("update search");

    	logdt1.setParam(0,isbn); // isbn

       	if(connected == 1)	
       			client.handleMessageFromClientUI(logdt1);
   	  
	    try {
            Thread.sleep(100); 
            } // end try
	    catch (InterruptedException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } // end catch

    	/* end sql  */
	    System.out.println("update search end");
	}// end updateBookSearch
	
	  
	
}// end Class


