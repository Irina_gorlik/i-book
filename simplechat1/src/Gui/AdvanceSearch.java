package Gui;

import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Rectangle;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Rectangle;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import Gui.*;
import logic.*;
import client.*;


import javax.swing.BorderFactory;
import java.awt.Color;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.JSlider;
import javax.swing.JToggleButton;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Choice;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JRadioButton;

import common.ChatIF;



public class AdvanceSearch extends JPanel {

	private JLabel jLabel99 = null;
	  private JLabel jLabel666 = null;

	private int rows=0;
	
	 public javax.swing.table.DefaultTableModel model;
		public javax.swing.JTable table=null;
		private JScrollPane jScrollPane1 = null;
	
	public static ChatClient client;  //  @jve:decl-index=0:
    private  ChatIF ChatIF = null;
    public int connected;
    
	private javax.swing.JLabel jLabel1;
	public javax.swing.JButton jButton1;
    public javax.swing.JButton jButton2;
    public javax.swing.JButton jButton3;
    public javax.swing.JButton jButton4;
    public javax.swing.JButton jButton5;
    public javax.swing.JButton jButton6;
    public javax.swing.JButton jButton7;
    public javax.swing.JButton jButton8;
    public javax.swing.JButton jButton9;
    User u = new User();  //  @jve:decl-index=0:
    public JTextField SearchField1 = null;
    public JTextField SearchField2 = null;
    public JTextField SearchField3 = null;
    public JTextField SearchField4 = null;
    public JTextField SearchField = null;
	private JLabel jLabel51 = null;
	private JButton AdSearch = null;
	public Choice choice0 = null;
	public Choice choice1 = null;
	public Choice choice2 = null;
	public Choice choice3 = null;
	public Choice choice4 = null;
	
	public JRadioButton jRadioOR1 = null;
	public JRadioButton jRadioAnd1 = null;
	private JLabel jLabelOR1 = null;
	private JLabel jLabelAnd1 = null;
	public JRadioButton jRadioOR2 = null;
	public JRadioButton jRadioAnd2 = null;
	private JLabel jLabelOR2 = null;
	private JLabel jLabelAnd2 = null;
	public JRadioButton jRadioOR3 = null;
	public JRadioButton jRadioOR4 = null;
	public JRadioButton jRadioAnd3 = null;
	public JRadioButton jRadioAnd4 = null;
	private JLabel jLabelOR3 = null;
	private JLabel jLabelOR4 = null;
	private JLabel jLabelAnd3 = null;
	private JLabel jLabelAnd4 = null;
	/**
	 * This is the default constructor
	 */
	public AdvanceSearch() {
		super();
		initialize();
	}

	/**
	 * This method initializes StudentForm
	 */
	private void initialize() {
	
	        
		jLabel99 = new JLabel();
		jLabel99.setBounds(new Rectangle(568, 291, 134, 17));
		jLabel99.setText("");
		this.add(jLabel99);
		
        jLabelAnd4 = new JLabel();
        jLabelAnd4.setBounds(new Rectangle(820, 190, 30, 25));
        jLabelAnd4.setText("AND");
        jLabelAnd4.setVisible(false);
        jLabelAnd3 = new JLabel();
        jLabelAnd3.setBounds(new Rectangle(820, 160, 30, 25));
        jLabelAnd3.setText("AND");
        jLabelAnd3.setVisible(false);
        jLabelOR4 = new JLabel();
        jLabelOR4.setBounds(new Rectangle(770, 190, 30, 25));
        jLabelOR4.setText("OR");
        jLabelOR4.setVisible(false);
        jLabelOR3 = new JLabel();
        jLabelOR3.setBounds(new Rectangle(770, 160, 30, 25));
        jLabelOR3.setText("OR");
        jLabelOR3.setVisible(false);
        jLabelAnd2 = new JLabel();
        jLabelAnd2.setBounds(new Rectangle(820, 130, 30, 25));
        jLabelAnd2.setText("AND");
        jLabelAnd2.setVisible(false);
        jLabelOR2 = new JLabel();
        jLabelOR2.setBounds(new Rectangle(770, 130, 30, 25));
        jLabelOR2.setText("OR");
        jLabelOR2.setVisible(false);
        jLabelAnd1 = new JLabel();
        jLabelAnd1.setBounds(new Rectangle(820, 100, 30, 25));
        jLabelAnd1.setText("AND");
        jLabelAnd1.setVisible(false);
        jLabelOR1 = new JLabel();
        jLabelOR1.setBounds(new Rectangle(770, 100, 30, 25));
        jLabelOR1.setText("OR");
        jLabelOR1.setVisible(false);
        jLabel51 = new JLabel();
        jLabel51.setBounds(new Rectangle(507, 19, 230, 32));
        jLabel51.setText("Advance Search");
        jLabel51.setFont(new Font("Tahoma", 1, 18));
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
       
        
        this.setSize(1000, 700);
        this.setLayout(null);
        jButton1.setText("Search Book");
        jButton1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 0, 0), 3));
        this.add(jButton1,null);
        jButton1.setBounds(30, 20, 140, 40);
		
        jButton2.setText("Search Review");
        jButton2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton2,null);
        jButton2.setBounds(30, 60, 140, 40);

        jButton3.setText("Manage Books");
        jButton3.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton3,null);
        jButton3.setBounds(30, 100, 140, 40);

        jButton4.setText("Manage Catalog");
        jButton4.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton4,null);
        jButton4.setBounds(30, 140, 140, 40);

        jButton5.setText("Manage Reviews");
        jButton5.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton5,null);
        jButton5.setBounds(30, 180, 140, 40);

        jButton6.setText("Logout");
        jButton6.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton6,null);
        jButton6.setBounds(30, 220, 140, 40);

        jButton7.setText("jButton7");
        jButton7.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton7,null);
        jButton7.setBounds(30, 260, 140, 40);

        jButton8.setText("Logout");
        jButton8.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton8,null);
        jButton8.setBounds(30, 300, 140, 40);	
        
        jButton9.setText("Advance Search");
        jButton9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 0, 0), 3));
        this.add(jButton9,null);
        this.add(getTitle0(), null);
        this.add(getTitle(), null);
        this.add(getAuthor(), null);
        this.add(getLanguage(), null);
        this.add(getField(), null);
        this.add(jLabel51, null);
        this.add(getAdSearch(), null);
        this.add(getChoice0(), null);
        this.add(getChoice1(), null);
        this.add(getChoice2(), null);
        this.add(getChoice3(), null);
        this.add(getChoice4(), null);
        this.add(getJRadioOR1(), null);
        this.add(getJRadioAnd1(), null);
        this.add(jLabelOR1, null);
        this.add(jLabelAnd1, null);
        this.add(getJRadioOR2(), null);
        this.add(getJRadioAnd2(), null);
        this.add(jLabelOR2, null);
        this.add(jLabelAnd2, null);
        this.add(getJRadioOR3(), null);
        this.add(getJRadioOR4(), null);
        this.add(getJRadioAnd3(), null);
        this.add(getJRadioAnd4(), null);
        this.add(jLabelOR3, null);
        this.add(jLabelOR4, null);
        this.add(jLabelAnd3, null);
        this.add(jLabelAnd4, null);
        jButton9.setBounds(168, 20, 140, 40);	

        this.add(getJScrollPane1(), null);
        
		privinit();
        jLabel1 = new javax.swing.JLabel();
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Blue1.jpg"))); // NOI18N
        jLabel1.setText("jLabel1");
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
	} // end initialize

	void privinit(){
		/* Setup Connection to server */
        
	       connected = 1;
	   	    try  // trying to connect to server 
	   	    {
	   	    	 client= new ChatClient(u.getHost(),5555,ChatIF);
	   	    } // end try
	   	    catch(IOException exception)
	   	    {
	   	    	JOptionPane.showMessageDialog(null,"Error: Can't setup connection!","Error",JOptionPane.CLOSED_OPTION);
	   	    	connected = 0; // failed to connect
	   	    } // end catch
	        
	   	    /* end setup connection */
	
	   	 /* Reader */
	   	if(u.getPriv() == 0){  
	   		jButton3.setText("Account Settings");
	   		jButton4.setText("Logout");
	   		jButton5.setVisible(false);
	   		jButton6.setVisible(false);
	   		jButton7.setVisible(false);
	   		jButton8.setVisible(false);

	   	}
	   	/* Librarian */
	   	else if(u.getPriv() == 1){
	   		jButton7.setVisible(false);
	   		jButton8.setVisible(false);

	   	
	   	
	   	}
	   	/* Library Manager */
	   else if(u.getPriv() == 2){
   		jButton6.setText("Manage Reports");
  		jButton7.setText("Logout");
		jButton8.setVisible(false);

	   	
	   	}
	   	/* System Manager */
	   	else if(u.getPriv() == 3){
	      		jButton6.setText("Manage Reports");
	   		jButton7.setText("Account Management");
	   		jButton8.setText("Logout");

	   	}
	}// end privinit

	/**
	 * This method initializes Title	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getTitle0() {
		if (SearchField == null) {
			SearchField = new JTextField();
			SearchField.setBounds(new Rectangle(550, 70, 200, 20));
			SearchField.setEditable(false);
		}
		return SearchField;
	}

	/**
	 * This method initializes Title	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getTitle() {
		if (SearchField1 == null) {
			SearchField1 = new JTextField();
			SearchField1.setBounds(new Rectangle(550, 100, 200, 20));
			SearchField1.setEditable(false);
			SearchField1.setVisible(false);
		}
		return SearchField1;
	}

	/**
	 * This method initializes Author	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getAuthor() {
		if (SearchField2 == null) {
			SearchField2 = new JTextField();
			SearchField2.setBounds(new Rectangle(550, 130, 200, 20));
			SearchField2.setEditable(false);
			SearchField2.setVisible(false);
		}
		return SearchField2;
	}

	/**
	 * This method initializes Language	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getLanguage() {
		if (SearchField3 == null) {
			SearchField3 = new JTextField();
			SearchField3.setBounds(new Rectangle(550, 160, 200, 20));
			SearchField3.setEditable(false);
			SearchField3.setVisible(false);
		}
		return SearchField3;
	}

	/**
	 * This method initializes Field	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getField() {
		if (SearchField4 == null) {
			SearchField4 = new JTextField();
			SearchField4.setBounds(new Rectangle(550, 190, 200, 20));
			SearchField4.setEditable(false);
			SearchField4.setVisible(false);
		}
		return SearchField4;
	}

	/**
	 * This method initializes AdSearch	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getAdSearch() {
		if (AdSearch == null) {
			AdSearch = new JButton();
			AdSearch.setBounds(new Rectangle(541, 223, 130, 29));
			AdSearch.setText("Search");
			AdSearch.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					jButtonAdSearchActionPerformed()	;
				}
		});
		}
		return AdSearch;
	}
	
	public void jButtonAdSearchActionPerformed() {// search button was pressed
		int i;
		int j=0;
		int index=1;
		jScrollPane1.setVisible(false);
		
        for(i=this.rows-1;i>=0;i--)
        	model.removeRow(i);
        //****   the same bug from the first FIT test
        //to solve need to zero the rows after cleaning the table
		this.rows=0; // reset last result
		
		if (CheckTextFieldAndChoice())
    {
			
			
	        
			AdvanceSearchBook();	
			
			Book b = new Book();
			
			jScrollPane1.setVisible(false);
			if(b.getResults(0).equals("no results")){
				
				jLabel99.setText("No Results Found:");
				jLabel99.setVisible(true); // show no results found
				

			} // end if
			else{ // display results
		
				Object[] row = new Object[5];
				jLabel99.setText("Results Found:");
				jLabel99.setVisible(true);
	       
		        /*
		         * get number of rows in result table
		         */
		             
		        for(i=1;!b.getResults(i).equals("end1");i++);
		        this.rows=(--i)/5;
		       
		        /*
		         * set results in table
		         */
		        
		        for(i=0;i<rows;i++){//for1     
		        	for(j=0;j<5;j++,index++){//for2
		        		row[j]=b.getResults(index);	        	      	
		        	}//end for2        	
		            model.addRow(row);            
		        }//end for1
		        jScrollPane1.setVisible(true);
		        jScrollPane1.setBounds(400, 330, 440,17*(rows+1));	// set table size
			}// end else
		 }else
			 jLabel99.setText("***<<-you didn't type->>***");
    }

public void AdvanceSearchBook(){ // this function will connect to server to search reviews
		
    	obj logdt1 = new obj();
    	logdt1.setType("AdSearch Book");
    	
    	logdt1.setParam(0,choice0.getSelectedItem());
    	logdt1.setParam(1,SearchField.getText()); 
    	
    	if (jRadioOR1.isSelected()) 
    		logdt1.setParam(2,"or");
    	else
    		logdt1.setParam(2,"and");
    	
    	logdt1.setParam(3,choice1.getSelectedItem());
    	logdt1.setParam(4,SearchField1.getText()); 
    	
    	if (jRadioOR2.isSelected()) 
    		logdt1.setParam(5,"or");
    	else
    		logdt1.setParam(5,"and");
    	
    	logdt1.setParam(6,choice2.getSelectedItem());
    	logdt1.setParam(7,SearchField2.getText()); 
    	
    	if (jRadioOR3.isSelected()) 
    		logdt1.setParam(8,"or");
    	else
    		logdt1.setParam(8,"and");
    	
    	logdt1.setParam(9,choice3.getSelectedItem());
    	logdt1.setParam(10,SearchField3.getText()); 
    	
    	if (jRadioOR4.isSelected()) 
    		logdt1.setParam(11,"or");
    	else
    		logdt1.setParam(11,"and");
    	
    	logdt1.setParam(12,choice4.getSelectedItem());
    	logdt1.setParam(13,SearchField4.getText()); 
    	
    	    
    	if(connected == 1)	
   	    	client.handleMessageFromClientUI(logdt1);
    	
	    try {
            Thread.sleep(1000); 
            } // end try
	    catch (InterruptedException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } // end catch
    
    	/* end sql  */
		
}
	/**
	 * This method initializes choice1	
	 * 	
	 * @return java.awt.Choice	
	 */
	private Choice getChoice0() {
		if (choice0 == null) {
			choice0 = new Choice();
			choice0.setBounds(new Rectangle(450, 70, 100, 25));
			choice0.addItemListener(new java.awt.event.ItemListener() {
				public void itemStateChanged(java.awt.event.ItemEvent e) {
                  if (choice0.getSelectedIndex() == 0)
                  {
                	  
                	  jRadioAnd1.setVisible(false);
                      jRadioOR1.setVisible(false);
                      jLabelAnd1.setVisible(false);
                      jLabelOR1.setVisible(false);
                      jRadioAnd2.setVisible(false);
                      jRadioOR2.setVisible(false);
                      jLabelAnd2.setVisible(false);
                      jLabelOR2.setVisible(false);
                      jRadioAnd3.setVisible(false);
                      jRadioOR3.setVisible(false);
                      jLabelAnd3.setVisible(false);
                      jLabelOR3.setVisible(false);
                      jRadioAnd4.setVisible(false);
                      jRadioOR4.setVisible(false);
                      jLabelAnd4.setVisible(false);
                      jLabelOR4.setVisible(false);
                                            
                      SearchField.setEditable(false);
                      SearchField1.setVisible(false);
                      SearchField2.setVisible(false);
                      SearchField3.setVisible(false);
                      SearchField4.setVisible(false);
                      
                      SearchField.setText("");
                      SearchField1.setText("");
                      SearchField2.setText("");
                      SearchField3.setText("");
                      SearchField4.setText("");
                      
                      choice1.select(0);
                      choice2.select(0);
                      choice3.select(0);
                      choice4.select(0);
                      
                      choice1.setVisible(false);
                	  choice2.setVisible(false);
                	  choice3.setVisible(false);
                	  choice4.setVisible(false);
                     
                                            
                      
                  }
                  else
                  {
                	  
                	  SearchField.setEditable(true);
                	  SearchField1.setVisible(true);
                	  SearchField1.setEditable(false);
                	  choice1.setVisible(true);
                  }
                	  
				}
			});
			choice0.insert("None", 0);
			choice0.insert("Title", 1);
			choice0.insert("Author", 2);
			choice0.insert("Language", 3);
			choice0.insert("Field", 4);
			choice0.insert("Subject", 5);
			choice0.insert("Keyword", 6);
		
			
		}
		return choice0;
	}

	/**
	 * This method initializes choice1	
	 * 	
	 * @return java.awt.Choice	
	 */
	private Choice getChoice1() {
		if (choice1 == null) {
			choice1 = new Choice();
			choice1.setBounds(new Rectangle(450, 100, 100, 25));
			choice1.addItemListener(new java.awt.event.ItemListener() {
				public void itemStateChanged(java.awt.event.ItemEvent e) {
                  if (choice1.getSelectedIndex() == 0)
                  {
                	  
                	  jRadioAnd1.setVisible(false);
                      jRadioOR1.setVisible(false);
                      jLabelAnd1.setVisible(false);
                      jLabelOR1.setVisible(false);
                      jRadioAnd2.setVisible(false);
                      jRadioOR2.setVisible(false);
                      jLabelAnd2.setVisible(false);
                      jLabelOR2.setVisible(false);
                      jRadioAnd3.setVisible(false);
                      jRadioOR3.setVisible(false);
                      jLabelAnd3.setVisible(false);
                      jLabelOR3.setVisible(false);
                      jRadioAnd4.setVisible(false);
                      jRadioOR4.setVisible(false);
                      jLabelAnd4.setVisible(false);
                      jLabelOR4.setVisible(false);
                      
                      SearchField1.setEditable(false);
                      SearchField2.setVisible(false);
                      SearchField3.setVisible(false);
                      SearchField4.setVisible(false);
                      
                      SearchField1.setText("");
                      SearchField2.setText("");
                      SearchField3.setText("");
                      SearchField4.setText("");
                      
                      choice2.select(0);
                      choice3.select(0);
                      choice4.select(0);
                      
                	  choice2.setVisible(false);
                	  choice3.setVisible(false);
                	  choice4.setVisible(false);
                	  
                  }
                  else
                  {
                	  jRadioAnd1.setVisible(true);
                	  jRadioAnd1.setSelected(false);
                	  jRadioOR1.setVisible(true);
                	  jRadioOR1.setSelected(true);
                      jLabelAnd1.setVisible(true);
                      jLabelOR1.setVisible(true);
                      SearchField1.setEditable(true);
                   	  SearchField2.setVisible(true);
                	  SearchField2.setEditable(false);
                	  choice2.setVisible(true);
                  }
                	  
				}
			});
			choice1.insert("None", 0);
			choice1.insert("Title", 1);
			choice1.insert("Author", 2);
			choice1.insert("Language", 3);
			choice1.insert("Field", 4);
			choice1.insert("Subject", 5);
			choice1.insert("Keyword", 6);
			choice1.setVisible(false);
			
		}
		return choice1;
	}

	/**
	 * This method initializes choice2	
	 * 	
	 * @return java.awt.Choice	
	 */
	private Choice getChoice2() {
		if (choice2 == null) {
			choice2 = new Choice();
			choice2.setBounds(new Rectangle(450, 130, 100, 25));
			choice2.addItemListener(new java.awt.event.ItemListener() {
				public void itemStateChanged(java.awt.event.ItemEvent e) {
                  if (choice2.getSelectedIndex() == 0)
                  {
                	  jRadioAnd2.setVisible(false);
                      jRadioOR2.setVisible(false);
                      jLabelAnd2.setVisible(false);
                      jLabelOR2.setVisible(false);
                      jRadioAnd3.setVisible(false);
                      jRadioOR3.setVisible(false);
                      jLabelAnd3.setVisible(false);
                      jLabelOR3.setVisible(false);
                      jRadioAnd4.setVisible(false);
                      jRadioOR4.setVisible(false);
                      jLabelAnd4.setVisible(false);
                      jLabelOR4.setVisible(false);
                      
                      SearchField2.setEditable(false);
                      SearchField3.setVisible(false);
                      SearchField4.setVisible(false);
                      
                      SearchField2.setText("");
                      SearchField3.setText("");
                      SearchField4.setText("");

                      choice3.select(0);
                      choice4.select(0);
                      

                	  choice3.setVisible(false);
                	  choice4.setVisible(false);
                	  
                  }
                  else
                  {
                	  jRadioAnd2.setVisible(true);
                	  jRadioAnd2.setSelected(false);
                      jRadioOR2.setVisible(true);
                      jRadioOR2.setSelected(true);
                      jLabelAnd2.setVisible(true);
                      jLabelOR2.setVisible(true);
                      SearchField2.setEditable(true);
                      SearchField3.setVisible(true);
                      SearchField3.setEditable(false);
                	  choice3.setVisible(true);
                  }
                	  
				}
			});
			choice2.insert("None", 0);
			choice2.insert("Title", 1);
			choice2.insert("Author", 2);
			choice2.insert("Language", 3);
			choice2.insert("Field", 4);
			choice2.insert("Subject", 5);
			choice2.insert("Keyword", 6);
			choice2.setVisible(false);
		}
		return choice2;
	}

	/**
	 * This method initializes choice3	
	 * 	
	 * @return java.awt.Choice	
	 */
	private Choice getChoice3() {
		if (choice3 == null) {
			choice3 = new Choice();
			choice3.setBounds(new Rectangle(450, 160, 100, 25));
			choice3.addItemListener(new java.awt.event.ItemListener() {
				public void itemStateChanged(java.awt.event.ItemEvent e) {
                  if (choice3.getSelectedIndex() == 0)
                  {
                	  
                      jRadioAnd3.setVisible(false);
                      jRadioOR3.setVisible(false);
                      jLabelAnd3.setVisible(false);
                      jLabelOR3.setVisible(false);
                      jRadioAnd4.setVisible(false);
                      jRadioOR4.setVisible(false);
                      jLabelAnd4.setVisible(false);
                      jLabelOR4.setVisible(false);
                      
                      SearchField3.setEditable(false);
                      SearchField4.setVisible(false);
                      
                      SearchField3.setText("");
                      SearchField4.setText("");

                      choice4.select(0);
                	  choice4.setVisible(false);
                	  
                	  
                  }
                  else
                  {
                	  jRadioAnd3.setVisible(true);
                	  jRadioAnd3.setSelected(false);
                      jRadioOR3.setVisible(true);
                      jRadioOR3.setSelected(true);
                      jLabelAnd3.setVisible(true);
                      jLabelOR3.setVisible(true);
                      SearchField3.setEditable(true);
                      SearchField4.setVisible(true);
                      SearchField4.setEditable(false);
                	  choice4.setVisible(true);
                  }
                	  
				}
			});
			choice3.insert("None", 0);
			choice3.insert("Title", 1);
			choice3.insert("Author", 2);
			choice3.insert("Language", 3);
			choice3.insert("Field", 4);
			choice3.insert("Subject", 5);
			choice3.insert("Keyword", 6);
			choice3.setVisible(false);
		}
		return choice3;
	}

	/**
	 * This method initializes choice4	
	 * 	
	 * @return java.awt.Choice	
	 */
	private Choice getChoice4() {
		if (choice4 == null) {
			choice4 = new Choice();
			choice4.setBounds(new Rectangle(450, 190, 100, 25));
			choice4.addItemListener(new java.awt.event.ItemListener() {
				public void itemStateChanged(java.awt.event.ItemEvent e) {
                  if (choice4.getSelectedIndex() == 0)
                  {
                	  jRadioAnd4.setVisible(false);
                      jRadioOR4.setVisible(false);
                      jLabelAnd4.setVisible(false);
                      jLabelOR4.setVisible(false);
                      SearchField4.setEditable(false);
                      SearchField4.setText("");
                  }
                  else
                  {
                	  jRadioAnd4.setVisible(true);
                	  jRadioAnd4.setSelected(false);
                      jRadioOR4.setVisible(true);
                      jRadioOR4.setSelected(true);
                      jLabelAnd4.setVisible(true);
                      jLabelOR4.setVisible(true);
                      SearchField4.setEditable(true);
                  }
                	  
				}
			});
			choice4.insert("None", 0);
			choice4.insert("Title", 1);
			choice4.insert("Author", 2);
			choice4.insert("Language", 3);
			choice4.insert("Field", 4);
			choice4.insert("Subject", 5);
			choice4.insert("Keyword", 6);
			choice4.setVisible(false);
		}
		return choice4;
	}

	/**
	 * This method initializes jRadioOR1	
	 * 	
	 * @return javax.swing.JRadioButton	
	 */
	private JRadioButton getJRadioOR1() {
		if (jRadioOR1 == null) {
			jRadioOR1 = new JRadioButton();
			jRadioOR1.setBounds(new Rectangle(750, 100, 21, 21));
			jRadioOR1.setOpaque(false);
			jRadioOR1.setSelected(true);
			jRadioOR1.addItemListener(new java.awt.event.ItemListener() {
				public void itemStateChanged(java.awt.event.ItemEvent e) {
					if (jRadioOR1.isSelected())
						jRadioAnd1.setSelected(false); 		
				}
			});
			jRadioOR1.setVisible(false);

		}
		return jRadioOR1;
	}

	/**
	 * This method initializes jRadioAnd1	
	 * 	
	 * @return javax.swing.JRadioButton	
	 */
	private JRadioButton getJRadioAnd1() {
		if (jRadioAnd1 == null) {
			jRadioAnd1 = new JRadioButton();
			jRadioAnd1.setBounds(new Rectangle(800, 100, 21, 21));
			jRadioAnd1.setOpaque(false);
			jRadioAnd1.addItemListener(new java.awt.event.ItemListener() {
				public void itemStateChanged(java.awt.event.ItemEvent e) {
               if (jRadioAnd1.isSelected())
            	   jRadioOR1.setSelected(false);
				}
			});
			jRadioAnd1.setVisible(false);
		}
		return jRadioAnd1;
	}

	/**
	 * This method initializes jRadioOR2	
	 * 	
	 * @return javax.swing.JRadioButton	
	 */
	private JRadioButton getJRadioOR2() {
		if (jRadioOR2 == null) {
			jRadioOR2 = new JRadioButton();
			jRadioOR2.setBounds(new Rectangle(750, 130, 21, 21));
			jRadioOR2.setSelected(true);
			jRadioOR2.setOpaque(false);
			jRadioOR2.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					if (jRadioOR2.isSelected()) 
	                     jRadioAnd2.setSelected(false);					}
			});
			jRadioOR2.setVisible(false);
		}
		return jRadioOR2;
	}

	/**
	 * This method initializes jRadioAnd2	
	 * 	
	 * @return javax.swing.JRadioButton	
	 */
	private JRadioButton getJRadioAnd2() {
		if (jRadioAnd2 == null) {
			jRadioAnd2 = new JRadioButton();
			jRadioAnd2.setBounds(new Rectangle(800, 130, 21, 21));
			jRadioAnd2.setOpaque(false);
			jRadioAnd2.addItemListener(new java.awt.event.ItemListener() {
				public void itemStateChanged(java.awt.event.ItemEvent e) {
                   if (jRadioAnd2.isSelected()) 
                     jRadioOR2.setSelected(false);	   
				}
			});
			jRadioAnd2.setVisible(false);
		}
		return jRadioAnd2;
	}

	/**
	 * This method initializes jRadioOR3	
	 * 	
	 * @return javax.swing.JRadioButton	
	 */
	private JRadioButton getJRadioOR3() {
		if (jRadioOR3 == null) {
			jRadioOR3 = new JRadioButton();
			jRadioOR3.setBounds(new Rectangle(750, 160, 21, 21));
			jRadioOR3.setSelected(true);
			jRadioOR3.setOpaque(false);
			jRadioOR3.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					if (jRadioOR3.isSelected()) 
	                     jRadioAnd3.setSelected(false);					}
			});
			jRadioOR3.setVisible(false);
		}
		return jRadioOR3;
	}

	/**
	 * This method initializes jRadioOR4	
	 * 	
	 * @return javax.swing.JRadioButton	
	 */
	private JRadioButton getJRadioOR4() {
		if (jRadioOR4 == null) {
			jRadioOR4 = new JRadioButton();
			jRadioOR4.setBounds(new Rectangle(750, 190, 21, 21));
			jRadioOR4.setSelected(true);
			jRadioOR4.setOpaque(false);
			jRadioOR4.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					if (jRadioOR4.isSelected()) 
	                     jRadioAnd4.setSelected(false);					}
			});
			jRadioOR4.setVisible(false);
		}
		return jRadioOR4;
	}

	/**
	 * This method initializes jRadioAnd3	
	 * 	
	 * @return javax.swing.JRadioButton	
	 */
	private JRadioButton getJRadioAnd3() {
		if (jRadioAnd3 == null) {
			jRadioAnd3 = new JRadioButton();
			jRadioAnd3.setBounds(new Rectangle(800, 160, 21, 21));
			jRadioAnd3.setOpaque(false);
			jRadioAnd3.addItemListener(new java.awt.event.ItemListener() {
				public void itemStateChanged(java.awt.event.ItemEvent e) {
                   if (jRadioAnd3.isSelected()) 
                     jRadioOR3.setSelected(false);
				}
			});
			jRadioAnd3.setVisible(false);
		}
		return jRadioAnd3;
	}

	/**
	 * This method initializes jRadioAnd4	
	 * 	
	 * @return javax.swing.JRadioButton	
	 */
	private JRadioButton getJRadioAnd4() {
		if (jRadioAnd4 == null) {
			jRadioAnd4 = new JRadioButton();
			jRadioAnd4.setBounds(new Rectangle(800, 190, 21, 21));
			jRadioAnd4.setOpaque(false);
			jRadioAnd4.addItemListener(new java.awt.event.ItemListener() {
				public void itemStateChanged(java.awt.event.ItemEvent e) {
                   if (jRadioAnd4.isSelected()) 
                     jRadioOR4.setSelected(false);
				}
			});
			jRadioAnd4.setVisible(false);
		}
		return jRadioAnd4;
	}
	
	boolean CheckTextFieldAndChoice()
	{
		if (choice0.getSelectedIndex() != 0 )
			if (SearchField.getText().isEmpty())
				return false;
		if (choice1.getSelectedIndex() != 0 )
			if (SearchField1.getText().isEmpty())
				return false;
		if (choice2.getSelectedIndex() != 0 )
			if (SearchField2.getText().isEmpty())
				return false;
		if (choice3.getSelectedIndex() != 0 )
			if (SearchField3.getText().isEmpty())
				return false;
		if (choice4.getSelectedIndex() != 0 )
			if (SearchField4.getText().isEmpty())
				return false;
		return true;
		
	}
	
	/**
	 * This method initializes jScrollPane1	
	 * 	
	 * @return javax.swing.JScrollPane	
	 */
	private JScrollPane getJScrollPane1() {
		if (jScrollPane1 == null) {
			jScrollPane1 = new JScrollPane();
			jScrollPane1.setBounds(new Rectangle(360, 400, 453, 420));
			jScrollPane1.setViewportView(getJTable());
			jScrollPane1.setVisible(false);
		}
		return jScrollPane1;
	}

	/**
	 * This method initializes jTable1	
	 * 	
	 * @return javax.swing.JTable	
	 */
	public JTable getJTable() {
		if (table == null) {     
	        table = new JTable(getModel());	
	        table.setPreferredScrollableViewportSize(new Dimension(300, 200));
	        table.setFillsViewportHeight(true);
	        model.removeRow(1);
	        model.removeRow(0);
	        
	        /*
	         *     table alignment 
	         */
	        
	        DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
	        dtcr.setHorizontalAlignment(SwingConstants.CENTER);
	        table.getColumn("ISBN").setCellRenderer(dtcr);
	        table.getColumn("Title").setCellRenderer(dtcr);
	        table.getColumn("Language").setCellRenderer(dtcr);
	        table.getColumn("Rating").setCellRenderer(dtcr);
	        table.getColumn("Price").setCellRenderer(dtcr);	        
	        
	        
		}// end if
		return table;
	}// end getTable

	
	public TableModel getModel(){
	if (model == null) {
	    String[] columnNames = {"ISBN","Title","Language","Rating","Price"};    
        Object[][] data = {{"","","","",""},{"","","","",""}};
        model = new DefaultTableModel(data,columnNames);
		}// end if
		return model;
	}// end getModel
	

	
}  //  @jve:decl-index=0:visual-constraint="156,12" // end Class
