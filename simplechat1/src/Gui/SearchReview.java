package Gui;

import java.awt.Dimension;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Rectangle;

import javax.swing.event.ListSelectionEvent;
import javax.swing.table.TableModel;


import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Rectangle;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.EventListener;



import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;

import common.ChatIF;

import Gui.*;
import logic.*;
import client.*;



public class SearchReview extends JPanel {
	
	  private JLabel jLabel666 = null;	
	private javax.swing.JLabel jLabel1;
	public javax.swing.JButton jButton1;
    public javax.swing.JButton jButton2;
    public javax.swing.JButton jButton3;
    public javax.swing.JButton jButton4;
    public javax.swing.JButton jButton5;
    public javax.swing.JButton jButton6;
    public javax.swing.JButton jButton7;
    public javax.swing.JButton jButton8;
    User u = new User();
   
    
    public javax.swing.JLabel jLabel3;
    public javax.swing.JLabel jLabel51;
    public javax.swing.JTextField jText1;
    public javax.swing.JLabel jLabel11;
    private javax.swing.JComboBox jComboBox1;
    public javax.swing.JButton jButton10;
    public javax.swing.JTable jTable1;
    public javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel jLabel5;
    public javax.swing.table.DefaultTableModel model=null;
    public javax.swing.JScrollPane scrollPane=null;
    public javax.swing.JTable table1;
    public javax.swing.JTable table=null;
    
    	/*  show review buttons  */
    public javax.swing.JLabel jLabel10; // Review
    public javax.swing.JLabel jLabel12; // from:
    public javax.swing.JLabel jLabel13; // date
    public javax.swing.JLabel jLabel14; //book details
    public javax.swing.JLabel jLabel15; //title
    public javax.swing.JLabel jLabel16; //isbn
    public javax.swing.JLabel jLabel17; // rating
    public javax.swing.JLabel jLabel18; // review
   
    
    
    public static ChatClient client;
    private  ChatIF ChatIF = null;
    public int connected=1;
    public int rows=0;
    
	
    
    
    
	/**
	 * This is the default constructor
	 */
	public SearchReview() {
		super();
		initialize();
	}

	/**
	 * This method initializes StudentForm
	 */
	public void initialize() {
	
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();

        jLabel51 = new javax.swing.JLabel();
        jText1 = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        jButton10 = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel(); // Review
        jLabel12 = new javax.swing.JLabel(); // from:
        jLabel13 = new javax.swing.JLabel(); // date
        jLabel14 = new javax.swing.JLabel(); //book details
        jLabel15 = new javax.swing.JLabel(); //title
        jLabel16 = new javax.swing.JLabel(); //isbn
        jLabel17 = new javax.swing.JLabel(); // rating
        jLabel18 = new javax.swing.JLabel(); // review
        
        jScrollPane1 = new javax.swing.JScrollPane();
        jLabel5 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        
        
        
        
 
        
        /* Create Table with ScrollPane */  
 
        scrollPane = new JScrollPane(getTable());    
        add(scrollPane);
        scrollPane.setVisible(true);
//    //    scrollPane.setBounds(300, 270, 440,17*(8+1));	// set table size


        
        this.setSize(1000, 700);
        this.setLayout(null);
        jButton1.setText("Search Book");
        jButton1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton1,null);
        jButton1.setBounds(30, 20, 140, 40);
		
        jButton2.setText("Search Review");
        jButton2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 0, 0), 3));
        this.add(jButton2,null);
        jButton2.setBounds(30, 60, 140, 40);

        jButton3.setText("Manage Books");
        jButton3.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton3,null);
        jButton3.setBounds(30, 100, 140, 40);

        jButton4.setText("Manage Catalog");
        jButton4.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton4,null);
        jButton4.setBounds(30, 140, 140, 40);

        jButton5.setText("Manage Reviews");
        jButton5.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton5,null);
        jButton5.setBounds(30, 180, 140, 40);

        jButton6.setText("Logout");
        jButton6.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton6,null);
        jButton6.setBounds(30, 220, 140, 40);

        jButton7.setText("jButton7");
        jButton7.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton7,null);
        jButton7.setBounds(30, 260, 140, 40);

        jButton8.setText("Logout");
        jButton8.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton8,null);
        jButton8.setBounds(30, 300, 140, 40);	
		
		privinit();
	
		
		
		
    	/* Setup Connection to server */
        
	 
	   	    try  // trying to connect to server 
	   	    {
	   	    	 client= new ChatClient(u.getHost(),5555,ChatIF);
	   	    } // end try
	   	    catch(IOException exception)
	   	    {
	   	    	JOptionPane.showMessageDialog(null,"Error: Can't setup connection!","Error",JOptionPane.CLOSED_OPTION);
	   	    	connected = 0; // failed to connect
	   	    } // end catch
	        
	   	    /* end setup connection */
	   	    
	   	    
	        /*
	         *   	 manage buttons and labels 
	         */
	        
	        jLabel51.setFont(new java.awt.Font("Tahoma", 1, 18));
	        jLabel51.setText("Search Review");
	        this.add(jLabel51);
	        jLabel51.setBounds(460, 50, 250, 22);
	        
	        
	        
	   	    
	        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 15));  // filter jLabel11
	        jLabel11.setText("Keywords: ");
	        this.add(jLabel11);
	        jLabel11.setBounds(210, 165, 100, 15);

	        
	        
	        
	        this.add(jText1);  // set search field
	        jText1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));
	        jText1.setBounds(300,160,300,25);
    
	        
	        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "All", "Title", "Author", "Keyword" }));
	        add(jComboBox1);
	        jComboBox1.setBounds(610, 160, 100, 25);
	   	    
	        jButton10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/log-arrlog.gif"))); // NOI18N
	        jButton10.setText("Search ");
	        jButton10.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
	        jButton10.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
	        this.add(jButton10);
	        jButton10.setBounds(720, 160,100, 25);
	        jButton10.addActionListener(new java.awt.event.ActionListener() {
	            public void actionPerformed(java.awt.event.ActionEvent evt) {
	                jButton10ActionPerformed(evt);
	            }
	        });
	        
	        
	        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12));  // filter jLabel11
	        jLabel5.setText("No Results Found ");
	        this.add(jLabel5);
	        jLabel5.setBounds(460, 235, 200, 15);
	        jLabel5.setVisible(false);
	        
	   /*
	    * Listeners     
	    */
	        addHierarchyBoundsListener(new java.awt.event.HierarchyBoundsListener() {
	            public void ancestorMoved(java.awt.event.HierarchyEvent evt) {
	            }
	            public void ancestorResized(java.awt.event.HierarchyEvent evt) {
	                formAncestorResized(evt);
	            }
	        });

	        /*
	         *		Show Review Buttons  ( hidden at first )
	         */
	        
	        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 18));
	        jLabel10.setText("Review");
	        this.add(jLabel10);
	        jLabel10.setBounds(460, 50, 250, 22); 
	        jLabel10.setVisible(false);
	        

	        jLabel12.setText("From: ");
	        this.add(jLabel12);
	        jLabel12.setBounds(410, 130, 130, 14);	
	        jLabel12.setVisible(false);
	        

	        jLabel13.setText("Date: ");
	        this.add(jLabel13);
	        jLabel13.setBounds(570, 130, 150, 14);
	        jLabel13.setVisible(false);
	
	        jLabel14.setText("Book Details:");
	        this.add(jLabel14);
	        jLabel14.setBounds(410, 160, 240, 14);
	        jLabel14.setVisible(false);
	        
	        
	        jLabel15.setText("Title: ");
	        this.add(jLabel15);
	        jLabel15.setBounds(410, 190, 500, 14);     
	        jLabel15.setVisible(false);
	        
	        jLabel16.setText("ISBN: ");
	        this.add(jLabel16);
	        jLabel16.setBounds(410, 220, 250, 14);
	        jLabel16.setVisible(false);
	        
	        
	        jLabel17.setText("Rating: ");
	        add(jLabel17);
	        jLabel17.setBounds(570,220,200,14);
	        jLabel17.setVisible(false);
	        
	        
	        jLabel18.setText("");  //review
	        this.add(jLabel18);
	        jLabel18.setBounds(410, 240, 270, 180);
	        jLabel18.setVisible(false);
	        

	        
	   	    
		/* Create Background */
		
        jLabel1 = new javax.swing.JLabel();
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Blue1.jpg"))); // NOI18N
        jLabel1.setText("jLabel1");
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
		/* end Background */
		
   
	} // end initialize

	void privinit(){
	   	 /* Reader */
	   	if(u.getPriv() == 0){  
	   		jButton3.setText("Account Settings");
	   		jButton4.setText("Logout");
	   		jButton5.setVisible(false);
	   		jButton6.setVisible(false);
	   		jButton7.setVisible(false);
	   		jButton8.setVisible(false);
	   	}
	   	/* Librarian */
	   	else if(u.getPriv() == 1){
	   		jButton7.setVisible(false);
	   		jButton8.setVisible(false);
	   	
	   	
	   	}
	   	/* Library Manager */
	   	else if(u.getPriv() == 2){
	   		jButton6.setText("Manage Reports");
	   		jButton7.setText("Logout");
	   		jButton8.setVisible(false);
	   	
	   	}
	   	/* System Manager */
	   	else if(u.getPriv() == 3){
	      		jButton6.setText("Manage Reports");
	   		jButton7.setText("Account Management");
	   		jButton8.setText("Logout");
	   	}
	}// end privinit
	
	
	
	
	
	
	
	
	private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {// search button was pressed
        String input;
        String filter;
        int i;
		int j=0;
		int index=1;
   
		/*
         *    remove old results
         */
		scrollPane.setVisible(false);
		jLabel3.setVisible(false);
        for(i=this.rows-1;i>=0;i--)
        	model.removeRow(i);
		

		if(!jText1.getText().isEmpty()){ // if there is any input in text field
			
			input=jText1.getText(); // get text from text field
			filter=(String)jComboBox1.getSelectedItem(); // get filter 
		
			if(filter.equals("All")){ // Change All string to Filter
				filter = "Filter";
			}
			
			
			
			searchReviews(input,filter); // search reviews
		
		Review r = new Review();
		System.out.println(r.getResults(0)+r.getResults(1)+r.getResults(2));
		if(r.getResults(1).equals("end")){
			jLabel5.setText("No Results Found:");
			jLabel5.setVisible(true); // show no results found
			this.rows=0; // reset last result
		} // end if
		else{ // display results
	
			Object[] row = new Object[4];
			jLabel5.setText("Results Found:");
			jLabel5.setVisible(true);
       
	        /*
	         * get number of rows in result table
	         */
	             
	        for(i=1;!r.getResults(i).equals("end");i++);
	        this.rows=(--i)/4;
	       
	        /*
	         * set results in table
	         */
	        
	        for(i=0;i<rows;i++){//for1     
	        	for(j=0;j<4;j++,index++){//for2
	        		row[j]=r.getResults(index);	        	      	
	        	}//end for2        	
	            model.addRow(row);            
	        }//end for1
	        scrollPane.setVisible(true);
	        scrollPane.setBounds(300, 270, 440,17*(rows+1));	// set table size
		}// end else
	 }// end if (!jText1...
		
   }//end event Search button
	

	
	
	
	
	
	public void searchReviews(String input, String filter){ // this function will connect to server to search reviews
		
    	obj logdt1 = new obj();
    	logdt1.setType("search reviews");
    	logdt1.setParam(0,input); // set input from text field
    	logdt1.setParam(1,filter); // set filter
    	
       	if(connected == 1)	
   	    	client.handleMessageFromClientUI(logdt1);
    	
	    try {
            Thread.sleep(2000); 
            } // end try
	    catch (InterruptedException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } // end catch
    
    	/* end sql  */

	}// end searchReviews
	
	private void formAncestorResized(java.awt.event.HierarchyEvent evt) { // resize window listener
    
		//table.revalidate();
		
    }// end resize window listener
	

	public JTable getTable() {
		if (table == null) {     
	        table = new JTable(getModel());	
	        table.setPreferredScrollableViewportSize(new Dimension(300, 70));
	        table.setFillsViewportHeight(true);
	        model.removeRow(1);
	        model.removeRow(0);
	        
	        /*
	         *     table alignment 
	         */
	        
	        DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
	        dtcr.setHorizontalAlignment(SwingConstants.CENTER);
	        table.getColumn("ISBN").setCellRenderer(dtcr);
	        table.getColumn("Username").setCellRenderer(dtcr);
	        table.getColumn("Date").setCellRenderer(dtcr);
	        table.getColumn("Rating").setCellRenderer(dtcr);
	        
	        
		}// end if
		return table;
	}// end getTable

	
	public TableModel getModel(){
	if (model == null) {
	    String[] columnNames = {"ISBN","Username","Date","Rating",};    
        Object[][] data = {{"","","",""},{"","","",""}};
        model = new DefaultTableModel(data,columnNames);
		}// end if
		return model;
	}// end getModel
		

	
} // end Class
