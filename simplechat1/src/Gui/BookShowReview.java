package Gui;

import java.awt.Dimension;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Rectangle;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Rectangle;
import java.io.IOException;

import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import common.ChatIF;

import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.text.SimpleDateFormat;

import Gui.*;
import logic.*;
import client.*;



public class BookShowReview extends JPanel {
	  private JLabel jLabel666 = null;
    private javax.swing.JLabel jLabel5;
	 
    public static ChatClient client;
    private  ChatIF ChatIF = null;
    public int connected=1;
    public int rows=0;
    
	private javax.swing.JLabel jLabel1;
	public javax.swing.JButton jButton1;
   
    User u = new User();
    Book b = new Book();
    Review r = new Review();
    
    public javax.swing.JTable table=null;
    public javax.swing.JScrollPane scrollPane;
    public javax.swing.table.DefaultTableModel model=null;
    
    public javax.swing.JButton Button1; //Details
    public javax.swing.JButton Button2; //Reviews
    public javax.swing.JButton Button3;	//Write Reveiw
    public javax.swing.JButton Button4; //Buy

	private JButton jButton = null;
 
    
		/*  show review buttons  */
    

    
//	public static final String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";


    
	/**
	 * This is the default constructor
	 */
	public BookShowReview() {
		super();
		initialize();
	}

	/**
	 * This method initializes StudentForm
	 */
	private void initialize() {
	
		
		
        scrollPane = new JScrollPane(getTable());    
        add(scrollPane);
        scrollPane.setVisible(true);
		
        jButton1 = new javax.swing.JButton();
        
       
        Button1 = new javax.swing.JButton();
        Button2 = new javax.swing.JButton();
        Button3 = new javax.swing.JButton();
        Button4 = new javax.swing.JButton();
       
        
        this.setSize(1000, 700);
        this.setLayout(null);
    
        
        
        jButton1.setText("Back");
        jButton1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton1,null);
        jButton1.setBounds(30, 20, 140, 40);
		
        /*
         *    Write Review Buttons
         */
        
        Button1.setText("Details");
        Button1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(Button1);
        Button1.setBounds(300,50, 110, 29);
        
        Button2.setText("Reviews");
        Button2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 0, 0), 3));
        this.add(Button2);
        Button2.setBounds(410,50, 110, 29);
        
        Button3.setText("Write Review");
        Button3.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(Button3);
        Button3.setBounds(520,50, 110, 29);
        
        Button4.setText("Buy");
        Button4.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(Button4);
        Button4.setBounds(630,50, 110, 29);
        
        jLabel5 = new javax.swing.JLabel();
        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12));  // filter jLabel11
        jLabel5.setText("No Results Found ");
        this.add(jLabel5);

        jLabel5.setBounds(460, 235, 200, 15);
        jLabel5.setVisible(false);

        
		privinit();
	


		/* Create Background */
		
        jLabel1 = new javax.swing.JLabel();
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Blue1.jpg"))); // NOI18N
        jLabel1.setText("jLabel1");
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
		/* end Background */
	} // end initialize

	void privinit(){
		
		 try  // trying to connect to server 
	   	    {
	   	    	 client= new ChatClient(u.getHost(),5555,ChatIF);
	   	    } // end try
	   	    catch(IOException exception)
	   	    {
	   	    	JOptionPane.showMessageDialog(null,"Error: Can't setup connection!","Error",JOptionPane.CLOSED_OPTION);
	   	    	connected = 0; // failed to connect
	   	    } // end catch
	        
	   	    /* end setup connection */
	   	 
	}// end privinit
	public JTable getTable() {
		if (table == null) {     
	        table = new JTable(getModel());	
	        table.setPreferredScrollableViewportSize(new Dimension(300, 70));
	        table.setFillsViewportHeight(true);
	        model.removeRow(1);
	        model.removeRow(0);
	        
	        /*
	         *     table alignment 
	         */
	        
	        DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
	        dtcr.setHorizontalAlignment(SwingConstants.CENTER);
	        table.getColumn("ISBN").setCellRenderer(dtcr);
	        table.getColumn("Username").setCellRenderer(dtcr);
	        table.getColumn("Date").setCellRenderer(dtcr);
	        table.getColumn("Rating").setCellRenderer(dtcr);
	        
	        
		}// end if
		return table;
	}// end getTable

	
	public TableModel getModel(){
	if (model == null) {
	    String[] columnNames = {"ISBN","Username","Date","Rating",};    
        Object[][] data = {{"","","",""},{"","","",""}};
        model = new DefaultTableModel(data,columnNames);
		}// end if
		return model;
	}// end getModel
		
	
	public void setResults(Book b)
	{
		this.b = b;
		
        int i;
		int j=0;
		int index=1;
   
		/*
         *    remove old results
         */
		scrollPane.setVisible(false);
        for(i=this.rows-1;i>=0;i--)
        	model.removeRow(i);
		
		
        SearchReviewsOfBook(); // search reviews
		
		Review r = new Review();
		
		if(r.getResults(0).equals("no results")){
			jLabel5.setText("No Results Found:");
			jLabel5.setVisible(true); // show no results found
			this.rows=0; // reset last result
		} // end if
		else{ // display results
	
			Object[] row = new Object[4];
			jLabel5.setText("Results Found:");
			jLabel5.setVisible(true);
       
	        /*
	         * get number of rows in result table
	         */
	             
	        for(i=1;!r.getResults(i).equals("end1");i++);
	        this.rows=(--i)/4;
	       
	        /*
	         * set results in table
	         */
	        
	        for(i=0;i<rows;i++){//for1     
	        	for(j=0;j<4;j++,index++){//for2
	        		row[j]=r.getResults(index);	        	      	
	        	}//end for2        	
	            model.addRow(row);            
	        }//end for1
	        scrollPane.setVisible(true);
	        scrollPane.setBounds(300, 270, 440,17*(rows+1));	// set table size
		}// end else
	 }// end if (!jText1...
		
	
	private void SearchReviewsOfBook()
	{
		obj logdt1 = new obj();
    	logdt1.setType("Search reviews isbn");
    	logdt1.setParam(0,b.getIsbn()); // set input from text field
    	
       	if(connected == 1)	
   	    	client.handleMessageFromClientUI(logdt1);
    	
	    try {
            Thread.sleep(1500); 
            } // end try
	    catch (InterruptedException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } // end catch
	}

	/**
	 * This method initializes jButton	
	 * 	
	 * @return javax.swing.JButton	
	 */

	
} // end Class
