package Gui;

import java.awt.Color;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Rectangle;

import javax.swing.BorderFactory;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Rectangle;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import Gui.*;
import logic.*;
import client.*;

import javax.swing.JPasswordField;
import javax.swing.border.Border;

import common.ChatIF;
import javax.swing.JRadioButton;



public class AccountStg extends JPanel {
	  private JLabel jLabel666 = null;
	private javax.swing.JLabel jLabel1;
	public javax.swing.JButton jButton1;
    public javax.swing.JButton jButton2;
    public javax.swing.JButton jButton3;
    public javax.swing.JButton jButton4;
    public javax.swing.JButton jButton5;
    public javax.swing.JButton jButton6;
    public javax.swing.JButton jButton7;
    public javax.swing.JButton jButton8;
    User u = new User();			/** user info**/
    String userPass;				/** user pass**/
    public static ChatClient client;  //  @jve:decl-index=0:
    public int connected;			/** if connected to server =1 else 0 **/
    private  ChatIF ChatIF = null;
    public static int Ures = 0;		/** store result for user name check **/
    public static int Ares = 0;		/** store result for user account check **/
	private JLabel Heading_jLabel = null;
	private JLabel user_jLabel = null;
	private JTextField user_jTextField = null;
	private JButton check_jButton = null;
	private JLabel Oldpass_jLabel = null;
	private JLabel newPass_jLabel = null;
	private JLabel RenewPass_jLabel = null;
	private JPasswordField newPass_jPasswordField = null;
	private JPasswordField OldPass_jPasswordField = null;
	private JPasswordField reNew_jPasswordField = null;
	private JLabel avlbl_jLabel = null;
	private JButton userUpdate_jButton = null;
	private JLabel NoMatch_jLabel = null;
	private JButton passUpdate_jButton = null;
	private JLabel WrongPass_jLabel = null;
	private JLabel headeracount_jLabel = null;
	private JLabel payment_jLabel = null;
	private JComboBox choosePaymentType_jComboBox = null;
	private JButton checkAcount_jButton = null;
	private JLabel acountStat_jLabel = null;
	private JTextField detail_jTextField = null;
	private JButton createAcount_jButton = null;
	/**
	 * This is the default constructor
	 */
	public AccountStg() {
		super();
		initialize();
	}

	/**
	 * This method initializes StudentForm
	 */
	private void initialize() {

        acountStat_jLabel = new JLabel();
        acountStat_jLabel.setBounds(new Rectangle(360, 314, 378, 21));
        acountStat_jLabel.setText("");
        acountStat_jLabel.setVisible(false);
        payment_jLabel = new JLabel();
        payment_jLabel.setBounds(new Rectangle(208, 362, 149, 18));
        payment_jLabel.setText("Choose Payment Method");
        headeracount_jLabel = new JLabel();
        headeracount_jLabel.setBounds(new Rectangle(225, 286, 121, 17));
        headeracount_jLabel.setText("Check Acount");
        WrongPass_jLabel = new JLabel();
        WrongPass_jLabel.setBounds(new Rectangle(465, 181, 120, 21));
        WrongPass_jLabel.setText("Wrong Password");
        WrongPass_jLabel.setVisible(false);
        NoMatch_jLabel = new JLabel();
        NoMatch_jLabel.setBounds(new Rectangle(462, 224, 122, 25));
        NoMatch_jLabel.setText("Not Match");
        NoMatch_jLabel.setVisible(false);
        avlbl_jLabel = new JLabel();
        avlbl_jLabel.setBounds(new Rectangle(570, 151, 76, 19));
        avlbl_jLabel.setText("");
        avlbl_jLabel.setVisible(false);
        RenewPass_jLabel = new JLabel();
        RenewPass_jLabel.setBounds(new Rectangle(225, 240, 116, 20));
        RenewPass_jLabel.setText("Re-Enter New Pass");
        newPass_jLabel = new JLabel();
        newPass_jLabel.setBounds(new Rectangle(225, 211, 105, 19));
        newPass_jLabel.setText("New Pass");
        Oldpass_jLabel = new JLabel();
        Oldpass_jLabel.setBounds(new Rectangle(225, 181, 104, 19));
        Oldpass_jLabel.setText("Old Password");
        user_jLabel = new JLabel();
        user_jLabel.setBounds(new Rectangle(225, 149, 104, 20));
        user_jLabel.setText("User Name");
        Heading_jLabel = new JLabel();
        Heading_jLabel.setBounds(new Rectangle(369, 50, 239, 37));
        Heading_jLabel.setBounds(new Rectangle(420, 51, 105, 37));
        Heading_jLabel.setText("Acount Setting");
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
       
        
        this.setSize(1000, 700);
        this.setLayout(null);
      
        
        jButton1.setText("Search Book");
        jButton1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton1,null);
        jButton1.setBounds(30, 20, 140, 40);
		
        jButton2.setText("Search Review");
        jButton2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton2,null);
        jButton2.setBounds(30, 60, 140, 40);

        jButton3.setText("Manage Books");
        jButton3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 0, 0), 3));
        this.add(jButton3,null);
        jButton3.setBounds(30, 100, 140, 40);

        jButton4.setText("Manage Catalog");
        jButton4.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton4,null);
        jButton4.setBounds(30, 140, 140, 40);

        jButton5.setText("Manage Reviews");
        jButton5.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton5,null);
        jButton5.setBounds(30, 180, 140, 40);

        jButton6.setText("Logout");
        jButton6.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton6,null);
        jButton6.setBounds(30, 220, 140, 40);

        jButton7.setText("jButton7");
        jButton7.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton7,null);
        jButton7.setBounds(30, 260, 140, 40);

        jButton8.setText("Logout");
        jButton8.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton8,null);
        jButton8.setBounds(30, 300, 140, 40);	
        
        
        
        this.add(Heading_jLabel, null);
        this.add(user_jLabel, null);
        this.add(getUser_jTextField(), null);
        this.add(getCheck_jButton(), null);
        this.add(Oldpass_jLabel, null);
        this.add(newPass_jLabel, null);
        this.add(RenewPass_jLabel, null);
        this.add(getNewPass_jPasswordField(), null);
        this.add(getOldPass_jPasswordField(), null);
        this.add(getReNew_jPasswordField(), null);
        this.add(avlbl_jLabel, null);
        this.add(getUserUpdate_jButton(), null);
        this.add(NoMatch_jLabel, null);
        this.add(getPassUpdate_jButton(), null);
        this.add(WrongPass_jLabel, null);
        this.add(headeracount_jLabel, null);
        this.add(payment_jLabel, null);
        this.add(getChoosePaymentType_jComboBox(), null);
        this.add(getCheckAcount_jButton(), null);
        this.add(acountStat_jLabel, null);
        this.add(getDetail_jTextField(), null);
        this.add(getCreateAcount_jButton(), null);
        jButton8.setBounds(30, 300, 140, 40);	
        connected = 1;
		try  // trying to connect to server
		{
        	client= new ChatClient(u.getHost(),5555,ChatIF);
		} catch(IOException exception)
		{
        	JOptionPane.showMessageDialog(null,"Error: Can't setup connection!","Error",JOptionPane.CLOSED_OPTION);
        	connected = 0; // failed to connect
		} // end catch
		privinit();

		/* Create Background */
		
        jLabel1 = new javax.swing.JLabel();
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Blue1.jpg"))); // NOI18N
        jLabel1.setText("jLabel1");
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
		/* end Background */
	} // end initialize
	/**
	 * initalize menue buttons 
	 */
	void privinit(){
	   	 /* Reader */
	   	if(u.getPriv() == 0){  
	   		jButton3.setText("Account Settings");
	   		jButton4.setText("Logout");
	   		jButton5.setVisible(false);
	   		jButton6.setVisible(false);
	   		jButton7.setVisible(false);
	   		jButton8.setVisible(false);
	   	}
	   	/* Librarian */
	   	else if(u.getPriv() == 1){
	   		jButton7.setVisible(false);
	   		jButton8.setVisible(false);
	   	
	   	
	   	}
	   	/* Library Manager */
	   	else if(u.getPriv() == 2){
	   		jButton5.setText("Manage Reports");
	   		jButton6.setText("Logout");
	   		jButton7.setVisible(false);
	   		jButton8.setVisible(false);
	   	
	   	}
	   	/* System Manager */
	   	else if(u.getPriv() == 3){
	      		jButton6.setText("Manage Reports");
	   		jButton7.setText("Account Management");
	   		jButton8.setText("Logout");
	   	}
	}// end privinit

	/**
	 * This method initializes user_jTextField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getUser_jTextField() {
		if (user_jTextField == null) {
			user_jTextField = new JTextField();
			user_jTextField.setBounds(new Rectangle(345, 150, 109, 20));
			user_jTextField.setText(u.getUser());
			userPass = u.getPassword();	//saving user password;
		}
		return user_jTextField;
	}

	/**
	 * This method initializes check_jButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getCheck_jButton() {
		if (check_jButton == null) {
			check_jButton = new JButton();
			check_jButton.setBounds(new Rectangle(466, 151, 92, 20));
			check_jButton.setText("Check");
			check_jButton.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) 
				{
					if (!user_jTextField.getText().equals(u.getUser()));
					{
						System.out.println("Check actionPerformed()"); // TODO Auto-generated Event stub actionPerformed()
						obj logdt = new obj();
				    	logdt.setType("checkAcc");
				    	logdt.setParam(0,user_jTextField.getText());//user
				    	if (connected == 1)
							client.handleMessageFromClientUI(logdt);
						try
						{
							Thread.sleep(100);
						} catch (InterruptedException ex)
						{
							Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);			
						}
				   	 	
				        if(Ures == 1)
				        {
				        	avlbl_jLabel.setVisible(true);
				        	avlbl_jLabel.setText("Available");
				        	userUpdate_jButton.setVisible(true);
				        //	jLabel15.setVisible(true);
				        }
				    		
				        if(Ures == 0)
				        {
				        	avlbl_jLabel.setVisible(true);
				        	avlbl_jLabel.setText("Not Available");
				        	userUpdate_jButton.setVisible(false);
				        	//jLabel14.setVisible(true);
				        }
					}
					
				}
			});
		}
		return check_jButton;
	}

	/**
	 * This method initializes newPass_jPasswordField	
	 * 	
	 * @return javax.swing.JPasswordField	
	 */
	private JPasswordField getNewPass_jPasswordField() {
		if (newPass_jPasswordField == null) {
			newPass_jPasswordField = new JPasswordField();
			newPass_jPasswordField.setBounds(new Rectangle(345, 212, 109, 21));
			newPass_jPasswordField.addFocusListener(new java.awt.event.FocusAdapter() {
				public void focusLost(java.awt.event.FocusEvent e) 
				{
					validatePass();
				}
			});
		}
		return newPass_jPasswordField;
	}

	/**
	 * This method initializes OldPass_jPasswordField	
	 * 	
	 * @return javax.swing.JPasswordField	
	 */
	private JPasswordField getOldPass_jPasswordField() {
		if (OldPass_jPasswordField == null) {
			OldPass_jPasswordField = new JPasswordField();
			OldPass_jPasswordField.setBounds(new Rectangle(344, 181, 109, 20));
			OldPass_jPasswordField.addFocusListener(new java.awt.event.FocusAdapter() {
				public void focusLost(java.awt.event.FocusEvent e) 
				{
					validatePass();
				}
			});
		}
		return OldPass_jPasswordField;
	}

	/**
	 * This method initializes reNew_jPasswordField	
	 * 	
	 * @return javax.swing.JPasswordField	
	 */
	private JPasswordField getReNew_jPasswordField() {
		if (reNew_jPasswordField == null) {
			reNew_jPasswordField = new JPasswordField();
			reNew_jPasswordField.setBounds(new Rectangle(344, 239, 108, 23));
			reNew_jPasswordField.addFocusListener(new java.awt.event.FocusAdapter() {
				public void focusLost(java.awt.event.FocusEvent e) 
				{
					validatePass();
				}
			});
		}
		return reNew_jPasswordField;
	}
	/**
     * sets if user acount active or not
     */
    public void validateAcount(boolean result)
    {
    	if(result == true)
    	{
    		Ares=1;
    	}
    	else 
    	{
    		Ares=0;
    	}	
    }
	/** sets if new user name is available or not
	 * 
	 * @param result - result of query
	 * true - 
	 * false - 
	 */
    public void Available(boolean result)
    {
    	if(result == true)
    	{
    		Ures=1;
    	}
    	else 
    	{
    		Ures=0;
    	}	
    }
    /**
     * checks paasword fields
     * if fields correct then update password button is visible
     * else it won't
     */
    public void validatePass()
    {
    	if (userPass.equals(OldPass_jPasswordField.getText()))
    	{
    		WrongPass_jLabel.setVisible(false);
    		if (!newPass_jPasswordField.getText().equals(reNew_jPasswordField.getText()))
    		{
    			NoMatch_jLabel.setVisible(true);
    			passUpdate_jButton.setVisible(false);
    			
    		}
    		else
    		{
    			NoMatch_jLabel.setVisible(false);
    			passUpdate_jButton.setVisible(true);
    		}
    	}
    	else
    	{
    		WrongPass_jLabel.setVisible(true);
    		passUpdate_jButton.setVisible(false);
    	}
    }
    
	/**
	 * This method initializes userUpdate_jButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getUserUpdate_jButton() {
		if (userUpdate_jButton == null) {
			userUpdate_jButton = new JButton();
			userUpdate_jButton.setVisible(false);
			userUpdate_jButton.setBounds(new Rectangle(660, 150, 77, 19));
			userUpdate_jButton.setText("Update");
			userUpdate_jButton.addActionListener(new java.awt.event.ActionListener() 
			{
				/**
				 * updating user name
				 * sending to server request to update
				 * new user name and old user name
				 */
				public void actionPerformed(java.awt.event.ActionEvent e) 
				{
					System.out.println("Update user name actionPerformed()"); 
					int i;
					obj bookdt = new obj();
					
					bookdt.setType("***update user name***");
					bookdt.setParam(0, u.getUser());
					bookdt.setParam(1, user_jTextField.getText());
					
					if (connected == 1)
						client.handleMessageFromClientUI(bookdt);
					try
					{
						Thread.sleep(100);
					} catch (InterruptedException ex)
					{
						Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);			
					}
					
					Border green_line;
					green_line = BorderFactory.createLineBorder(Color.green);
					userUpdate_jButton.setBorder(green_line);
					u.setUser(user_jTextField.getText());
				}
			});
		}
		return userUpdate_jButton;
	}

	/**
	 * This method initializes passUpdate_jButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getPassUpdate_jButton() {
		if (passUpdate_jButton == null) {
			passUpdate_jButton = new JButton();
			passUpdate_jButton.setBounds(new Rectangle(647, 227, 97, 21));
			passUpdate_jButton.setText("Save Pass");
			passUpdate_jButton.setVisible(false);
			passUpdate_jButton.addActionListener(new java.awt.event.ActionListener() 
			{
				/**
				 * updating user name
				 * sending to server request to update
				 * new user name and old user name
				 */
				public void actionPerformed(java.awt.event.ActionEvent e) 
				{
					System.out.println("Update user Password actionPerformed()"); 
					int i;
					obj bookdt = new obj();
					
					bookdt.setType("***update user pass***");
					bookdt.setParam(0, u.getUser());
					bookdt.setParam(1, newPass_jPasswordField.getText());
					
					if (connected == 1)
						client.handleMessageFromClientUI(bookdt);
					try
					{
						Thread.sleep(100);
					} catch (InterruptedException ex)
					{
						Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);			
					}
					
					Border green_line;
					green_line = BorderFactory.createLineBorder(Color.green);
					passUpdate_jButton.setBorder(green_line);
					u.setPass(newPass_jPasswordField.getText());
					userPass = newPass_jPasswordField.getText();
				}
			});
		}
		return passUpdate_jButton;
	}

	/**
	 * This method initializes choosePaymentType_jComboBox	
	 * 	
	 * @return javax.swing.JComboBox	
	 */
	private JComboBox getChoosePaymentType_jComboBox() {
		if (choosePaymentType_jComboBox == null) {
			choosePaymentType_jComboBox = new JComboBox();
			choosePaymentType_jComboBox.addItem("Credit Card");
			choosePaymentType_jComboBox.addItem("PayPal");
			choosePaymentType_jComboBox.setBounds(new Rectangle(209, 388, 149, 23));
			choosePaymentType_jComboBox.addItemListener(new java.awt.event.ItemListener() {
				public void itemStateChanged(java.awt.event.ItemEvent e) 
				{
					if (choosePaymentType_jComboBox.getSelectedItem().equals("Credit Card"))
					{
						detail_jTextField.setVisible(true);
						System.out.println("itemStateChanged()"); // TODO Auto-generated Event stub itemStateChanged()
					}
				}
			});
			
		}
		return choosePaymentType_jComboBox;
	}

	/**
	 * This method initializes checkAcount_jButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getCheckAcount_jButton() {
		if (checkAcount_jButton == null) {
			checkAcount_jButton = new JButton();
			checkAcount_jButton.setBounds(new Rectangle(217, 314, 121, 20));
			checkAcount_jButton.setText("Check Acount");
			checkAcount_jButton.addActionListener(new java.awt.event.ActionListener() {
				/**
				 * checking the user acount status
				 * sending request to server to retieve info
				 * if exists we inform the user
				 * if not exist we offer to open an acount
				 */
				public void actionPerformed(java.awt.event.ActionEvent e) 
				{
					checkAcount();
				}
			});
		}
		return checkAcount_jButton;
	}
	/**
	 * send te server request to check user account
	 * called by getCheckAccount Button
	 */
	public void checkAcount()
	{
		if (u.getUser() != null );
		{
			System.out.println("Check Acount actionPerformed()"); // TODO Auto-generated Event stub actionPerformed()
			obj logdt = new obj();
	    	logdt.setType("*** check Account State ***");
	    	logdt.setParam(0,u.getUser());//user
	    	if (connected == 1)
				client.handleMessageFromClientUI(logdt);
			try
			{
				Thread.sleep(100);
			} catch (InterruptedException ex)
			{
				Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);			
			}
	        if(Ares == 1)
	        {
	        	acountStat_jLabel.setVisible(true);
	        	acountStat_jLabel.setText("You Have No Acount, Choose Payment Method And Create Acount.");
	        	payment_jLabel.setVisible(true);
	        	payment_jLabel.setText("Choose Payment Method");
	        	choosePaymentType_jComboBox.setVisible(true);
	        }
	    		
	        if(Ares == 0)
	        {
	        	acountStat_jLabel.setText("You Have An Acount.");
	        	acountStat_jLabel.setVisible(true);
	        	payment_jLabel.setVisible(false);
	        	choosePaymentType_jComboBox.setVisible(false);
	        	detail_jTextField.setVisible(false);
	        	createAcount_jButton.setVisible(false);
	        	//jLabel14.setVisible(true);
	        }
		}
	}
	/**
	 * This method initializes detail_jTextField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getDetail_jTextField() {
		if (detail_jTextField == null) {
			detail_jTextField = new JTextField();
			detail_jTextField.setVisible(false);
			detail_jTextField.setBounds(new Rectangle(405, 389, 213, 25));
			detail_jTextField.addKeyListener(new java.awt.event.KeyAdapter() {
				public void keyTyped(java.awt.event.KeyEvent e) 
				{
					createAcount_jButton.setVisible(true);
					System.out.println("keyTyped()"); // TODO Auto-generated Event stub keyTyped()
				}
			});
		}
		return detail_jTextField;
	}

	/**
	 * This method initializes createAcount_jButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getCreateAcount_jButton() {
		if (createAcount_jButton == null) {
			createAcount_jButton = new JButton();
			createAcount_jButton.setBounds(new Rectangle(632, 389, 135, 28));
			createAcount_jButton.setText("Create Acount");
			createAcount_jButton.setVisible(false);
			createAcount_jButton.addActionListener(new java.awt.event.ActionListener() {
				/**
				 * creating acount for user
				 * sending to server
				 * user name
				 * payment type
				 */
				public void actionPerformed(java.awt.event.ActionEvent e) 
				{
					int i;
					obj bookdt = new obj();
					
					bookdt.setType("***create account***");
					bookdt.setParam(0, u.getUser());
					bookdt.setParam(1, detail_jTextField.getText());
					
					if (connected == 1)
						client.handleMessageFromClientUI(bookdt);
					try
					{
						Thread.sleep(100);
					} catch (InterruptedException ex)
					{
						Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);			
					}
					
					Border green_line;
					green_line = BorderFactory.createLineBorder(Color.green);
					createAcount_jButton.setBorder(green_line);
					checkAcount();
				}
			});
		}
		return createAcount_jButton;
	}
	
} // end Class
