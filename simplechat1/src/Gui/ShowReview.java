package Gui;

import java.awt.Dimension;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Rectangle;

import javax.swing.event.ListSelectionEvent;
import javax.swing.table.TableModel;


import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Rectangle;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.EventListener;



import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;

import common.ChatIF;

import Gui.*;
import logic.*;
import client.*;



public class ShowReview extends JPanel {
	  private JLabel jLabel666 = null;
	
	private javax.swing.JLabel jLabel1;
	public javax.swing.JButton jButton1;
    public javax.swing.JButton jButton2;
    public javax.swing.JButton jButton3;
    public javax.swing.JButton jButton4;
    public javax.swing.JButton jButton5;
    public javax.swing.JButton jButton6;
    public javax.swing.JButton jButton7;
    public javax.swing.JButton jButton8;
    User u = new User();
    ShowRev show = new ShowRev();
    
    public javax.swing.JLabel jLabel3;
    public javax.swing.JLabel jLabel51;
    public javax.swing.JTextField jText1;
    public javax.swing.JLabel jLabel11;

    public javax.swing.JButton jButton10;

    private javax.swing.JLabel jLabel5;
    private javax.swing.JTextArea Text=null; // review updated


    
    	/*  show review buttons  */
    public javax.swing.JLabel jLabel10; // Review
    public javax.swing.JLabel jLabel12=null; // from:
    public javax.swing.JLabel jLabel13=null; // date
    public javax.swing.JLabel jLabel14; //book details
    public javax.swing.JLabel jLabel15=null; //title
    public javax.swing.JLabel jLabel16=null; //isbn
    public javax.swing.JLabel jLabel17=null; // rating
    public javax.swing.JLabel jLabel18=null; // review old
    public javax.swing.JButton jButton11; // back
    
    
    public static ChatClient client;
    private  ChatIF ChatIF = null;
    public int connected=1;
  
    public String isbn;
    public String username;

   public Review r;
    
    
	/**
	 * This is the default constructor
	 */

public ShowReview() {
		
		super();
		initialize();
		
		
	}
	
	
	
	/**
	 * This method initializes StudentForm
	 */
	private void initialize() {
		
	        
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();

        jLabel51 = new javax.swing.JLabel();
        jText1 = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();

        jButton10 = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel(); // Review

        jLabel14 = new javax.swing.JLabel(); //book details
        jButton11 = new javax.swing.JButton(); // back
 
        jLabel5 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        
        



        
        this.setSize(1000, 700);
        this.setLayout(null);
        jButton1.setText("Search Book");
        jButton1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton1,null);
        jButton1.setBounds(30, 20, 140, 40);
		
        jButton2.setText("Search Review");
        jButton2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 0, 0), 3));
        this.add(jButton2,null);
        jButton2.setBounds(30, 60, 140, 40);

        jButton3.setText("Manage Books");
        jButton3.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton3,null);
        jButton3.setBounds(30, 100, 140, 40);

        jButton4.setText("Manage Catalog");
        jButton4.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton4,null);
        jButton4.setBounds(30, 140, 140, 40);

        jButton5.setText("Manage Reviews");
        jButton5.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton5,null);
        jButton5.setBounds(30, 180, 140, 40);

        jButton6.setText("Logout");
        jButton6.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton6,null);
        jButton6.setBounds(30, 220, 140, 40);

        jButton7.setText("jButton7");
        jButton7.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton7,null);
        jButton7.setBounds(30, 260, 140, 40);

        jButton8.setText("Logout");
        jButton8.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton8,null);
        jButton8.setBounds(30, 300, 140, 40);	
        
        
        this.add(getjLabel12(), null);
        this.add(getjLabel13(), null);
        this.add(getjLabel15(), null);
        this.add(getjLabel16(), null);
        this.add(getjLabel17(), null);
        this.add(getjtext(), null);
        
        
        privinit();
		

	        /*
	         *		Show Review Buttons  
	         */
	        
	        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 18));
	        jLabel10.setText("Review");
	        this.add(jLabel10);
	        jLabel10.setBounds(460, 50, 250, 22); 

	  

	
	        jLabel14.setText("Book Details:");
	        this.add(jLabel14);
	        jLabel14.setBounds(400, 160, 240, 14);
	   
	        

	        
	        jButton11.setText("Back");
	        this.add(jButton11);
	        jButton11.setBounds(460, 460, 80, 23);
	        
	        
	   

	        
	        
	        
	        
	   	    
		/* Create Background */
		
        jLabel1 = new javax.swing.JLabel();
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Blue1.jpg"))); // NOI18N
        jLabel1.setText("jLabel1");
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        
        jLabel1.addMouseListener(new MouseAdapter(){
              public void mouseEntered(MouseEvent arg0) {
        	      System.out.println("Entered!!!");
        	     }

	     });
		
		/* end Background */
		
   
	} // end initialize

	void privinit(){
	   	 /* Reader */
	   	if(u.getPriv() == 0){  
	   		jButton3.setText("Account Settings");
	   		jButton4.setText("Logout");
	   		jButton5.setVisible(false);
	   		jButton6.setVisible(false);
	   		jButton7.setVisible(false);
	   		jButton8.setVisible(false);
	   	}
	   	/* Librarian */
	   	else if(u.getPriv() == 1){
	   		jButton7.setVisible(false);
	   		jButton8.setVisible(false);
	   	
	   	
	   	}
	   	/* Library Manager */
	   	else if(u.getPriv() == 2){
	   		jButton6.setText("Manage Reports");
	   		jButton7.setText("Logout");
	   		jButton8.setVisible(false);
	   	
	   	}
	   	/* System Manager */
	   	else if(u.getPriv() == 3){
	      		jButton6.setText("Manage Reports");
	   		jButton7.setText("Account Management");
	   		jButton8.setText("Logout");
	   	}
	}// end privinit
	
	
	public void loadReview(Review rev){
		this.r=rev;
	    this.jLabel12.setText("From: "+r.getUsername());
	    this.jLabel13.setText("Date: "+r.getDate());
	    this.jLabel15.setText("Title: "+r.getTitle());     
	    this.jLabel16.setText("ISBN: "+r.getIsbn());     
	    this.jLabel17.setText("Rating: "+r.getRating());     
	    this.Text.setText(r.getReview());  //review

   
	}// end ShowReview
	
	
	
	/**
	 * This method initializes jLabel12	
	 */
	private JLabel getjLabel12() {
		if (jLabel12 == null) {
			 jLabel12 = new JLabel(); // from
			jLabel12.setText("From: ");
	      //  this.add(jLabel12);
	        jLabel12.setBounds(400, 130, 130, 14);	
		}
		jLabel12.addFocusListener(new java.awt.event.FocusAdapter() {
			public void focusLost(java.awt.event.FocusEvent e) {
				System.out.println("focusLost()"); 
				String pName=jLabel12.getText();
				if(!pName.equals(r.getUsername())) r.setUsername(pName);
			}
		});
		return jLabel12;
	}


	
	/**
	 * This method initializes jLabel13	
	 */
	private JLabel getjLabel13() {
		if (jLabel13 == null) {
			 jLabel13 = new JLabel(); // date
			 jLabel13.setText("Date: ");		        
		     jLabel13.setBounds(570, 130, 150, 14);
		}
		jLabel13.addFocusListener(new java.awt.event.FocusAdapter() {
			public void focusLost(java.awt.event.FocusEvent e) {
				System.out.println("focusLost()"); 
				String pName=jLabel13.getText();
				if(!pName.equals(r.getDate())) r.setDate(pName);
			}
		});
		return jLabel13;
	}

	/**
	 * This method initializes jLabel15	
	 */
	private JLabel getjLabel15() {
		if (jLabel15 == null) {
			 jLabel15 = new JLabel(); // title:
		        jLabel15.setText("Title: ");
		        jLabel15.setBounds(400, 190, 500, 14);     
		}
		jLabel15.addFocusListener(new java.awt.event.FocusAdapter() {
			public void focusLost(java.awt.event.FocusEvent e) {
				System.out.println("focusLost()"); 
				String pName=jLabel15.getText();
				if(!pName.equals(r.getTitle())) r.setTitle(pName);
			}
		});
		return jLabel15;
	}

	
	/**
	 * This method initializes jLabel16	
	 */
	private JLabel getjLabel16() {
		if (jLabel16 == null) {
			 jLabel16 = new JLabel(); // ISBN:	
		        jLabel16.setText("ISBN: ");
		        jLabel16.setBounds(400, 220, 250, 14);
		}
		jLabel16.addFocusListener(new java.awt.event.FocusAdapter() {
			public void focusLost(java.awt.event.FocusEvent e) {
				System.out.println("focusLost()"); 
				String pName=jLabel16.getText();
				if(!pName.equals(r.getIsbn())) r.setIsbn(pName);
			}
		});
		return jLabel16;
	}

	
	/**
	 * This method initializes jLabel17	
	 */
	private JLabel getjLabel17() {
		if (jLabel17 == null) {
			 jLabel17 = new JLabel(); // rating
		        jLabel17.setText("Rating: ");
		        jLabel17.setBounds(570,220,200,14);


		}
		jLabel17.addFocusListener(new java.awt.event.FocusAdapter() {
			public void focusLost(java.awt.event.FocusEvent e) {
				System.out.println("focusLost()"); 
				String pName=jLabel17.getText();
				if(!pName.equals(r.getRating())) r.setRating(pName);
			}
		});
		return jLabel17;
	}

	
	/**
	 * This method initializes getjtext	
	 */
	private JTextArea getjtext() {
		if (Text == null) {
			Text = new JTextArea(); //review
	        Text.setBounds(400, 275, 270, 180);
	        Text.setEditable(false);
	        Text.setFont(new java.awt.Font("Tahoma", 1, 14));
	        Text.setOpaque(false);
 
		}
		jLabel12.addFocusListener(new java.awt.event.FocusAdapter() {
			public void focusLost(java.awt.event.FocusEvent e) {
				System.out.println("focusLost()"); 
				String pName=Text.getText();
				if(!pName.equals(r.getReview())) r.setReview(pName);
			}
		});
		return Text;
	}

	
	
} // end Class
