package Gui;

import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagLayout;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Rectangle;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Rectangle;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.text.SimpleDateFormat;
import javax.swing.border.Border;
import java.awt.Color;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.BorderFactory; 
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.border.EtchedBorder;

import common.ChatIF;

import Gui.*;
import logic.*;
import client.*;



public class Overview extends JPanel {
	  private JLabel jLabel666 = null;
	private javax.swing.JLabel jLabel1;
	public javax.swing.JButton jButton1;
    public javax.swing.JButton jButton2;
    public javax.swing.JButton jButton3;
    public javax.swing.JButton jButton4;
    public javax.swing.JButton jButton5;
    public javax.swing.JButton jButton6;
    public javax.swing.JButton jButton7;
    public javax.swing.JButton jButton8;
    User u = new User();
    Book b = new Book();
    Review r = new Review();
    Orders o = new Orders();
    public String[] List= new String[200];
    public String[] List2= new String[200];
    
    public javax.swing.JButton Button1; //Overview
    public javax.swing.JButton Button2; //Reviews
    public javax.swing.JButton Button3;	//Write Reveiw
    public javax.swing.JButton Button4; //Buy

    
    	/* Labels, Text fields, panel, buttons */
    
    private javax.swing.JLabel Label01=null; // title
    public javax.swing.JTextField jText1=null; //title
    private javax.swing.JLabel Label02=null; // ganre
    private javax.swing.JComboBox Combo02=null; // ganre
    private javax.swing.JLabel Label03=null; // subject
    private javax.swing.JComboBox Combo03=null; // subject
    private javax.swing.JLabel Label04=null; // isbn
    public javax.swing.JTextField jText4=null;// isbn
    private javax.swing.JLabel Label05=null; // language
    private javax.swing.JTextField jText2=null;// language
    //    private javax.swing.JComboBox Combo04=null;// language
    private javax.swing.JLabel Label06=null; // rating
    private javax.swing.JLabel Label07=null; // price
    public javax.swing.JTextField jText7=null;// price
    private javax.swing.JLabel Label08=null; // authors
    private javax.swing.JLabel Label09=null; // book picture
    private javax.swing.JLabel Label10=null; // rating 
    private javax.swing.JComboBox Combo01=null; // authors
    public javax.swing.JButton Button5=null; //Back 
    private javax.swing.JButton Button6=null; //summary
    private javax.swing.JButton Button7=null; //contents
    private javax.swing.JTextArea TextArea01=null; // summary or contents


    
    
    
		/*  show review buttons  */
    
    public javax.swing.JLabel jLabel10; // Review
	public javax.swing.JLabel jLabel12=null; // from:
	public javax.swing.JLabel jLabel13=null; // date
	public javax.swing.JLabel jLabel14; //book details
	public javax.swing.JLabel jLabel15=null; //title
	public javax.swing.JLabel jLabel16=null; //isbn
	public javax.swing.JLabel jLabel17=null; // rating
	private javax.swing.JTextArea Text=null; // review updated
    
	/**
	 *  Instance of int instance, 
	 *  to save the connection to the server status 
	 *  1 = connected , else not connected
	 */    
    public int connected;
    /**
	 * Instance of ChatClient 
	 * To run the Chat Client functions
	 */   
    public static ChatClient client;  //  @jve:decl-index=0:
    /**
	 * Instance of ChatIF 
	 * To check the connection
	 */   
	private  ChatIF ChatIF = null;
	
//	public static final String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";


    
	/**
	 * This is the default constructor
	 */
	public Overview() {
		super();
		initialize();
	}

	/**
	 * This method initializes StudentForm
	 */
	private void initialize() {
		
		
	        
		/* Setup Connection to server */
        
	       connected = 1;
	   	    try  // trying to connect to server 
	   	    {
	   	    	 client= new ChatClient(u.getHost(),5555,ChatIF);
	   	    } // end try
	   	    catch(IOException exception)
	   	    {
	   	    	JOptionPane.showMessageDialog(null,"Error: Can't setup connection!","Error",JOptionPane.CLOSED_OPTION);
	   	    	connected = 0; // failed to connect
	   	    } // end catch
	        
	   	    /* end setup connection */

		
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
       
        Button1 = new javax.swing.JButton();
        Button2 = new javax.swing.JButton();
        Button3 = new javax.swing.JButton();
        Button4 = new javax.swing.JButton();
        Button5 = new javax.swing.JButton();
        Button6 = new javax.swing.JButton();
        Button7 = new javax.swing.JButton();
        
        
        this.setSize(1000, 700);
        this.setLayout(null);
    
        
    
        /*
         *    Write Review Buttons
         */
        
        Button1.setText("Overview");
        Button1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 0, 0), 3));
        this.add(Button1);
        Button1.setBounds(330,50, 110, 29);
        
        Button2.setText("Reviews");
        Button2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
//        this.add(Button2);
        Button2.setBounds(380,50, 110, 29);
        
        Button3.setText("Write Review");
        Button3.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(Button3);
        Button3.setBounds(440,50, 110, 29);
        
        Button4.setText("Buy");
        Button4.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(Button4);
        Button4.setBounds(550,50, 110, 29);
        
        
        Button5.setText("Back");
        Button5.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(Button5);
        Button5.setBounds(455, 600, 90, 23);
        
        
        Button6.setText("Summary");
        Button6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 0, 0), 3));
        this.add(Button6);
        Button6.setBounds(240, 350, 90, 23);

        Button6.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				Button6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 0, 0), 3));
				Button7.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
		    	TextArea01.setText(List[3]); // summary
			
			}});
        
        
        Button7.setText("Contents");
        Button7.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(Button7);
        Button7.setBounds(330, 350, 90, 23);
        
        Button7.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				Button7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 0, 0), 3));
				Button6.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
		    	TextArea01.setText(List[4]); // contents
				
			}});
        
        
        
        
		this.add(getjLabel01(),null); //title
        this.add(getjText1(),null);
        this.add(getjLabel02(),null); // Genre
        this.add(getCombo02(),null);
        this.add(getjLabel03(),null); // subject
        this.add(getCombo03(),null);
        this.add(getjLabel04(),null); // ISBN
        this.add(getjText4(),null);
        this.add(getjLabel05(),null); // language
        this.add(getjText2(),null);   
        this.add(getjLabel06(),null); // Authors
        this.add(getCombo01(),null); 
        this.add(getjLabel07(),null); // Price
        this.add(getjText7(),null);
        this.add(getjLabel06(),null); // Rating
        this.add(getjLabel08(),null);
        this.add(getjLabel09(),null);
        this.add(getjLabel10(),null);
        this.add(TextArea01(),null);

        
        
        
//		privinit();
	


		/* Create Background */
		
        jLabel1 = new javax.swing.JLabel();
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Blue1.jpg"))); // NOI18N
        jLabel1.setText("jLabel1");
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
		/* end Background */
	} // end initialize

	void privinit(){
	   	 /* Reader */
	   	if(u.getPriv() == 0){  
	   		jButton3.setText("Account Settings");
	   		jButton4.setText("Logout");
	   		jButton5.setVisible(false);
	   		jButton6.setVisible(false);
	   		jButton7.setVisible(false);
	   		jButton8.setVisible(false);
	   	}
	   	/* Librarian */
	   	else if(u.getPriv() == 1){
	   		jButton7.setVisible(false);
	   		jButton8.setVisible(false);
	   	
	   	
	   	}
	   	/* Library Manager */
	   	else if(u.getPriv() == 2){
	   		jButton5.setText("Manage Reports");
	   		jButton6.setText("Logout");
	   		jButton7.setVisible(false);
	   		jButton8.setVisible(false);
	   	
	   	}
	   	/* System Manager */
	   	else if(u.getPriv() == 3){
	      		jButton6.setText("Manage Reports");
	   		jButton7.setText("Account Management");
	   		jButton8.setText("Logout");
	   	}
	}// end privinit
	
	
	/**
	 * This method initializes Label01 : Title
	 */
	private JLabel getjLabel01() {
		if (Label01 == null) {
			Label01 = new JLabel();
			Label01.setText("Title");
			Label01.setBounds(400, 110, 130, 14);	
		}
		return Label01;
	}
	/**
	 * This method initializes jText1 : Title
	 */
	private JTextField getjText1() {
		if (jText1 == null) {
			jText1 = new JTextField();
//			jText1.setText("Title");
			jText1.setEditable(false);
			jText1.setBounds(400, 130, 330, 20);	
		}
		return jText1;
	}
	
	/**
	 * This method initializes Label02 : Genre
	 */
	private JLabel getjLabel02() {
		if (Label02 == null) {
			Label02 = new JLabel();
			Label02.setText("Genre");
			Label02.setBounds(400, 160, 130, 14);	
		}
		return Label02;
	}
	
	/**
	 * This method initializes Combo02 : Genre
	 */
	private JComboBox getCombo02() {
		if (Combo02 == null) {
			Combo02 = new JComboBox();
			Combo02.setBounds(new Rectangle(400, 180, 160, 20));
//			Combo02.addItem("Genre");
//			for (int i = 0; i < List.length-1; i++) {
//				Combo01.addItem((Object)List[i]);

		}
		return Combo02;
	}
	
	
	
	/**
	 * This method initializes Label03 : Subject
	 */
	private JLabel getjLabel03() {
		if (Label03 == null) {
			Label03 = new JLabel();
			Label03.setText("Subject");
			Label03.setBounds(570, 160, 130, 14);	
		}
		return Label03;
	}
	
	
	/**
	 * This method initializes Combo03 : Subject
	 */
	private JComboBox getCombo03() {
		if (Combo03 == null) {
			Combo03 = new JComboBox();
			Combo03.setBounds(new Rectangle(570, 180, 160, 20));
//			Combo03.addItem("Subject");
		}
		return Combo03;
	}
	
	
	
	/**
	 * This method initializes Label04 : ISBN
	 */
	private JLabel getjLabel04() {
		if (Label04 == null) {
			Label04 = new JLabel();
			Label04.setText("ISBN");
			Label04.setBounds(400, 210, 130, 14);	
		}
		return Label04;
	}
	
	/**
	 * This method initializes jText4 : ISBN
	 */
	private JTextField getjText4() {
		if (jText4 == null) {
			jText4 = new JTextField();
//			jText4.setText("ISBN");
			jText4.setEditable(false);
			jText4.setBounds(400, 230, 160, 20);
		}
		return jText4;
	}
	
	
	
	
	/**
	 * This method initializes Label05 : Language
	 */
	private JLabel getjLabel05() {
		if (Label05 == null) {
			Label05 = new JLabel();
			Label05.setText("Language");
			Label05.setBounds(570, 210, 130, 14);	
		}
		return Label05;
	}
	
	
	/**
	 * This method initializes jText2 : Language
	 */
	private JTextField getjText2() {
		if (jText2 == null) {
			jText2 = new JTextField();
//			jText7.setText("Price");
			jText2.setEditable(false);
			jText2.setBounds(570, 230, 160, 20);
		}
		return jText2;
	}
	

	


	
	/**
	 * This method initializes Label08 : Authors
	 */
	private JLabel getjLabel08() {
		if (Label08 == null) {
			Label08 = new JLabel();
			Label08.setText("Author");
			Label08.setBounds(400, 260, 130, 14);	
		}
		return Label08;
	}
	
	
	/**
	 * This method initializes Combo01 : Authors
	 */
	private JComboBox getCombo01() {
		
		if (Combo01 == null) {
			Combo01 = new JComboBox();
			Combo01.setBounds(new Rectangle(400, 280, 160, 20));
//			Combo01.addItem("Authors");


		}
		return Combo01;
	}

	
	
	/**
	 * This method initializes Label07 : Price
	 */
	private JLabel getjLabel07() {
		if (Label07 == null) {
			Label07 = new JLabel();
			Label07.setText("Price");
			Label07.setBounds(570, 260, 130, 14);	
		}
		return Label07;
	}
	
	/**
	 * This method initializes jText7 : Price
	 */
	private JTextField getjText7() {
		if (jText7 == null) {
			jText7 = new JTextField();
//			jText7.setText("Price");
			jText7.setEditable(false);
			jText7.setBounds(570, 280, 160, 20);	
		}
		return jText7;
	}
	
	/**
	 * This method initializes Label09 : Book Picture
	 */
	private JLabel getjLabel09() {
		if (Label09 == null) {
			Label09 = new JLabel();
//			Label09.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/"+List[9]+".jpg"))); // NOI18N
			Label09.setVerticalAlignment(JLabel.CENTER);
			Label09.setBounds(240, 110, 160, 160);	
		}
		return Label09;
	}
	
	
	/**
	 * This method initializes Label06 : Rating
	 */
	private JLabel getjLabel06() {
		if (Label06 == null) {
			Label06 = new JLabel();
			Label06.setText("Rating");
			Label06.setBounds(240, 280, 130, 14);	
		}
		return Label06;
	}
	
	
	/**
	 * This method initializes Label10 : Rating Picture
	 */
	private JLabel getjLabel10() {
		if (Label10 == null) {
			Label10 = new JLabel();
//			Label10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/sgreen.jpg"))); // NOI18N
			Label10.setBounds(240, 300, 130, 20);		
		}
		return Label10;
	}
	

	
	

	
	/**
	 * This method initializes TextArea01
	 */
	private JTextArea TextArea01() {
		if (TextArea01 == null) {
			TextArea01 = new JTextArea();
//			TextArea01.setText("TextArea01");
			TextArea01.setEditable(false);
			TextArea01.setFont(new java.awt.Font("Tahoma", 1, 13));
			TextArea01.setBounds(240, 373, 500, 210);	
		}
		return TextArea01;
	}
	
	/*
	 * 		loads all books
	 */
	void loadBook(Orders Or) {
		String pic = new String();
		String[] tmpList = new String[200];
		int rate;
		this.o=Or;
		tmpList = (String[]) o.getBooks();
//		this.List = (String[]) o.getBooks();
		
//		System.out.println("ov:loadBook:o.getbook"+List[1]);
		

	
        int rows=tmpList.length/10;  // get number of rows
//        System.out.println("ov:loadBook:o.getbook"+rows);
    	jText1.setText(tmpList[1]); //title
    	
    	jText2.setText(tmpList[2]); //language
    	jText4.setText(tmpList[0]); //isbn
    	jText7.setText(tmpList[9]); //price
    	TextArea01.setText(tmpList[3]); // summary
    	
    	
    	try{

	    	getPicFromServer(tmpList[0]);
	    	pic = "c:"+File.separator+"Pic"+File.separator+tmpList[0]+".jpg";
	    	
			Label09.setIcon(new javax.swing.ImageIcon(pic)); // NOI18N
			}
		catch (NullPointerException ex){
			System.out.println("Null Exception: " + ex.getMessage());
			}
		
		
		if(tmpList[5].equals("0"))
			rate=1;
		else	
			rate=Integer.parseInt(tmpList[5]);
	
		pic = "/Pictures/star"+rate+".jpg";
		Label10.setIcon(new javax.swing.ImageIcon(getClass().getResource(pic))); // NOI18N
    	
		Combo01.removeAllItems();
		Combo02.removeAllItems();
		Combo03.removeAllItems();

		for (int i = 0; i < rows; i=i+9) {
			Combo01.addItem((Object)tmpList[i+8]); // authors
			Combo02.addItem((Object)tmpList[i+6]); // ganre
			Combo03.addItem((Object)tmpList[i+7]); // subject

			
		}// end for
		this.List=(String[])tmpList;
		
		Button6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 0, 0), 3));
		Button7.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
    	TextArea01.setText(List[3]); // summary
//    	List=null; // reset
	} // end loadUsers()
	private void getPicFromServer(String isbn)
	{
		obj logdt1 = new obj();
		logdt1.setType("Download FILE");
		
		logdt1.setParam(0,isbn ); // set filter
		logdt1.setParam(1,"pic" ); // set filter
		
		if(connected == 1)	
		    	client.handleMessageFromClientUI(logdt1);
		
	    try {
	        Thread.sleep(1500); 
	        } // end try
	    catch (InterruptedException ex) {
	        Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
	    } // end catch

		/* end sql  */
	}
} // end Class

