package Gui;

import java.awt.Choice;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Rectangle;

import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import com.sun.xml.internal.messaging.saaj.util.LogDomainConstants;

import common.ChatIF;

import Gui.*;
import logic.*;
import client.*;



public class AccountMng extends JPanel {
	private int rows=0;
	static String Username=null;
	//declaration of parameters
	public javax.swing.table.DefaultTableModel model = null;
	private javax.swing.JLabel jLabel1;
	public javax.swing.JButton jButton1;
    public javax.swing.JButton jButton2;
    public javax.swing.JButton jButton3;
    public javax.swing.JButton jButton4;
    public javax.swing.JButton jButton5;
    public javax.swing.JButton jButton6;
    public javax.swing.JButton jButton7;
    public javax.swing.JButton jButton8;
    public javax.swing.JButton jButton21;
    
    
    public javax.swing.JButton jButton9; // button to change privilege 
    public javax.swing.JButton jButton10; // button to delete account
    public javax.swing.JButton jButton11; // button to active account
    public javax.swing.JButton jButton12; // button to frozen account
 
    User u = new User();  //  @jve:decl-index=0:
    
    private JLabel jLabel0 = null;//for the headline 
    private JLabel jLabel = null;//for the name chose 
    private JLabel jLabel3 = null;//for the account chose
    private JLabel jLabel99 = null;
    
    private JLabel jLabel5 = null;// for user name
    private JLabel jLabel6 = null;// for account
    private JLabel jLabel666 = null;
    
    private Choice NameChoice = null;
    private Choice PrivilageChoice = null;
    private Choice changePrivilegeChoice = null;
    public javax.swing.JScrollPane scrollPane;
	
	public static ChatClient client;  //  @jve:decl-index=0:
    private  ChatIF ChatIF = null;
    public int connected;
    
  
    
    public javax.swing.JTable tableUser = null;
	private JScrollPane jScrollPane1 = null;//scrollpanel
	/**
	 * This is the default constructor
	 */
	public AccountMng() 
	{
		super();
		initialize();
	}
	
	

	/**
	 * This method initializes StudentForm
	 */
	private void initialize()
	{
	
		jLabel0 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jButton11 = new javax.swing.JButton();
        jButton12 = new javax.swing.JButton();
        
              
        

        
        this.setSize(1000, 466);
        this.setLayout(null);
        
        
        
        this.add(getJScrollPane1(), null);

        
        jButton1.setText("Search Book");
        jButton1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton1,null);
        jButton1.setBounds(30, 20, 140, 40);
		
        jButton2.setText("Search Review");
        jButton2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton2,null);
        jButton2.setBounds(30, 60, 140, 40);

        jButton3.setText("Manage Books");
        jButton3.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton3,null);
        jButton3.setBounds(30, 100, 140, 40);

        jButton4.setText("Manage Catalog");
        jButton4.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton4,null);
        jButton4.setBounds(30, 140, 140, 40);

        jButton5.setText("Manage Reviews");
        jButton5.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton5,null);
        jButton5.setBounds(30, 180, 140, 40);

        jButton6.setText("Logout");
        jButton6.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton6,null);
        jButton6.setBounds(30, 220, 140, 40);

        jButton7.setText("jButton7");
        jButton7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 0, 0), 3));
        this.add(jButton7,null);
        jButton7.setBounds(30, 260, 140, 40);

        jButton8.setText("Logout");
        jButton8.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton8,null);
        jButton8.setBounds(30, 300, 140, 40);	
  
        
      //for changing privilege
        jButton9.setText("change privilege");
        jButton9.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton9);
        jButton9.setBounds(850, 150, 100, 20);	
        jButton9.addActionListener(new java.awt.event.ActionListener()
        {   
        	public void actionPerformed(java.awt.event.ActionEvent e)
        	{    
        		updatePrivilege();
        		ShowActionPerformed();	

        	}
        
        });

        
        
        //for delete account
        jButton10.setText("Delete Account");
        jButton10.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton10);
        jButton10.setBounds(850, 250, 100, 20);	
        jButton10.addActionListener(new java.awt.event.ActionListener()
        {
        	public void actionPerformed(java.awt.event.ActionEvent e) 
        	{
        		deleteAccount();
        		checkAccountShow();
        	}
        });
           
        
        //for active account
        jButton11.setText("Activate Account");
        jButton11.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton11);
        jButton11.setBounds(700, 250, 100, 20);	
        jButton11.addActionListener(new java.awt.event.ActionListener()
        {
        	public void actionPerformed(java.awt.event.ActionEvent e) 
        	{
        		activeAccount();
        		checkAccountShow();
        	}
        });
        
        
        //for delete account
        jButton12.setText("Froze Account");
        jButton12.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton12);
        jButton12.setBounds(700, 250, 100, 20);	
        jButton12.addActionListener(new java.awt.event.ActionListener()
        {
        	public void actionPerformed(java.awt.event.ActionEvent e) 
        	{
        		frozenAccount();
        		checkAccountShow();
        	}
        });
        
        
        
        
        jLabel3 =new JLabel();
        jLabel3.setText("Sort By :                  User Name");
        jLabel3.setBounds(250, 100, 250, 20);
        this.add(jLabel3);
        
        jLabel =new JLabel();
        jLabel.setText("Privilage");
        jLabel.setBounds(466, 100, 100, 20);
        this.add(jLabel);
        
        
        jLabel99 =new JLabel();
        jLabel99.setText("");
        jLabel99.setBounds(274, 165, 200, 20);
        this.add(jLabel99);
        
        jLabel5 =new JLabel();//for username
        jLabel5.setText("Account Settings Change");
        jLabel5.setBounds(700, 200, 200, 20);
        this.add(jLabel5);
        
        jLabel6 =new JLabel();//for username
        jLabel6.setText("Choosen UserName :");
        jLabel6.setBounds(700, 100, 250, 20);
        this.add(jLabel6);
       
        
        this.add(getNameChoice(), null);
        this.add(getPrivilageChoice(), null);
        this.add(jLabel0);
        
        this.add(getChangePrivilegeChoice(),null);

        
        this.add(getJScrollPane1(), null);
        
 
      //headline
        jLabel0.setFont(new java.awt.Font("Tahoma", 1, 18));
        jLabel0.setText("Account Management");
        jLabel0.setBounds(485, 50, 235, 23);
  
        ////
		
		privinit();

		/* Create Background */
		
		
        jLabel1 = new JLabel();
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Blue1.jpg"))); // NOI18N
        jLabel1.setText("jLabel1");
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        
        unVisibleAccounts();
        unVisiblePrivilege();
        
		
	}// end initialize



	void privinit(){
		/* Setup Connection to server */
        
	       connected = 1;
	   	    try  // trying to connect to server 
	   	    {
	   	    	 client= new ChatClient(u.getHost(),5555,ChatIF);
	   	    } // end try
	   	    catch(IOException exception)
	   	    {
	   	    	JOptionPane.showMessageDialog(null,"Error: Can't setup connection!","Error",JOptionPane.CLOSED_OPTION);
	   	    	connected = 0; // failed to connect
	   	    } // end catch
	        
	   	    /* end setup connection */
	   	    
	   	 /* reader */
	   	if(u.getPriv() == 0){  
	   		jButton3.setText("Account Settings");
	   		jButton4.setText("Logout");
	   		jButton5.setVisible(false);
	   		jButton6.setVisible(false);
	   		jButton7.setVisible(false);
	   		jButton8.setVisible(false);
	   	}
	   	/* Librarian */
	   	else if(u.getPriv() == 1){
	   		jButton7.setVisible(false);
	   		jButton8.setVisible(false);
	   	
	   	
	   	}
	   	/* Library Manager */
	   	/* Library Manager */
	   	else if(u.getPriv() == 2){
	   		jButton6.setText("Manage Reports");
	   		jButton7.setText("Logout");
	   		jButton8.setVisible(false);
	   	
	   	}
	   	/* System Manager */
	   	else if(u.getPriv() == 3){
	      		jButton6.setText("Manage Reports");
	   		jButton7.setText("Account Management");
	   		jButton8.setText("Logout");
	   	}
	}// end privinit

//choice by user name

	  private Choice getNameChoice() {
	        if (NameChoice == null) {
	            NameChoice = new Choice();
	            NameChoice.setBounds(new Rectangle(350, 120, 100, 25));
	            
	            NameChoice.insert("None", 0); 
	            NameChoice.insert("a", 1);
	            NameChoice.insert("b", 2);
	            NameChoice.insert("c", 3);
	            NameChoice.insert("d", 4);
	            NameChoice.insert("e", 5);
	            NameChoice.insert("f", 6);
	            NameChoice.insert("g", 7);
	            NameChoice.insert("h", 8);
             	NameChoice.insert("i", 9);
	            NameChoice.insert("j", 10);
	            NameChoice.insert("k", 11);
               	NameChoice.insert("l", 12);
	            NameChoice.insert("m", 13);
	            NameChoice.insert("n", 14);
	            NameChoice.insert("o", 15);
	            NameChoice.insert("p", 16);
	            NameChoice.insert("q",17);
	            NameChoice.insert("r", 18);
	            NameChoice.insert("s", 19);
	            NameChoice.insert("t", 20);
	            NameChoice.insert("u", 21);
	            NameChoice.insert("v", 22);
              	NameChoice.insert("w", 23);
	            NameChoice.insert("x",24);
	            NameChoice.insert("y", 25);
	            NameChoice.insert("z", 26);
	            NameChoice.setVisible(true);
	            NameChoice.addItemListener(new java.awt.event.ItemListener() {
	            	public void itemStateChanged(java.awt.event.ItemEvent e) {
	            		unVisibleAccounts();
						unVisiblePrivilege();
						ShowActionPerformed();
						}
	            });
	        }
	        return NameChoice;
	    } 

	//choice by Privilage

	  private Choice getPrivilageChoice() {
	         if (PrivilageChoice == null) {
	             PrivilageChoice = new Choice();
	             PrivilageChoice.setBounds(new Rectangle(460, 120, 100, 25));

	             PrivilageChoice.insert("None",0);
	             PrivilageChoice.insert("0",1);
	             PrivilageChoice.insert("1",2);
	             PrivilageChoice.insert("2",3);
	             PrivilageChoice.insert("3",4);

	              
	              PrivilageChoice.setVisible(true);      
	              PrivilageChoice.addItemListener(new java.awt.event.ItemListener() {
	              	public void itemStateChanged(java.awt.event.ItemEvent e) {
	              		unVisibleAccounts();
						unVisiblePrivilege();
						ShowActionPerformed();
						}
	              });
	            }
	         return PrivilageChoice;
	     }
	  
	  
	  private Choice getChangePrivilegeChoice() //choice to change user privilege 
	  {
	        if (changePrivilegeChoice == null) {
	        	changePrivilegeChoice = new Choice();
	        	changePrivilegeChoice.setBounds(new Rectangle(725, 150, 50, 14));
	            
	        	changePrivilegeChoice.insert("0", 0); 
	        	changePrivilegeChoice.insert("1", 1);
	        	changePrivilegeChoice.insert("2", 2);
	        	changePrivilegeChoice.insert("3", 3);
	              
	            changePrivilegeChoice.setVisible(true);      
	            }
	         return changePrivilegeChoice;
	     }
	  
	  
	            
	private void ShowActionPerformed() {// search button was pressed
			
	        
		    int i;
			int j=0;
			int index=1;
	   
			/*
	         *    remove old results
	         */
			
			jScrollPane1.setVisible(false);
			jLabel99.setVisible(false);
	        for(i=this.rows-1;i>=0;i--)
	        	model.removeRow(i);
			
        
			searchAccounts(); // search reviews
						
			AccountM amm = new AccountM();
			
					
			if(amm.getResults(0).equals("no results")){
				jLabel99.setText("No Results Found:");
				jLabel99.setVisible(true); // show no results found
				this.rows=0; // reset last result
			} // end if
			else{ // display results
		
				Object[] row = new Object[5];
				jLabel99.setText("Results Found:");
				jLabel99.setVisible(true);
	       
		        /*
		         * get number of rows in result table
		         */
		             
		        for(i=1;!amm.getResults(i).equals("end1");i++);
		        this.rows=(--i)/5;
		       
		        /*
		         * set results in table
		         */
		        
		        for(i=0;i<rows;i++){//for1     
		        	for(j=0;j<5;j++,index++){//for2
		        		row[j]=amm.getResults(index);	        	      	
		        	}//end for2        	
		            model.addRow(row);            
		        }//end for1
		        jScrollPane1.setVisible(true);
		        jScrollPane1.setBounds(220, 200, 440,17*(rows+1));	// set table size
			}// end else

			
	   }//end event Search button
	
	private void searchAccounts()
	 {
		obj logdt1 = new obj();
    	logdt1.setType("MngShow Account");
    	logdt1.setParam(0,NameChoice.getSelectedItem() ); // set input from text field
    	logdt1.setParam(1,PrivilageChoice.getSelectedItem()); // set filter
    	
    	if(connected == 1)	
   	    	client.handleMessageFromClientUI(logdt1);
    	
	    try {
            Thread.sleep(1000); 
            } // end try
	    catch (InterruptedException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } // end catch
    
    	/* end sql  */
		 
	 }
	 

	/**
	 * This method initializes jScrollPane1	
	 * 	
	 * @return javax.swing.JScrollPane	
	 */
	private JScrollPane getJScrollPane1() {
		if (jScrollPane1 == null) {
			jScrollPane1 = new JScrollPane();

			jScrollPane1.setViewportView(getJTable());
			jScrollPane1.setBounds(new Rectangle(200, 400, 450, 400));
			jScrollPane1.setVisible(false);
		}
		return jScrollPane1;
	}

	/**
	 * This method initializes jTable1	
	 * 	
	 * @return javax.swing.JTable	
	 */
	public JTable getJTable() {
		if (tableUser == null) {     
	        tableUser = new JTable(getModel());	
	        tableUser.setPreferredScrollableViewportSize(new Dimension(300, 200));
	        tableUser.setBounds(new Rectangle(369, 262, 450, 400));
	        tableUser.setFillsViewportHeight(true);
	        model.removeRow(1);
	        model.removeRow(0);
	        
	        /*
	         *     table alignment 
	         */
	        
	        DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
	        dtcr.setHorizontalAlignment(SwingConstants.CENTER);
	        
	        tableUser.getColumn("UserName").setCellRenderer(dtcr);
	        tableUser.getColumn("Name").setCellRenderer(dtcr);
	        tableUser.getColumn("Last Name").setCellRenderer(dtcr);
	        tableUser.getColumn("Privilage").setCellRenderer(dtcr);
	        tableUser.getColumn("Adress").setCellRenderer(dtcr);
	        
	        tableUser.addMouseListener(new MouseAdapter() {
		         public void mouseClicked(MouseEvent e) {
		        	 visiblePrivilege();
		             Username=(String)model.getValueAt(tableUser.getSelectedRow(), 0);
		        	 jLabel6.setText("Choosen UserName: ".concat(Username));
		        	 
		        	 checkAccountShow();
		         }
	        });
	        
		}// end if
		return tableUser;
	}// end getTable


	public TableModel getModel(){
	if (model == null) {
	    String[] columnNames = {"UserName","Name","Last Name","Privilage","Adress"};    
        Object[][] data = {{"","","","",""},{"","","","",""}};
        model = new DefaultTableModel(data,columnNames);
		}// end if
		return model;
	}// end getModel
	
	
	
	private void visiblePrivilege()//function to show the buttons 
	{
		jButton9.setVisible(true);
		jLabel6.setVisible(true);
		changePrivilegeChoice.setVisible(true);
		
	}
	
	private void visibleAccounts()//function to show the buttons 
	{
		jButton10.setVisible(true);
		jButton11.setVisible(true);
		jButton12.setVisible(true);
		jLabel5.setVisible(true);
		
	}
	
	
	private void unVisiblePrivilege()//function to hide the buttons 
	{
		jButton9.setVisible(false);
		jLabel6.setVisible(false);
		changePrivilegeChoice.setVisible(false);
		
	}
	
	private void unVisibleAccounts()//function to hide the buttons 
	{
		jButton10.setVisible(false);
		jButton11.setVisible(false);
		jButton12.setVisible(false);
		jLabel5.setVisible(false);
		
	}



	public  void updatePrivilege()//func for SQL
	{
		
			obj logdt1 = new obj();
	    	logdt1.setType("Update Privilege");
	        logdt1.setParam(0, Username);
	        logdt1.setParam(1,changePrivilegeChoice.getSelectedItem());
	        
	    	if(connected == 1)	
	   	    	client.handleMessageFromClientUI(logdt1);
	    	
		    try {
	            Thread.sleep(1000); 
	            } // end try
		    catch (InterruptedException ex) {
	            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
	        } // end catch
	    
	    	/* end sql  */
			 
		 }
	
	public  void HasAccount()//func for SQL
	{
		
			obj logdt2 = new obj();
	    	logdt2.setType("Has Account");
	        logdt2.setParam(0, Username);
	       
	        
	    	if(connected == 1)	
	   	    	client.handleMessageFromClientUI(logdt2);
	    	
		    try {
	            Thread.sleep(1000); 
	            } // end try
		    catch (InterruptedException ex) {
	            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
	        } // end catch
	    
	    	/* end sql  */
			 
		 }
	
	public void deleteAccount()//func for delete account
	{
		obj logdt3 = new obj();
    	logdt3.setType("Delete Account");
        logdt3.setParam(0, Username);
        
        
       	if(connected == 1)	
   	    	client.handleMessageFromClientUI(logdt3);
    	
	    try {
            Thread.sleep(1000); 
            } // end try
	    catch (InterruptedException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } // end catch
    
    	/* end sql  */
        
	}
	
	
	
	
	public void activeAccount()//func for active account
	{
		obj logdt4 = new obj();
    	logdt4.setType("Active Account");
        logdt4.setParam(0, Username);
        
        
       	if(connected == 1)	
   	    	client.handleMessageFromClientUI(logdt4);
    	
	    try {
            Thread.sleep(1000); 
            } // end try
	    catch (InterruptedException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } // end catch
    
    	/* end sql  */
        
	}
		
	
	
	public void frozenAccount()//func for frozen account
	{
		obj logdt5 = new obj();
    	logdt5.setType("Frozen Account");
        logdt5.setParam(0, Username);
        
        
       	if(connected == 1)	
   	    	client.handleMessageFromClientUI(logdt5);
    	
	    try {
            Thread.sleep(1000); 
            } // end try
	    catch (InterruptedException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } // end catch
    
    	/* end sql  */
        
	}
	
	
	
	
	
	
	
	public void checkAccountShow()
	{

   	 HasAccount();
   	 AccountM amm = new AccountM();
   	 if (!amm.getAct().equals("no account"))
   	 {
   		 visibleAccounts();
   		 if (amm.getAct().equals("1"))
   		 {
   			 jButton11.setVisible(false);
   			 jLabel5.setText("the account is active");
   		 }
   		 else
   		 {
   			 jButton12.setVisible(false);
   			 jLabel5.setText("the account is not active");
   		 }

   	 }
   	 else unVisibleAccounts();
   	}
	
	
}  //  @jve:decl-index=0:visual-constraint="43,16"// end Class
