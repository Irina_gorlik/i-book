package Gui;

import java.awt.Dimension;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Rectangle;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.TableModel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Rectangle;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.EventListener;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.ListSelectionModel;
import common.ChatIF;

import Gui.*;
import logic.*;
import client.*;



public class OrderList extends JPanel {
	  private JLabel jLabel666 = null;
	private javax.swing.JLabel jLabel1;
	public javax.swing.JButton jButton1;
    public javax.swing.JButton jButton2;
    public javax.swing.JButton jButton3;
    public javax.swing.JButton jButton4;
    public javax.swing.JButton jButton5;
    public javax.swing.JButton jButton6;
    public javax.swing.JButton jButton7;
    public javax.swing.JButton jButton8;
    public javax.swing.JLabel jLabel51; //big title
    public javax.swing.JLabel jLabel52; //choose user
    User u = new User();
    Orders o = new Orders();
    
    
    
    public int first=1; // if first time
    public javax.swing.JTable jTable1;
    public javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel jLabel5;
    public javax.swing.table.DefaultTableModel model;
    public javax.swing.JScrollPane scrollPane=null;
    public javax.swing.JTable table1;
    public javax.swing.JTable table=null;
	public javax.swing.JButton Button1; // order list
    public javax.swing.JButton Button2; // book statistics
    public javax.swing.JButton Button3; // book popularity
    public javax.swing.JComboBox jComboBox1=null; // user choise
    public String[] List= new String[200];
    public String[] List2= new String[200];
    public static ChatClient client;
    private  ChatIF ChatIF = null;
    public int connected=1;
    public int rows=0;
    public String str = new String();
    public String tmpStr = new String("");
    public String currentUser = new String();
    
    
    /**
	 * This is the default constructor
	 */
	public OrderList() {
		super();
		initialize();
	}

	/**
	 * This method initializes StudentForm
	 */
	private void initialize() {
		
	        
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jLabel51 = new javax.swing.JLabel();
        jLabel52 = new javax.swing.JLabel();
        
        Button1 = new javax.swing.JButton();
        Button2 = new javax.swing.JButton();
        Button3 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        
        

      
        this.add(getScrollPane(6,true));

        
        this.setSize(1000, 700);
        this.setLayout(null);
        jButton1.setText("Search Book");
        jButton1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton1,null);
        jButton1.setBounds(30, 20, 140, 40);
		
        jButton2.setText("Search Review");
        jButton2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton2,null);
        jButton2.setBounds(30, 60, 140, 40);

        jButton3.setText("Manage Books");
        jButton3.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton3,null);
        jButton3.setBounds(30, 100, 140, 40);

        jButton4.setText("Manage Catalog");
        jButton4.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton4,null);
        jButton4.setBounds(30, 140, 140, 40);

        jButton5.setText("Manage Reviews");
        jButton5.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton5,null);
        jButton5.setBounds(30, 180, 140, 40);

        jButton6.setText("Logout");
        jButton6.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton6,null);
        jButton6.setBounds(30, 220, 140, 40);

        jButton7.setText("jButton7");
        jButton7.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton7,null);
        jButton7.setBounds(30, 260, 140, 40);

        jButton8.setText("Logout");
        jButton8.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton8,null);
        jButton8.setBounds(30, 300, 140, 40);	
		
        
        /*
         *  	Manage Reprts Buttons
         */
        
        jLabel51.setFont(new java.awt.Font("Tahoma", 1, 18));
        jLabel51.setText("Operational monitoring");
        this.add(jLabel51);
        jLabel51.setBounds(420, 35, 250, 22);
        
        
        Button1.setText("Orders List");
        Button1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 0, 0), 3));
        this.add(Button1);
        Button1.setBounds(350,95, 115, 28);
        
        Button2.setText("Book Statistics");
        Button2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(Button2);
        Button2.setBounds(465,95, 115, 28);
        
        Button3.setText("Book Popularity");
        Button3.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(Button3);
        Button3.setBounds(580,95, 115, 28);
        
        jLabel52.setFont(new java.awt.Font("Tahoma", 1, 14));
        jLabel52.setText("Book Orders ");
        this.add(jLabel52);
        jLabel52.setBounds(450, 230, 250, 22);
        jLabel52.setVisible(false);
        this.add(getFacComboBox(), null);
        

        
//        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12));  // filter jLabel11
//        jLabel5.setText("No Results Found ");
//        this.add(jLabel5);
//        jLabel5.setBounds(460, 235, 200, 15);
//        jLabel5.setVisible(false);
//        
//        
        
        
        
        
        
		privinit();

		/* Create Background */
		
        jLabel1 = new javax.swing.JLabel();
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Blue1.jpg"))); // NOI18N
        jLabel1.setText("jLabel1");
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
		/* end Background */
	} // end initialize

	void privinit(){
	   	 /* Reader */
	   	if(u.getPriv() == 0){  
	   		jButton3.setText("Account Settings");
	   		jButton4.setText("Logout");
	   		jButton5.setVisible(false);
	   		jButton6.setVisible(false);
	   		jButton7.setVisible(false);
	   		jButton8.setVisible(false);
	   	}
	   	/* Librarian */
	   	else if(u.getPriv() == 1){
	   		jButton7.setVisible(false);
	   		jButton8.setVisible(false);
	   	
	   	
	   	}
	   	/* Library Manager */
	   	else if(u.getPriv() == 2){
	   		jButton6.setText("Manage Reports");
	   		jButton7.setText("Logout");
	   		jButton8.setVisible(false);
	   	
	   	}
	   	/* System Manager */
	   	else if(u.getPriv() == 3){
	      		jButton6.setText("Manage Reports");
	   		jButton7.setText("Account Management");
	   		jButton8.setText("Logout");
	   	}
	}// end privinit
	
	
	/**
	 * This method initializes facComboBox	
	 */
	private JComboBox getFacComboBox() {
		
		if (jComboBox1 == null) {
			jComboBox1 = new JComboBox();
					jComboBox1.setBounds(new Rectangle(455,175, 140, 27));

			jComboBox1.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Object[] objectRow = new Object[4];
					
					int i,j;
					int index=0;
					if(!jComboBox1.getSelectedItem().equals("Choose User")){
						currentUser=(String)jComboBox1.getSelectedItem();
						getOrders((String)jComboBox1.getSelectedItem());
						
						List2 = (String[]) o.getOrders();
				        

				        objectRow=List2;
				      
				        
						/*
				         *    remove old results
				         */
						
						jLabel52.setVisible(true);
						jLabel52.setText(""+currentUser+" Book Orders ");
				        for(i=rows-1;i>=0;i--)
				        	model.removeRow(i);
				        
				        
				        rows=List2.length/4;  // get number of rows
				        
				        
				        /*
				         * set results in table
				         */
				      
				        System.out.println("Order rows: "+rows);
				        for(i=0;i<rows;i++){//for1     
				        	for(j=0;j<4;j++,index++){//for2
				        		objectRow[j]=List2[index];	        	
				        	
				        	}//end for2        	
				        		addRow(objectRow); 
				        }//end for1

				        scrollPane.setBounds(270, 270, 600,17*(rows+1));	// set table size
//				        scrollPane.setBounds(300, 270, 440,17*(rows+1));	// set table size
						
//						System.out.println("JComboBox actionPerformed()");
					}// end if
				}
			});
		}
		return jComboBox1;
	}

	/*
	 * 		loads all user
	 */
	void loadUsers(Orders Or,int first){
		
		this.o=Or;
		
		if(this.first == 1) {
			this.first = first;
		this.List = (String[]) o.getUsers();
//		System.out.println("OrderList List length: "+List.length);
		jComboBox1.removeAllItems();
		
		
		jComboBox1.addItem("Choose User");
		
		
		for (int i = 0; i < List.length-1; i++) {
			jComboBox1.addItem((Object)List[i]);
			}
		jComboBox1.addItem("All");
			} // end if
		} // end loadUsers()
	
	
	/*
	 * 		loads all user
	 */

	
	
	
	public TableModel getModel(){
		if (model == null) {
		    String[] columnNames = {"Title","ISBN","User","Date"};    
	        Object[][] data = {{"","","",""},{"","","",""}};
	        model = new DefaultTableModel(data,columnNames);
			}// end if
			return model;
		}// end getModel
	
	public void addRow(Object[] object){
		model.addRow(object);
	}// addRow
	
	public JTable getTable() {
		if (table == null) {     
	        table = new JTable(getModel());	
	        table.setPreferredScrollableViewportSize(new Dimension(300, 70));
	        table.setFillsViewportHeight(true);
	        table.setEnabled(false);
	        TableColumn column = table.getColumnModel().getColumn(0);
	   
	        column.setPreferredWidth(150);
	        model.removeRow(1);
	        model.removeRow(0);
	        /*
	         *     table alignment 
	         */	        
	        DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
	        dtcr.setHorizontalAlignment(SwingConstants.CENTER);
//	        table.getColumn("Title").setCellRenderer(dtcr);
	        table.getColumn("ISBN").setCellRenderer(dtcr);
	        table.getColumn("User").setCellRenderer(dtcr);
	        table.getColumn("Date").setCellRenderer(dtcr);

	        
	        
		}// end if
		return table;
	}// end getTable
	
	
	public void getOrders(String user){ // this function will connect to server to search reviews

    	obj logdt1 = new obj();
    	logdt1.setType("get orders");
    	logdt1.setParam(0,user); // set username
  
    	/* Setup Connection to server */
 
   	    try  // trying to connect to server 
   	    {
   	    	 client= new ChatClient(u.getHost(),5555,ChatIF);
   	    } // end try
   	    catch(IOException exception)
   	    {
   	    	JOptionPane.showMessageDialog(null,"Error: Can't setup connection!","Error",JOptionPane.CLOSED_OPTION);
   	    	connected = 0; // failed to connect
   	    } // end catch
        
   	    /* end setup connection */

       	if(connected == 1)	
   	    	client.handleMessageFromClientUI(logdt1);  	
	    try {
            Thread.sleep(100); 
            } // end try
	    catch (InterruptedException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } // end catch
	    		
	    		/* end sql  */

	}// end getOrders()
	
	public JScrollPane getScrollPane(int row,boolean bol){
        if(scrollPane==null){
        	scrollPane = new JScrollPane(getTable());    
//       	add(scrollPane);
        	 scrollPane.setVisible(true);
//        	 scrollPane.setBounds(300, 270, 440,17*(row+1));	// set table size
        }
        else{
        	
        System.out.println("getScrollPane");
        scrollPane.setVisible(bol);
        scrollPane.setBounds(300, 270, 440,17*(row+1));	// set table size

        }//else
		return scrollPane;
	}// end getScrollPane
	
	
	
} // end Class
