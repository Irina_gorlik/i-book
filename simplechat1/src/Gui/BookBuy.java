package Gui;

import java.awt.Choice;
import java.awt.Font;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Rectangle;

import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;

import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;

import common.ChatIF;

import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.text.SimpleDateFormat;

import Gui.*;
import logic.*;
import client.*;

import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;



public class BookBuy extends JPanel {
	  private JLabel jLabel666 = null;
   public String Title = null;
   public String Price = null;
   public String Isbn = null;
	/**
	 *  Instance of int instance, 
	 *  to save the connection to the server status 
	 *  1 = connected , else not connected
	 */    
    public int connected;
    /**
	 * Instance of ChatClient 
	 * To run the Chat Client functions
	 */   
    public static ChatClient client;  //  @jve:decl-index=0:
    /**
	 * Instance of ChatIF 
	 * To check the connection
	 */   
	private  ChatIF ChatIF = null;
    /**
     * Instance of User Class in logic
     * to save the user details
     */
	User u = new User();  //  @jve:decl-index=0:
	 /**
     * Instance of Accounts Class in logic
     * to save the account details
     */
	Accounts a = new Accounts();  //  @jve:decl-index=0:
	/**
     * Instance of Book Class in logic
     * to save the chosen book details
     */
	Book b = new Book();  //  @jve:decl-index=0:
    /**
	 * Instance of Choice
	 * set the file format(downloaded file)
	 */   
	private Choice choice = null;
	/**
	 * Instance of JButton 
	 * go back to Search book
	 */
	public javax.swing.JButton jButton1;
	
     /**
     * Instance of JLabel for the BookBy Form
     * to set  the back ground icon
     */
    private javax.swing.JLabel jLabel1;
    
    /**
     * Instance of JLabel show where the account detail 
     * and where the selected book details
     */
    public javax.swing.JLabel jLabel51;
   	/**
	 * Instance of jTextField to present the Isbn of selected book
	 */
	private JTextField jTextIsbn = null;
		/**
	 * Instance of jTextField to present the Title of selected book
	 */
	private JTextField jTextTitle = null;
	
	/**
	 * Instance of jTextField to present the Price of selected book
	 */
	private JTextField jTextPrice = null;
	
	/**
	 * Instance of jTextField to present the Name of the user
	 */
	private JTextField jTextName = null;
	/**
	 * Instance of jTextField to present the Last Name of the user
	 */
	private JTextField jTextLName = null;
	/**
	 * Instance of jTextField to present the Account type(details) of the user
	 */
	private JTextField jTextAcDetails = null;
	/**
	 * Instance of JButton
	 * action Create Account
	 */
//	private JButton ButtonCreateAccount = null;
	/**
	 * Instance of JButton
	 * action Download the book
	 */	
	private JButton Buttondownload = null;
	/**
	 * Instance of JLabel with text Choice
	 */	
	private JLabel jLabeChoice = null;
	/**
	 * Instance of JLabel with text Details
	 */	
	private JLabel jLabelAcDetails = null;
	/**
	 * Instance of JLabel with text Isbn
	 */	
	private JLabel jLabelIsbn = null;
	/**
	 * Instance of JLabel with text Title
	 */	
	private JLabel jLabelTitle = null;
	/**
	 * Instance of JLabel with text Price
	 */	
	private JLabel jLabelPrice = null;
	/**
	 * Instance of JLabel with text first name
	 */	
	private JLabel jLabelName = null;
	/**
	 * Instance of JLabel with text last name
	 */	
	private JLabel jLabelLName = null;
	/**
	 * Instance of String the file name to save
	 */	
	  private String fname = new String(),
	  /**
	   * Instance of String the new file dir to save into
	   */
	                      fdir = new String();
	    
	  /**
       * Instance of JButton
       * go to details of the book
       */
	   public javax.swing.JButton Button1; //Details
	   /**
	       * Instance of JButton
	       * go to review of the book
	       */ 
	   public javax.swing.JButton Button2; //Reviews
	   /**
	       * Instance of JButton
	       * go to write review of the book
	       */ 
	   public javax.swing.JButton Button3;	//Write Reveiw
	      /**
	       * Instance of JButton
	       * noting happen when pressed
	       */ 
	   public javax.swing.JButton Button4; //Buy
	    /**
         * Instance of JButton
         * accept to buy this book button
         * when pressed update the data base 
         * and show the file choice to download
         */
	    public javax.swing.JButton Button5; //Save 


    
	/**
	 * This is the default constructor
	 */
	public BookBuy() {
		super();
		initialize();
	}

	/**
	 * This method initializes BookBuy
	 */
	private void initialize() {
	
		
		
		jLabeChoice = new JLabel();
		jLabeChoice.setBounds(new Rectangle(350, 220, 108, 20));
		jLabeChoice.setText("Choose FORMAT :");
		jLabel51 = new JLabel();
        jLabel51.setBounds(new Rectangle(350, 100, 360, 32));
        jLabel51.setText("");
        jLabel51.setFont(new Font("Tahoma", 1, 18));
        this.add( getChoice(),null);
        this.add(jLabel51);
        jLabelAcDetails = new JLabel();
        jLabelAcDetails.setBounds(new Rectangle(550, 250, 100, 20));
        jLabelAcDetails.setText("Details : ");
        jLabelLName = new JLabel();
        jLabelLName.setBounds(new Rectangle(550, 200, 100, 20));
        jLabelLName.setText("L Name : ");
        jLabelName = new JLabel();
        jLabelName.setBounds(new Rectangle(550, 150, 100, 20));
        jLabelName.setText("Name : ");
        jLabelPrice = new JLabel();
        jLabelPrice.setBounds(new Rectangle(350, 250, 100, 20));
        jLabelPrice.setText("Price : ");
        jLabelTitle = new JLabel();
        jLabelTitle.setBounds(new Rectangle(350, 200, 100, 20));
        jLabelTitle.setText("Title : ");
        jLabelIsbn = new JLabel();
        jLabelIsbn.setBounds(new Rectangle(350, 150, 100, 20));
        jLabelIsbn.setText("Isbn : ");
        jButton1 = new javax.swing.JButton();
        
       
        Button1 = new javax.swing.JButton();
        Button2 = new javax.swing.JButton();
        Button3 = new javax.swing.JButton();
        Button4 = new javax.swing.JButton();
        
        Button5 = new javax.swing.JButton();
   
        
        this.setSize(1000, 700);
        this.setLayout(null);
    
        
        
        jButton1.setText("Back");
        jButton1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton1,null);
        jButton1.setBounds(460, 500, 100, 25);
		
        /*
         *    Write Review Buttons
         */
        
        Button1.setText("Overview");
        Button1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(Button1);
        Button1.setBounds(330,50, 110, 29);
        
        Button2.setText("Reviews");
        Button2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
//        this.add(Button2);
        Button2.setBounds(380,50, 110, 29);
        
        Button3.setText("Write Review");
        Button3.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(Button3);
        Button3.setBounds(440,50, 110, 29);
        
        Button4.setText("Buy");
        Button4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 0, 0), 3));
        this.add(Button4);
        Button4.setBounds(550,50, 110, 29);
        
        
        Button5.setText("Accept");
        Button5.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(Button5);
        this.add(getJTextIsbn(), null);
        this.add(jLabelIsbn, null);
        this.add(jLabelTitle, null);
        this.add(getJTextTitle(), null);
        this.add(jLabelPrice, null);
        this.add(getJTextPrice(), null);
        this.add(jLabelName, null);
        this.add(getJTextName(), null);
        this.add(jLabelLName, null);
        this.add(getJTextLName(), null);
        this.add(jLabelAcDetails, null);
        this.add(getJTextAcDetails(), null);
//        this.add(getButtonCreateAccount(), null);
        this.add(jLabeChoice, null);
        this.add(getButtondownload(), null);
        Button5.setBounds(467, 330, 90, 23);
        Button5.addActionListener(new java.awt.event.ActionListener() {   
        	public void actionPerformed(java.awt.event.ActionEvent e) {    
        		BuyBookFromServer();
        		AllsetVisibleFalse();
        	  jLabel51.setText("Download it !");
              jLabeChoice.setVisible(true);
              choice.setVisible(true);
              Buttondownload.setVisible(true); 
              
              Button5.setVisible(false);
//              ButtonCreateAccount.setVisible(false);
              
              
        	
        	}
        
        });
   
        
		privinit();
	


		/* Create Background */
		
        jLabel1 = new javax.swing.JLabel();
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Blue1.jpg"))); // NOI18N
        jLabel1.setText("jLabel1");
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
		/* end Background */
	} // end initialize

	/**
	 * verify the connection with the server
	 */
	void privinit(){

		/* Setup Connection to server */
        
	       connected = 1;
	   	    try  // trying to connect to server 
	   	    {
	   	    	 client= new ChatClient(u.getHost(),5555,ChatIF);
	   	    } // end try
	   	    catch(IOException exception)
	   	    {
	   	    	JOptionPane.showMessageDialog(null,"Error: Can't setup connection!","Error",JOptionPane.CLOSED_OPTION);
	   	    	connected = 0; // failed to connect
	   	    } // end catch
	        
	   	    /* end setup connection */
  
	}// end privinit
	/**
	 * initialize the Account details of a
	 * and sets the Book buy form
	 * @param a
	 */
	public void	InitBook(Accounts a)
	{
		this.a = a;

		SetFormToView();
	    
	}
	/**
	 * refresh the panel bookbuy
	 * if the user has account view the buy/download button
	 * if the user don't have account show the create  account button
	 */
private void		SetFormToView(){
	if (!a.IshasTheBook())
	{
		if (a.IshasAccounts())
			AllsetVisibleTrue();
		else
			AllsetVisibleFalse();
	}
	else
	{
		AllsetVisibleFalse();
		jLabel51.setText("Download it !");
		 jLabeChoice.setVisible(true);
         choice.setVisible(true);
         Buttondownload.setVisible(true); 
//         ButtonCreateAccount.setVisible(false);         
		
	}
		

}

/**
 * sets all the label components that used when you 
 * have account not visible
 */
private void AllsetVisibleFalse()
{
	jLabeChoice.setVisible(false);
    choice.setVisible(false);
    Buttondownload.setVisible(false); 
	jLabel51.setText("    Sorry, you have NO account !");
	jLabelAcDetails.setVisible(false);
	jLabelIsbn.setVisible(false);
	jLabelLName.setVisible(false);
	jLabelPrice.setVisible(false);
	jLabelName.setVisible(false);
	jLabelTitle.setVisible(false);
	Button5.setVisible(false);
	
	jTextAcDetails.setVisible(false);
	jTextIsbn.setVisible(false);
	jTextLName.setVisible(false);
	jTextName.setVisible(false);
	jTextPrice.setVisible(false);
	jTextTitle.setVisible(false);
//	ButtonCreateAccount.setVisible(true);
	
	
}
/**
 * sets all the label components that used when you 
 * have account visible
 * sets the text to the components
 */
private void AllsetVisibleTrue()
{
	jLabeChoice.setVisible(false);
    choice.setVisible(false);
    Buttondownload.setVisible(false);  
    
	jLabel51.setText("Book Details :               Account Details :");
	jLabelAcDetails.setVisible(true);
	jLabelIsbn.setVisible(true);
	jLabelLName.setVisible(true);
	jLabelPrice.setVisible(true);
	jLabelName.setVisible(true);
	jLabelTitle.setVisible(true);
	Button5.setVisible(true);
	
	jTextAcDetails.setText(a.getDetail());
	jTextAcDetails.setVisible(true);
	
	jTextIsbn.setText(b.getIsbn());
	jTextIsbn.setVisible(true);
	
	jTextLName.setText(u.getLast_name());
	jTextLName.setVisible(true);
	
	jTextName.setText(u.getFirst_name());
	jTextName.setVisible(true);
	
	jTextPrice.setText(Float.toString(b.getPrice()));
	jTextPrice.setVisible(true);
	System.out.println("get title: "+b.getTitle());
	jTextTitle.setText(b.getTitle());
	jTextTitle.setVisible(true);
//	ButtonCreateAccount.setVisible(false);
}

/**
 * This method initializes jTextIsbn	
 * 	
 * @return javax.swing.JTextField	
 */
private JTextField getJTextIsbn() {
	if (jTextIsbn == null) {
		jTextIsbn = new JTextField();
		jTextIsbn.setBounds(new Rectangle(350, 170, 150, 20));
	}
	return jTextIsbn;
}

/**
 * This method initializes jTextTitle	
 * 	
 * @return javax.swing.JTextField	
 */
private JTextField getJTextTitle() {
	if (jTextTitle == null) {
		jTextTitle = new JTextField();
		jTextTitle.setBounds(new Rectangle(350, 220, 150, 20));
	}
	return jTextTitle;
}

/**
 * This method initializes jTextPrice	
 * 	
 * @return javax.swing.JTextField	
 */
private JTextField getJTextPrice() {
	if (jTextPrice == null) {
		jTextPrice = new JTextField();
		jTextPrice.setBounds(new Rectangle(350, 270, 150, 20));
	}
	return jTextPrice;
}

/**
 * This method initializes jTextName	
 * 	
 * @return javax.swing.JTextField	
 */
private JTextField getJTextName() {
	if (jTextName == null) {
		jTextName = new JTextField();
		jTextName.setBounds(new Rectangle(550, 170, 150, 20));
	}
	return jTextName;
}

/**
 * This method initializes jTextLName	
 * 	
 * @return javax.swing.JTextField	
 */
private JTextField getJTextLName() {
	if (jTextLName == null) {
		jTextLName = new JTextField();
		jTextLName.setBounds(new Rectangle(550, 220, 150, 20));
	}
	return jTextLName;
}

/**
 * This method initializes jTextAcDetails	
 * 	
 * @return javax.swing.JTextField	
 */
private JTextField getJTextAcDetails() {
	if (jTextAcDetails == null) {
		jTextAcDetails = new JTextField();
		jTextAcDetails.setBounds(new Rectangle(550, 270, 150, 20));
	}
	return jTextAcDetails;
}

/**
 * This method initializes ButtonCreateAccount	
 * 	
 * @return javax.swing.JButton	
 */
//private JButton getButtonCreateAccount() {
//	if (ButtonCreateAccount == null) {
//		ButtonCreateAccount = new JButton();
//		ButtonCreateAccount.setBounds(new Rectangle(423, 174, 152, 23));
//		ButtonCreateAccount.setText("Create Account");
//		ButtonCreateAccount.setBorder(new SoftBevelBorder(BevelBorder.RAISED));
//		ButtonCreateAccount.setVisible(false);
//	}
//	return ButtonCreateAccount;
//}
/**
 * create the choice of file types to download
 * @return choice instance
 */
private Choice getChoice() {
	if (choice == null) {
		choice = new Choice();
		choice.setBounds(new Rectangle(450, 220, 100, 25));
		
		choice.insert("pdf", 0);
		choice.insert("doc", 1);
		choice.insert("fb2", 2);
		
	}
	return choice;
}

/**
 * This method initializes Buttondownload	
 * when you pressed the download button
 * the file downloaded from the server to users computer
 * pop up save as menu where you choose the location to save 
 * the downloaded file in
 * save the file 	
 * @return javax.swing.JButton	
 */
private JButton getButtondownload() {
	if (Buttondownload == null) {
		Buttondownload = new JButton();
		Buttondownload.setBounds(new Rectangle(550, 220, 100, 20));
		Buttondownload.setText("Download");
		Buttondownload.setBorder(new SoftBevelBorder(BevelBorder.RAISED));
		Buttondownload.addActionListener(new java.awt.event.ActionListener() {   
			public void actionPerformed(java.awt.event.ActionEvent e) {    
				
				DownloadFromServer();
				// File (or directory) to be moved
				String name = b.getTitle().concat(".").concat(choice.getSelectedItem()); 
		        
				File file = new File(b.getTitle().concat(".").concat(choice.getSelectedItem()));

				
				JFileChooser c = new JFileChooser();
			      // Demonstrate "Save" dialog:
				c.setSelectedFile(file);
		        c.setAcceptAllFileFilterUsed(false);
		            
		        
			        int rVal = c.showSaveDialog(BookBuy.this);
			        
			        if (rVal == JFileChooser.APPROVE_OPTION) {
			    	   
			        fname = c.getSelectedFile().getName();
			        fdir = c.getCurrentDirectory().toString();
			        
			        fdir = fdir.replace('/', File.separatorChar).replace('\\', File.separatorChar);
      				// Destination directory
			        
					File dir = new File(fdir);

					// Move file to new directory
					boolean success = file.renameTo(new File(dir, file.getName()));
					
			        
			      }
			      if (rVal == JFileChooser.CANCEL_OPTION) {
			        fname = "You pressed cancel";
			        fdir = "";
			      }
			    }
		
		});
	}
	return Buttondownload;
}
/**
 * send the massege Buy Book Server to the server over the client
 * with the Isbn and the user name of the user
 * update the database
 */
private void BuyBookFromServer()
{
	obj logdt1 = new obj();
	logdt1.setType("Buy Book Server");
	
	logdt1.setParam(0,b.getIsbn()   ); // set filter
	logdt1.setParam(1,u.getUser()   ); // set filter
	System.out.println("******************************************\n");
	System.out.println(" USER : " + u.getUser()+" BOOK : " +b.getIsbn());
	if(connected == 1)	
	    	client.handleMessageFromClientUI(logdt1);
	
    try {
        Thread.sleep(1000); 
        } // end try
    catch (InterruptedException ex) {
        Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
    } // end catch

	/* end sql  */

}
/**
 * send the massege download FILE Server to the server over the client
 * with the Isbn and format to download
 * creates a file with this name and format in the root directory
 */
private void DownloadFromServer()
{
	obj logdt1 = new obj();
	logdt1.setType("Download FILE");
	
	logdt1.setParam(0,b.getIsbn()   ); // set filter
	logdt1.setParam(1,choice.getSelectedItem()  ); // set filter
	
	if(connected == 1)	
	    	client.handleMessageFromClientUI(logdt1);
	
    try {
        Thread.sleep(1000); 
        } // end try
    catch (InterruptedException ex) {
        Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
    } // end catch

	/* end sql  */

}

void loadBook(Orders Or,Book book) {
	String pic = new String();
	String[] tmpList = new String[200];
	int rate;
	this.b = book;
	
	tmpList = (String[]) Or.getBooks();


    int rows=tmpList.length/10;  // get number of rows

	
	jTextIsbn.setText(tmpList[0]); //isbn
	jTextPrice.setText(tmpList[9]); //price
    jTextTitle.setText(tmpList[1]);
	b.setTitle(tmpList[1]);
	b.setIsbn(tmpList[0]);

	System.out.println("***********b "+b.getTitle() + b.getIsbn());
} // end loadUsers()




}  //  @jve:decl-index=0:visual-constraint="51,13" // end Class
