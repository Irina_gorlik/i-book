package Gui;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Rectangle;

import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import javax.swing.JButton;
import java.awt.Rectangle;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.border.Border;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.*;

import Gui.*;
import logic.*;
import client.*;
import common.ChatIF;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JScrollPane;



public class ManageBooks extends JPanel {
	  private JLabel jLabel666 = null;
    private javax.swing.JLabel jLabel1;
    public javax.swing.JButton jButton1;
    public javax.swing.JButton jButton2;
    public javax.swing.JButton jButton3;
    public javax.swing.JButton jButton4;
    public javax.swing.JButton jButton5;
    public javax.swing.JButton jButton6;
    public javax.swing.JButton jButton7;
    public javax.swing.JButton jButton8;
    /** user  information**/
    User u = new User();  //  @jve:decl-index=0:
    /** book information  **/
    bookInfo b = new bookInfo();  //  @jve:decl-index=0:
    /** book field list  **/
    book_field bf = new book_field();	
    /** all catalog list, fields and subjects  **/
    CatalogList cl = new CatalogList();	//@jve:decl-index=0:
    /** book catalog information **/
    CatalogBook cb = new CatalogBook();  //  @jve:decl-index=0:
    
       // Variables declaration - do not modify   
    public javax.swing.JLabel Heading_lbl;
    private javax.swing.JButton new_btn;
    private javax.swing.JButton save_btn;
    private javax.swing.JButton delete_btn;    
	private JLabel isbn_lbl = null;
	private JTextField search_text = null;
	private JButton search_btn = null;
	private JInternalFrame jInternalFrame = null;
	private JPanel jContentPane = null;
	private JLabel pdf = null;
	private JLabel doc_icon = null;
	private JLabel fb2_icon = null;
	private JLabel author_lbl1 = null;
	private JLabel summery_lbl11 = null;
	private JLabel title_lbl = null;
	private JLabel lang_lbl11 = null;
	private JLabel field_lbl111 = null;
	private JComboBox s_criteria_ComboBox = null;
	private JTextField language_text1 = null;
	
	private JComboBox field_ComboBox1 = null;
	private JLabel isbn_lbl11 = null;
	private JTextField isbn_text11 = null;
	private JLabel content_lbl1111 = null;
	private JLabel rating_lbl1111 = null;
	private JLabel viewed_lbl11111 = null;
	private JLabel purchased_lbl = null;
	private JLabel rating_v_lbl = null;
	private JLabel viewed_v_lbl = null;
	private JLabel purchased_v_lbl = null;
	private JButton catalog_btn1 = null;
	private JButton addPicture_btn = null;
	private JButton fb2_btn1111 = null;
	private JButton doc_btn = null;
	private JButton pdf_btn = null;
	private JTextArea summery_jTextArea = null;
	private JTextArea content_jTextArea = null;
	private JTextArea title_jTextArea = null;
	public javax.swing.table.DefaultTableModel model;
	public javax.swing.JTable table1;
    public javax.swing.JTable table=null;
    
	private JScrollPane books_ScrollPane = null;
	/** connection to server  **/
	public static ChatClient client;  //  @jve:decl-index=0:
    private  ChatIF ChatIF = null;
    /** if connected to server =1 else =0  **/
    public int connected;
    /** number of rows in search results  **/
    public int rows=0;
       
    Color C = new java.awt.Color(153,0,0);
	private JComboBox author_ComboBox = null;
	private JLabel noResults_jLabel = null;
	private JLabel price_jLabel = null;
	private JTextField price_jTextField = null;
	private JButton add_author_jButton = null;
	private JButton remove_author_jButton = null;
	private JButton add_field_jButton = null;
	private JButton remove_field_jButton = null;
	private JComboBox ctlFields_jComboBox = null;
	private JButton addCtlField_jButton = null;
	private JButton removeCtlField_jButton = null;
	private JComboBox ctlSubject_jComboBox = null;
	private JButton addCtlSubject_jButton = null;
	private JButton removeCtlSubject_jButton = null;
	private JLabel CtlField_jLabel = null;
	private JLabel CtlSubject_jLabel = null;
	
        
	/**
	 * This is the default constructor
	 */
	public ManageBooks() 
	{
		super();
		initialize();
	}

	/**
	 * This method initializes StudentForm
	 */
	private void initialize() 
	{		
		
	        
        CtlSubject_jLabel = new JLabel();
        CtlSubject_jLabel.setBounds(new Rectangle(446, 633, 57, 29));
        CtlSubject_jLabel.setText("Subject");
        CtlField_jLabel = new JLabel();
        CtlField_jLabel.setBounds(new Rectangle(315, 633, 49, 27));
        CtlField_jLabel.setText("Field");
        price_jLabel = new JLabel();
        price_jLabel.setBounds(new Rectangle(761, 542, 74, 19));
        price_jLabel.setText("Price");
        noResults_jLabel = new JLabel();
        noResults_jLabel.setBounds(new Rectangle(367, 117, 121, 13));
        noResults_jLabel.setText("No Results Found");
        noResults_jLabel.setVisible(false);
        purchased_v_lbl = new JLabel();
        purchased_v_lbl.setBounds(new Rectangle(395, 507, 40, 24));
        purchased_v_lbl.setToolTipText("Sale Anount");
        purchased_v_lbl.setDisplayedMnemonic(KeyEvent.VK_UNDEFINED);
        purchased_v_lbl.setText("0");
        purchased_v_lbl.setFont(new Font("Tahoma", Font.BOLD, 12));
        purchased_v_lbl.setVisible(false);
        viewed_v_lbl = new JLabel();
        viewed_v_lbl.setBounds(new Rectangle(318, 507, 40, 24));
        viewed_v_lbl.setToolTipText("# of Viesw");
        viewed_v_lbl.setDisplayedMnemonic(KeyEvent.VK_UNDEFINED);
        viewed_v_lbl.setText("0");
        viewed_v_lbl.setFont(new Font("Tahoma", Font.BOLD, 12));
        viewed_v_lbl.setVisible(false);
        rating_v_lbl = new JLabel();
        rating_v_lbl.setBounds(new Rectangle(226, 510, 38, 26));
        rating_v_lbl.setToolTipText("Avarage Book Rating");
        rating_v_lbl.setDisplayedMnemonic(KeyEvent.VK_UNDEFINED);
        rating_v_lbl.setText("0");
        rating_v_lbl.setFont(new Font("Tahoma", Font.BOLD, 12));
        rating_v_lbl.setVisible(false);
        purchased_lbl = new JLabel();
        purchased_lbl.setBounds(new Rectangle(379, 485, 65, 22));
        purchased_lbl.setToolTipText("The Isbn Number of the Book");
        purchased_lbl.setDisplayedMnemonic(KeyEvent.VK_UNDEFINED);
        purchased_lbl.setText("Purchased");
        purchased_lbl.setFont(new Font("Tahoma", Font.BOLD, 12));
        viewed_lbl11111 = new JLabel();
        viewed_lbl11111.setBounds(new Rectangle(306, 484, 56, 23));
        viewed_lbl11111.setToolTipText("The Isbn Number of the Book");
        viewed_lbl11111.setDisplayedMnemonic(KeyEvent.VK_UNDEFINED);
        viewed_lbl11111.setText("Viewed");
        viewed_lbl11111.setFont(new Font("Tahoma", Font.BOLD, 12));
        rating_lbl1111 = new JLabel();
        rating_lbl1111.setBounds(new Rectangle(223, 484, 48, 28));
        rating_lbl1111.setToolTipText("The Isbn Number of the Book");
        rating_lbl1111.setDisplayedMnemonic(KeyEvent.VK_UNDEFINED);
        rating_lbl1111.setText("Rating");
        rating_lbl1111.setFont(new Font("Tahoma", Font.BOLD, 12));
        content_lbl1111 = new JLabel();
        content_lbl1111.setBounds(new Rectangle(589, 232, 69, 25));
        content_lbl1111.setToolTipText("The Isbn Number of the Book");
        content_lbl1111.setDisplayedMnemonic(KeyEvent.VK_UNDEFINED);
        content_lbl1111.setText("Content");
        content_lbl1111.setFont(new Font("Tahoma", Font.BOLD, 12));
        isbn_lbl11 = new JLabel();
        isbn_lbl11.setBounds(new Rectangle(194, 166, 60, 22));
        isbn_lbl11.setToolTipText("The Isbn Number of the Book");
        isbn_lbl11.setDisplayedMnemonic(KeyEvent.VK_UNDEFINED);
        isbn_lbl11.setText("ISBN");
        isbn_lbl11.setFont(new Font("Tahoma", Font.BOLD, 12));
        field_lbl111 = new JLabel();
        field_lbl111.setBounds(new Rectangle(411, 197, 44, 26));
        field_lbl111.setToolTipText("The Isbn Number of the Book");
        field_lbl111.setDisplayedMnemonic(KeyEvent.VK_UNDEFINED);
        field_lbl111.setText("field");
        field_lbl111.setFont(new Font("Tahoma", Font.BOLD, 12));
        lang_lbl11 = new JLabel();
        lang_lbl11.setBounds(new Rectangle(194, 197, 64, 26));
        lang_lbl11.setToolTipText("The Isbn Number of the Book");
        lang_lbl11.setDisplayedMnemonic(KeyEvent.VK_UNDEFINED);
        lang_lbl11.setText("Language");
        lang_lbl11.setFont(new Font("Tahoma", Font.BOLD, 12));
        title_lbl = new JLabel();
        title_lbl.setBounds(new Rectangle(197, 135, 46, 21));
        title_lbl.setToolTipText("The Isbn Number of the Book");
        title_lbl.setDisplayedMnemonic(KeyEvent.VK_UNDEFINED);
        title_lbl.setText("Title");
        title_lbl.setFont(new Font("Tahoma", Font.BOLD, 12));
        summery_lbl11 = new JLabel();
        summery_lbl11.setBounds(new Rectangle(294, 232, 65, 24));
        summery_lbl11.setToolTipText("The Isbn Number of the Book");
        summery_lbl11.setDisplayedMnemonic(KeyEvent.VK_UNDEFINED);
        summery_lbl11.setText("Summery");
        summery_lbl11.setFont(new Font("Tahoma", Font.BOLD, 12));
        author_lbl1 = new JLabel();
        author_lbl1.setBounds(new Rectangle(410, 165, 53, 22));
        author_lbl1.setToolTipText("The Isbn Number of the Book");
        author_lbl1.setDisplayedMnemonic(KeyEvent.VK_UNDEFINED);
        author_lbl1.setText("Auhtor");
        author_lbl1.setFont(new Font("Tahoma", Font.BOLD, 12));
        fb2_icon = new JLabel();
        fb2_icon.setBounds(new Rectangle(516, 492, 40, 43));
        fb2_icon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/fb2.png"))); 
        doc_icon = new JLabel();
        doc_icon.setBounds(new Rectangle(596, 493, 38, 42));
        doc_icon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/doc-small.jpg"))); 
        pdf = new JLabel();
        pdf.setBounds(new Rectangle(679, 492, 42, 44));
        pdf.setDisplayedMnemonic(KeyEvent.VK_UNDEFINED);
        pdf.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/pdf_small.jpg")));
        isbn_lbl = new JLabel();
        isbn_lbl.setBounds(new Rectangle(196, 91, 54, 17));
        isbn_lbl.setDisplayedMnemonic(KeyEvent.VK_UNDEFINED);
        isbn_lbl.setFont(new Font("Tahoma", Font.BOLD, 12));
        isbn_lbl.setToolTipText("The Isbn Number of the Book");
        isbn_lbl.setText("Search");
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();

        Heading_lbl = new javax.swing.JLabel();
        new_btn = new javax.swing.JButton();
        save_btn = new javax.swing.JButton();
        delete_btn = new javax.swing.JButton();
        
        this.setSize(1000, 700);
        this.setLayout(null);
        jButton1.setText("Search Book");
        jButton1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton1,null);
        jButton1.setBounds(30, 20, 140, 40);
		
        jButton2.setText("Search Review");
        jButton2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton2,null);
        jButton2.setBounds(30, 60, 140, 40);

        jButton3.setText("Manage Books");
        jButton3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 0, 0), 3));
        this.add(jButton3,null);
        jButton3.setBounds(30, 100, 140, 40);

        jButton4.setText("Manage Catalog");
        jButton4.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton4,null);
        jButton4.setBounds(30, 140, 140, 40);

        jButton5.setText("Manage Reviews");
        jButton5.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton5,null);
        jButton5.setBounds(30, 180, 140, 40);

        jButton6.setText("Logout");
        jButton6.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton6,null);
        jButton6.setBounds(30, 220, 140, 40);

        jButton7.setText("jButton7");
        jButton7.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton7,null);
        jButton7.setBounds(30, 260, 140, 40);

        jButton8.setText("Logout");
        jButton8.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton8,null);
        jButton8.setBounds(30, 300, 140, 40);	
        this.add(getfield_ComboBox1(), null);        		
        /**
         * Setup Connection to server
         */ 
		connected = 1;
		try  // trying to connect to server
		{
        	client= new ChatClient(u.getHost(),5555,ChatIF);
		} catch(IOException exception)
		{
        	JOptionPane.showMessageDialog(null,"Error: Can't setup connection!","Error",JOptionPane.CLOSED_OPTION);
        	connected = 0; // failed to connect
		} // end catch
	    /**
	     *  end setup connection 
	     **/
		
		privinit();
		
		fb2_icon.setVisible(false);
		doc_icon.setVisible(false);
		pdf.setVisible(false);
         Heading_lbl.setFont(new java.awt.Font("Tahoma", 1, 18));
         Heading_lbl.setText("Manage Books");
         this.add(Heading_lbl);
         Heading_lbl.setBounds(436, 45, 140, 22);
		/* Create Background */
         add(new_btn);
         new_btn.setBounds(196, 535, 80, 23);
         new_btn.setText("New");
         new_btn.addActionListener(new java.awt.event.ActionListener() 
         {
         	public void actionPerformed(java.awt.event.ActionEvent evt) 
         	{
         		System.out.println("New Book Pressed"); 
         		new_btnActionPerformed(evt);
         	}
         });
         add(save_btn);
         save_btn.setBounds(392, 537, 80, 23);
         save_btn.setText("Save");
         save_btn.addActionListener(new java.awt.event.ActionListener() 
         {
         	public void actionPerformed(java.awt.event.ActionEvent evt) 
         	{
         		System.out.println("Save Book actionPerformed()"); // TODO Auto-generated Event stub actionPerformed()
         		String str;
			    save_bookActionPerformed(evt);
         	}
         });
         add(delete_btn);
         this.add(isbn_lbl, null);
         this.add(getSearch_text(), null);
         this.add(getSearch_btn(), null);
         this.add(pdf, null);
         this.add(doc_icon, null);
         this.add(fb2_icon, null);
         this.add(author_lbl1, null);
         this.add(summery_lbl11, null);
         this.add(title_lbl, null);
         this.add(lang_lbl11, null);
         this.add(field_lbl111, null);
         this.add(getS_criteria_ComboBox(), null);
         this.add(getLanguage_text1(), null);
         
         this.add(isbn_lbl11, null);
         this.add(getIsbn_text11(), null);         
         this.add(content_lbl1111, null);
         this.add(rating_lbl1111, null);
         this.add(viewed_lbl11111, null);
         this.add(purchased_lbl, null);
         this.add(rating_v_lbl, null);
         this.add(viewed_v_lbl, null);
         this.add(purchased_v_lbl, null);
         this.add(getCatalog_btn1(), null);
         this.add(getAddPicture_btn(), null);
         this.add(getFb2_btn1111(), null);
         this.add(getDoc_btn(), null);
         this.add(getPdf_btn(), null);
         this.add(getSummery_jTextArea(), null);
         this.add(getContent_jTextArea(), null);
         this.add(getTitle_jTextArea(), null);
         this.add(getBooks_ScrollPane(), null);
         this.add(getAuthor_ComboBox(), null);
         this.add(noResults_jLabel, null);
         this.add(price_jLabel, null);
         this.add(getPrice_jTextField(), null);
         this.add(getAdd_author_jButton(), null);
         this.add(getRemove_author_jButton(), null);
         this.add(getAdd_field_jButton(), null);
         this.add(getRemove_field_jButton(), null);
         this.add(getCtlFields_jComboBox(), null);
         this.add(getAddCtlField_jButton(), null);
         this.add(getRemoveCtlField_jButton(), null);
         this.add(getCtlSubject_jComboBox(), null);
         this.add(getAddCtlSubject_jButton(), null);
         this.add(getRemoveCtlSubject_jButton(), null);
         this.add(CtlField_jLabel, null);
         this.add(CtlSubject_jLabel, null);
         delete_btn.setBounds(293, 536, 94, 24);
         delete_btn.setText("Delete");
         delete_btn.addActionListener(new java.awt.event.ActionListener() {
         	public void actionPerformed(java.awt.event.ActionEvent evt) {
         		System.out.println("Delete Book actionPerformed()"); 
         		delete_btnActionPerformed(evt);
         	}
         });
         /**
          * Create Background
          */
         jLabel1 = new javax.swing.JLabel();
         jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Blue1.jpg"))); // NOI18N
         jLabel1.setText("jLabel1");
         javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
         this.setLayout(layout);
         layout.setHorizontalGroup(
         layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
		/* end Background */
	} // end initialize
	/**
	 * initialize menue buttons
	 */
	void privinit()
	{				
	   	 /* Reader */
	   	if(u.getPriv() == 0){  
	   		jButton3.setText("Account Settings");
	   		jButton4.setText("Logout");
	   		jButton5.setVisible(false);
	   		jButton6.setVisible(false);
	   		jButton7.setVisible(false);
	   		jButton8.setVisible(false);
	   	}
	   	/* Librarian */
	   	else if(u.getPriv() == 1){
	   		jButton7.setVisible(false);
	   		jButton8.setVisible(false);
	   	
	   	
	   	}
	
		   	/* Library Manager */
		   	else if(u.getPriv() == 2){
		   		jButton6.setText("Manage Reports");
		   		jButton7.setText("Logout");
		   		jButton8.setVisible(false);
	   	
	   	}
	   	/* System Manager */
	   	else if(u.getPriv() == 3){
	      		jButton6.setText("Manage Reports");
	   		jButton7.setText("Account Management");
	   		jButton8.setText("Logout");
	   	}
	}// end privinit

	/**
	 * This method initializes search_text	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getSearch_text() {
		if (search_text == null) {
			search_text = new JTextField();
			search_text.setBounds(new Rectangle(256, 90, 324, 23));
		}
		return search_text;
	}

	/**
	 * This method initializes search_btn	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getSearch_btn() {
		if (search_btn == null) 
		{
			search_btn = new JButton();
			search_btn.setBounds(new Rectangle(768, 90, 100, 25));
			search_btn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/log-arrlog.gif"))); // NOI18N
			search_btn.setText("Search ");
			search_btn.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
			search_btn.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
			search_btn.addActionListener(new java.awt.event.ActionListener() 
			{
				/**
				 * performing search according to text search
				 */
				public void actionPerformed(java.awt.event.ActionEvent evt) 
				{
					System.out.println("Starting Search Of " + s_criteria_ComboBox.getSelectedItem());
					try
					{
						search_btnActionPerformed(evt);
					} catch(Exception ex)
					{
						System.out.println("Really Big Exception MSG: " + ex.getMessage());
					}
				
				}
			});
		}
		return search_btn;
	}

	/**
	 * This method initializes jInternalFrame	
	 * 	
	 * @return javax.swing.JInternalFrame	
	 */
	private JInternalFrame getJInternalFrame() {
		if (jInternalFrame == null) {
			jInternalFrame = new JInternalFrame();
			jInternalFrame.setBounds(new Rectangle(381, 194, 92, 52));
			jInternalFrame.setContentPane(getJContentPane());
		}
		return jInternalFrame;
	}

	/**
	 * This method initializes jContentPane	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
		}
		return jContentPane;
	}

	/**
	 * This method initializes s_criteria_ComboBox	
	 * 	setting search filters
	 * @return javax.swing.JComboBox	
	 */
	private JComboBox getS_criteria_ComboBox() 
	{
		String[] searchTopics = { "Filter", "ISBN", "Title", "Author" };
		if (s_criteria_ComboBox == null)
		{
			s_criteria_ComboBox = new JComboBox(searchTopics);
			s_criteria_ComboBox.setBounds(new Rectangle(596, 90, 161, 24));
			s_criteria_ComboBox.setSelectedIndex(0);
		}
		return s_criteria_ComboBox;
	}
	/**
	 * This method initializes language_text1	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getLanguage_text1() {
		if (language_text1 == null) {
			language_text1 = new JTextField();
			language_text1.setBounds(new Rectangle(255, 198, 137, 22));
			language_text1.addFocusListener(new java.awt.event.FocusAdapter() {
				/**
				 * updating book instance with language typed
				 */
				public void focusLost(java.awt.event.FocusEvent e) 
				{
					b.setLanguage(language_text1.getText());
				}
			});
		}
		return language_text1;
	}
	/**
	 * This method initializes field_ComboBox1	
	 * 	
	 * @return javax.swing.JComboBox	
	 */
	private JComboBox getfield_ComboBox1() 
	{
			
		if (field_ComboBox1 == null) 
		{
			field_ComboBox1 = new JComboBox();								
			field_ComboBox1.setBounds(new Rectangle(465, 200, 128, 23));
		}
		return field_ComboBox1;
	}
	/**
	 * This method initializes isbn_text11	
	 * 	If Focus Lost It Updates the instances with new ISBN
	 * @return javax.swing.JTextField	
	 */
	@SuppressWarnings("deprecation")
	private JTextField getIsbn_text11() {
		if (isbn_text11 == null) {
			isbn_text11 = new JTextField();
			isbn_text11.setBounds(new Rectangle(255, 165, 136, 21));
			isbn_text11.addFocusListener(new java.awt.event.FocusAdapter() {
				public void focusLost(java.awt.event.FocusEvent e) 
				{
					b.setIsbn(isbn_text11.getText());
					cb.setIsbn(isbn_text11.getText());
				}
			});
		}
		return isbn_text11;
	}
	/**
	 * This method initializes catalog_btn1	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getCatalog_btn1() {
		if (catalog_btn1 == null) {
			catalog_btn1 = new JButton();
			catalog_btn1.setBounds(new Rectangle(75, 587, 170, 23));
			catalog_btn1.setText("Add To Catalog");
			catalog_btn1.addActionListener(new java.awt.event.ActionListener() {
				/**
				 * this button add/remove book from catalog
				 * if it's not in catalog it enables fields that enables
				 * user to add fields and subjects
				 * if book in catalog it removes the book entirely from catalog
				 * deletes from every catalog table book instance
				 */
				public void actionPerformed(java.awt.event.ActionEvent e) 
				{
					if (cb.getIsbn() != null)
					{
						if (cb.isInCatalog())
						{
							System.out.println("Remove from catalog actionPerformed()");
							removeFromCatalogActionPerformed(e);
							disableCatalog();
							cb.setInCatalog(false);
							cb.clearCatalogFields();
							cb.clearCatalogSubjects();
						}
						else
						{
							System.out.println("Add to Catalog actionPerformed()");
							enableCatalog();
							cb.setInCatalog(true);
						}
					}
				}
			});
		}
		return catalog_btn1;
	}
	
	/**
	 * Delete Current Book from Catalog
	 * ## NOTE # this will only delete from catalog, catalog field, catalog subject
	 * @param evt
	 */
	private void removeFromCatalogActionPerformed(java.awt.event.ActionEvent evt)
	{	
		obj bookdt = new obj();
		bookdt.setType("***remove book from catalog***");
		bookdt.setParam(0, cb.getIsbn());
		if (connected == 1)
			client.handleMessageFromClientUI(bookdt);
		try
		{
			Thread.sleep(100);
		} catch (InterruptedException ex)
		{
			Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);			
		}
	}
	/**
	 * This method initializes addPicture_btn	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getAddPicture_btn() {
		if (addPicture_btn == null) {
			addPicture_btn = new JButton();
			addPicture_btn.setBounds(new Rectangle(566, 568, 107, 24));
			addPicture_btn.setText("Add Picture");
			addPicture_btn.addActionListener(new java.awt.event.ActionListener() 
			{
				/**
				 * opening select file dialog with jpg filter to pick picture to uplaod to DB
				 */
				public void actionPerformed(java.awt.event.ActionEvent e) 
				{
					if (b.getIsbn() != null)
					{
						System.out.println("OK, Add Picture actionPerformed()"); 
						File picFile;
						picFile = select_file("Jpg Files", "jpg");	//selecting file
						if (picFile != null)
						{
							truncButtos();
							uplaod_file("jpg",picFile);
							Border green_line;
							green_line = BorderFactory.createLineBorder(Color.green);
							addPicture_btn.setBorder(green_line);
							addPicture_btn.setText("Replace");
						}//if not null
					} //a book must be chosen to upload file
					else
					{
						JOptionPane.showMessageDialog(null,"Error: No Book Selected","Error",JOptionPane.CLOSED_OPTION);
					}
				}
			});
		}
		return addPicture_btn;
	}

	/**
	 * This method initializes fb2_btn1111	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getFb2_btn1111() {
		if (fb2_btn1111 == null) {
			fb2_btn1111 = new JButton();
			fb2_btn1111.setBounds(new Rectangle(493, 541, 84, 19));
			fb2_btn1111.setText("Add Fb2");
			fb2_btn1111.addActionListener(new java.awt.event.ActionListener() 
			{
				/**
				 * opening select file dialog with fb2 filter to pick book to uplaod to DB
				 */
				public void actionPerformed(java.awt.event.ActionEvent e) 
				{
					if (b.getIsbn() != null)
					{
						
						File fb2File;
						System.out.println("add fb2 file actionPerformed()"); 
						/**
						 * selecting file
						 */
						fb2File = select_file("fb2 Files", "fb2");
						if (fb2File != null)
						{
							truncButtos();
							uplaod_file("fb2",fb2File);
							Border green_line;
							green_line = BorderFactory.createLineBorder(Color.green);
							fb2_btn1111.setBorder(green_line);
							fb2_btn1111.setText("Replace");
						}
					}
					else
					{
						JOptionPane.showMessageDialog(null,"Error: No Book Selected","Error",JOptionPane.CLOSED_OPTION);
					}
				}
			});
		}
		return fb2_btn1111;
	}
	/**
	 * This method initializes doc_btn	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getDoc_btn() {
		if (doc_btn == null) {
			doc_btn = new JButton();
			doc_btn.setBounds(new Rectangle(581, 541, 83, 20));
			doc_btn.setText("Add Doc");
			doc_btn.addActionListener(new java.awt.event.ActionListener() {
				/**
				 * opening select file dialog with doc filter to pick book to uplaod to DB
				 */
				public void actionPerformed(java.awt.event.ActionEvent e) 
				{
					if (b.getIsbn() != null)
					{
						File docFile;
						System.out.println("add Doc file actionPerformed()"); // TODO Auto-generated Event stub actionPerformed()
						/**
						 * selecting file
						 */
						docFile = select_file("Doc Files", "doc");
						if (docFile != null)
						{
							truncButtos();
							uplaod_file("doc",docFile);
							Border green_line;
							green_line = BorderFactory.createLineBorder(Color.green);
							doc_btn.setBorder(green_line);
							doc_btn.setText("Replace");
						}
					}
					else
					{
						JOptionPane.showMessageDialog(null,"Error: No Book Selected","Error",JOptionPane.CLOSED_OPTION);
					}
				}
			});
		}
		return doc_btn;
	}

	/**
	 * This method initializes pdf_btn	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getPdf_btn() {
		if (pdf_btn == null) {
			pdf_btn = new JButton();
			pdf_btn.setBounds(new Rectangle(664, 541, 83, 20));
			pdf_btn.setText("Add Pdf");
			pdf_btn.addActionListener(new java.awt.event.ActionListener() 
			{
				/**
				 * opening select file dialog with pdf filter to pick book to uplaod to DB
				 */
				public void actionPerformed(java.awt.event.ActionEvent e) 
				{
					if (b.getIsbn() != null)
					{
						File pdfFile;
						System.out.println("Add Pdf actionPerformed()");
						/**
						 * selecting file
						 */
						pdfFile = select_file("Pdf Files", "pdf");	
						if (pdfFile != null)
						{
							uplaod_file("pdf",pdfFile);
							truncButtos();
							Border green_line;
							green_line = BorderFactory.createLineBorder(Color.green);
							pdf_btn.setBorder(green_line);
							pdf_btn.setText("Replace");
						}//if not null
					}
					else
					{
						JOptionPane.showMessageDialog(null,"Error: No Book Selected","Error",JOptionPane.CLOSED_OPTION);
					}
				}//action performed
			});
		}
		return pdf_btn;
	}

	/**
	 * This method initializes summery_jTextArea	
	 * 	on focuslost it updates current book info
	 * @return javax.swing.JTextArea	
	 */
	private JTextArea getSummery_jTextArea() {
		if (summery_jTextArea == null) {
			summery_jTextArea = new JTextArea();
			summery_jTextArea.setBounds(new Rectangle(194, 259, 286, 222));
			summery_jTextArea.addFocusListener(new java.awt.event.FocusAdapter() {
				public void focusLost(java.awt.event.FocusEvent e) 
				{
					b.setSummery(summery_jTextArea.getText());
				}
			});
		}
		return summery_jTextArea;
	}

	/**
	 * This method initializes content_jTextArea	
	 * 	
	 * @return javax.swing.JTextArea	
	 */
	private JTextArea getContent_jTextArea() {
		if (content_jTextArea == null) {
			content_jTextArea = new JTextArea();
			content_jTextArea.setBounds(new Rectangle(510, 256, 223, 227));
			content_jTextArea.addFocusListener(new java.awt.event.FocusAdapter() {
				public void focusLost(java.awt.event.FocusEvent e) 
				{
					b.setContents(content_jTextArea.getText());
				}
			});
		}
		return content_jTextArea;
	}

	/**
	 * This method initializes title_jTextArea	
	 * 	
	 * @return javax.swing.JTextArea	
	 */
	private JTextArea getTitle_jTextArea() {
		if (title_jTextArea == null) {
			title_jTextArea = new JTextArea();
			title_jTextArea.setBounds(new Rectangle(256, 136, 390, 18));
			title_jTextArea.addFocusListener(new java.awt.event.FocusAdapter() {
				public void focusLost(java.awt.event.FocusEvent e) 
				{
					b.setTitle(title_jTextArea.getText());
				}
			});
		}
		return title_jTextArea;
	}
	/**
	 * trunc all the buttons to be without border
	 */
	public void truncButtos()
	{
		Border no_border;
		no_border = BorderFactory.createEmptyBorder();
		
		addPicture_btn.setBorder(no_border);
		save_btn.setBorder(no_border);
		delete_btn.setBorder(no_border);
		fb2_btn1111.setBorder(no_border);
		doc_btn.setBorder(no_border);
		pdf_btn.setBorder(no_border);
	}
	/**
	 * this procedure clears all the fields in the panel
	 */
	public void truncFields()
	{
		Border no_border;
		no_border = BorderFactory.createEmptyBorder();
		truncButtos();
		int i;
		title_jTextArea.setText("");
		summery_jTextArea.setText("");
		content_jTextArea.setText("");
		isbn_text11.setText("");		
		author_ComboBox.removeAllItems();
		field_ComboBox1.removeAllItems();
		language_text1.setText("");
		rating_v_lbl.setText("0");
		rating_v_lbl.setVisible(true);
		viewed_v_lbl.setText("0");
		viewed_v_lbl.setVisible(true);		
		purchased_v_lbl.setText("0");
		purchased_v_lbl.setVisible(true);
		//search_text.setText("");
		fb2_icon.setVisible(false);
		fb2_btn1111.setBorder(no_border);
		fb2_btn1111.setText("Add fb2");
		doc_icon.setVisible(false);
		doc_btn.setBorder(no_border);
		doc_btn.setText("Add Doc");
		pdf.setVisible(false);
		pdf.setBorder(no_border);
		pdf.setText("Add PDF");
		save_btn.setBorder(no_border);
		price_jTextField.setText("0");
		/**
		 * clearing book scrollpane
		 */
		books_ScrollPane.setVisible(false);
		for(i=this.rows-1;i>=0;i--)
        model.removeRow(i);
		rows = 0;
	}
	/**
	 * entering book data into the form
	 */
	private void setBookInfo()
	{
		int i=0;
		//search_text.setText("");
		isbn_text11.setText(b.getIsbn());
		title_jTextArea.setText(b.getTitle());
		language_text1.setText(b.getLanguage());
		summery_jTextArea.setText(b.getSummery());
		content_jTextArea.setText(b.getContents());
		rating_v_lbl.setText(b.getRating());		
		rating_v_lbl.setVisible(true);
		viewed_v_lbl.setText(b.getViewed());
		viewed_v_lbl.setVisible(true);		
		purchased_v_lbl.setText(b.getPurchased());
		purchased_v_lbl.setVisible(true);
		if (b.getPdf() == null)	
			{
				pdf.setVisible(false);
				pdf_btn.setText("Add PDF");
			}
			else 
			{
				pdf.setVisible(true);
				pdf_btn.setText("Replace");
			}
		if (b.getDoc() == null)	
			{
				doc_icon.setVisible(false);
				doc_btn.setText("Add Doc");
			}
			else 
			{
				doc_icon.setVisible(true);
				doc_btn.setText("Replace");
			}
		if (b.getFb2() == null)	
			{
				fb2_icon.setVisible(false);
				fb2_btn1111.setText("Add fb2");
			}
			else 
			{
				fb2_btn1111.setText("Replace");
				fb2_icon.setVisible(true);
			}
		addPicture_btn.setText("Add Picture");
		price_jTextField.setText(b.getPrice());
		truncButtos();
		//author_text1.setText("AUTHOR ERROR");
		author_ComboBox.removeAllItems();
		/**
   	 	 * Author
   	 	 */
		for (i=0;i<b.getAuthorCount();i++)
		{
			author_ComboBox.addItem(b.getAuthor(i));
		}
		/**
   	 	 * Field List
   	 	 */
		field_ComboBox1.removeAllItems();
		for (i=0;i<b.getFieldCount();i++)
		{
			field_ComboBox1.addItem(b.getfield(i));
		}
	}
	/**
	 * Delete Current Book from DB
	 * ## NOTE # this will only delete book from book, catalog, book field, catalog field, catalog subject, book author
	 * @param evt
	 */
	private void delete_btnActionPerformed(java.awt.event.ActionEvent evt)
	{	
		if (b.getIsbn() != null)
		{
			int answer;
			answer = JOptionPane.showConfirmDialog(null, "Really Delete?", "Input",
      				JOptionPane.YES_NO_OPTION);
			if(answer == JOptionPane.YES_OPTION)
			{
				disableCatalog();
				obj bookdt = new obj();
				bookdt.setType("***delete book***");
				bookdt.setParam(0, b.getIsbn());
				if (connected == 1)
					client.handleMessageFromClientUI(bookdt);
				try
				{
					Thread.sleep(100);
				} catch (InterruptedException ex)
				{
					Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);			
				}
				//truncFields();
				search_btnActionPerformed(evt);
				b.setIsbn(null);
				cb.setIsbn(null);
			}
		}
		else
		{
			JOptionPane.showMessageDialog(null,"Error: No Book Selected","Error",JOptionPane.CLOSED_OPTION);
		}
	}
	/**
	 * creating new book with empty fields
	 * @param evt
	 */
	private void new_btnActionPerformed(java.awt.event.ActionEvent evt)
	{
		int i;
		Border no_border;
		no_border = BorderFactory.createEmptyBorder();
		obj bookdt = new obj();
		truncFields();			
		enableCatalog();
		
		bookdt.setType("new book");
		if (connected == 1)
			client.handleMessageFromClientUI(bookdt);
		try
		{
			Thread.sleep(100);
		} catch (InterruptedException ex)
		{
			Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);			
		}
		bookInfo b = new bookInfo();
		
		System.out.println("ISBN = " + b.getIsbn());
		isbn_text11.setText(b.getIsbn());
		cb.setIsbn(b.getIsbn());
		System.out.println("Catalog ISBN = " + cb.getIsbn());
		price_jTextField.setText("0");
		b.setPrice("0");
		b.setTitle("No Title");
		b.setLanguage("No Language");
		b.setSummery("No Summery");
		b.setContents("No Contents");
		fb2_btn1111.setBorder(no_border);
		fb2_btn1111.setText("Add fb2");
		doc_btn.setBorder(no_border);
		doc_btn.setText("Add Doc");
		pdf.setBorder(no_border);
		save_btn.setBorder(no_border);
		pdf.setText("Add PDF");
		addPicture_btn.setText("Add Picture");
	}
	/**
	 * Search books in DB according to filter and user search input
	 * after we get the answer from server
	 * we analyze the data and set book results in result table
	 * @param evt
	 */
	private void search_btnActionPerformed(java.awt.event.ActionEvent evt)
	{
		String input;
		String filter;
		int i,j=0,index=1;
		/**
		 * Clear Previews Results		 
		 */
		truncFields();
		if (!search_text.getText().isEmpty())	/** if search text not empty **/
		{			
			input = search_text.getText();		//search text
			filter = (String) s_criteria_ComboBox.getSelectedItem();	//criteria		
			
			searchBook(input,filter);
			
			bookInfo b = new bookInfo();
			System.out.println(b.getResults(0)+b.getResults(1));
			if(b.getResults(1).equals("***END***"))
			{
				noResults_jLabel.setText("No Results Found:");
				noResults_jLabel.setVisible(true); // show no results found
				this.rows=0; // reset last result
			} // end if
			else
			{ // display results
				Object[] row = new Object[2];
				noResults_jLabel.setText("Results Found");
				noResults_jLabel.setVisible(true);
				/**
		         * get number of rows in result table
		         **/
		        for(i=1;(!b.getResults(i).equals("***END***"));i++)
		        {
		        		if (b.getResults(i).equals("***book files start***"))
		        			i+=5;	//skipping file comparison
		        		if (b.getResults(i).equals("***book info start***"))
		        			this.rows++;
		        		while (b.getResults(i+1) == null)
		        		{
		        			i++;
		        		}
		        }
		        /**
		         * set results in table
		         **/
		        for(i=0;i<rows;i++)
		        {//for1 
		        	while (!b.getResults(index).equals("***book info start***")) 	//finding
		        	{		        		
		        		if (b.getResults(index).equals("***book files start***"))
		        			index+=5;	//skipping file comparison
		        		index++;
		        		while (b.getResults(index) == null)
		        		{
		        			index++;
		        		}
		        	}
		        		
		        		row[0]=b.getResults(++index);	
		        		row[1]=b.getResults(++index);
		        		model.addRow(row); 		        		
		        		while (b.getResults(index) == null)
		        		{
		        			index++;
		        		}
		        }//end for1
		        books_ScrollPane.setVisible(true);
		        books_ScrollPane.setBounds(796, 149, 180,17*(rows+1));	// set table size
			}//end else
		}
		else 
		{
			noResults_jLabel.setText("No Search Input Entered");
			noResults_jLabel.setVisible(true);
		}
	}
	/**
	 * send to server search book request
	 * @param input	text to search
	 * @param filter criteria of search
	 */
	public void searchBook(String input, String filter)
	{ 
    	obj logdt1 = new obj();
    	logdt1.setType("search manage book");
    	logdt1.setParam(0,input); // set input from text field
    	logdt1.setParam(1,filter); // set filter
    	
       	if(connected == 1)	
   	    	client.handleMessageFromClientUI(logdt1);
    	
	    try {
            Thread.sleep(1500); 
            } // end try
	    catch (InterruptedException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } // end catch
    
    	/* end sql  */

	}// end book
	/**
	 * this function update entire field list of books
	 * @param e
	 */
	public String[] getFieldList()
	{
		obj fielddt = new obj();
		int k=0;
		fielddt.setType("get book field list");
		if (connected == 1)
			client.handleMessageFromClientUI(fielddt);
		try
		{
			Thread.sleep(100);
		} catch (InterruptedException ex)
		{
			Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);			
		}	
		//CHANGE
		book_field bf = new book_field();		
		System.out.println("Got fields");
		return book_field.getBookField();		
	}
	/**
	 * This method initializes books_ScrollPane	
	 * 	
	 * @return javax.swing.JScrollPane	
	 */
	private JScrollPane getBooks_ScrollPane() {
		if (books_ScrollPane == null) 
		{
			books_ScrollPane = new JScrollPane(getTable());
			books_ScrollPane.setBounds(new Rectangle(796, 149, 180, 145));
			books_ScrollPane.setToolTipText("Choose Book To See It's Details");
			books_ScrollPane.setVisible(true);
		}
		return books_ScrollPane;
	}
	/**
	 * Creating table for results from search
	 * @return
	 */
	public JTable getTable() {
		if (table == null) {     
	        table = new JTable(getModel());	
	        table.setPreferredScrollableViewportSize(new Dimension(300, 70));
	        table.setFillsViewportHeight(true);
	        model.removeRow(1);
	        model.removeRow(0);
	        
	        /**
	         * table alignment 
	         **/
	        
	        DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
	        dtcr.setHorizontalAlignment(SwingConstants.CENTER);
	        table.getColumn("ISBN").setCellRenderer(dtcr);
	        table.getColumn("Title").setCellRenderer(dtcr);    
	        
	    	table.addMouseListener(new MouseAdapter() 
	    	{
	    		/**
	    		 * we chose a book from result table
	    		 * when we choose the book
	    		 * we find this book in search result list
	    		 * and update current book with his info
	    		 * after that we request from server book catalog info
	    		 * after we get it we set book catalog info in book catalog info variable
	    		 */
	            public void mouseClicked(MouseEvent e) 
	            {
	            	int index=1;
	            	
	            	/**
	            	 * we chose a book from search results
	            	 * now we gonna set current bookinfo instance to be the selected book
	            	 * and also current CatalogBook
	            	 * we will set all of bookinfo fields 
	            	 * after that we set Gui fields to be according to bookinfo
	            	 */
	            	
	           	 	b.setIsbn((String)model.getValueAt(table.getSelectedRow(), 0));
	            	cb.setIsbn(b.getIsbn());
	            	
	           	 	try
	           	 	{
		           	 	while (!b.getResults(index).equals(b.getIsbn())) 	//finding book in book list
			        	{		        		
			        		if (b.getResults(index).equals("***book files start***"))
			        			index+=5;	//skipping file comparison
			        		index++;
			        		while (b.getResults(index) == null)
			        		{
			        			index++;
			        		}
			        	}
	           	 	} catch (NullPointerException ex)
	           	 	{
	           	 		System.out.println("NULL Exception at finding ***book ISBN start***, ERROR: " + ex.getMessage());
	           	 	}
	           	 	b.setTitle(b.getResults(++index));
	           	 	if (b.getTitle() == null)	b.setTitle("No Title");
	           	 	b.setLanguage(b.getResults(++index));
	           	 	if (b.getLanguage() == null)	b.setLanguage("No Language");
	           	 	b.setSummery(b.getResults(++index));
	           	 	if (b.getSummery() == null)	b.setSummery("No Summery");
	           	 	b.setContents(b.getResults(++index));
	           	 	if (b.getContents() == null)	b.setContents("No Contents");
	           	 	b.setRating(b.getResults(++index));
	           	 	b.setViewed(b.getResults(++index));
	           	 	b.setPurchased(b.getResults(++index));
	           	 	index++;	//ignore system msg
	           	 	b.setBook_art(b.getResults(++index));
	           	 	b.setPdf(b.getResults(++index));
	           	 	b.setDoc(b.getResults(++index));
	           	 	b.setFb2(b.getResults(++index));
	           	 	b.setPrice(b.getResults(++index));
	           	 	if (b.getPrice() == null)	b.setPrice("0");
	           	 	/**
	           	 	 * author
	           	 	 */
	           	 	b.clearauthors();
	           	 	try
	           	 	{
		           	 	while (!b.getResults(index).equals("***book author start***")) 	//finding author start signal in book list
			        	{		
			        		if (b.getResults(index).equals("***book files start***"))
			        			index+=5;	//skipping file comparison
			        		index++;
			        		while (b.getResults(index) == null)
			        		{
			        			index++;
			        		}
			        	}
	        	 	} catch (NullPointerException e1)
	           	 	{
	           	 		System.out.println("NULL Exception at finding ***book author start***, ERROR:" + e1.getMessage());
	           	 	}
	           	 	index++;
	           	 	try
	           	 	{
		           	 	while (!b.getResults(index).equals("***book author end***")) 	//finding author stop signal in book list
			        	{		        
		           	 		b.add_author(b.getResults(index++));
			        	}
	           	 	} catch (NullPointerException e2)
	           	 	{
	           	 		System.out.println("NULL Exception at finding ***book author end***, ERROR:" + e2.getMessage());
	           	 	}
	           	 	/**
	           	 	 * book_fields
	           	 	 */
	           	 	b.clearFields();
	           	 	//System.out.println("before field start: " + b.getResults(index));
	           	 	try
	           	 	{
		           	 	while (!b.getResults(index).equals("***book field start***")) 	//finding field start signal in book list
			        	{		        		
			        		if (b.getResults(index).equals("***book files start***"))
			        			index+=5;	//skipping file comparison
			        		index++;
			        		while (b.getResults(index) == null)
			        		{
			        			index++;
			        		}
			        	}
	           	 	} catch (NullPointerException e3)
	           	 	{
	           	 		System.out.println("NULL Exception at finding ***book author end***, ERROR:" + e3.getMessage());
	           	 	}
	           	 		           	 	
	           	 	index++;
	           	 	while (!b.getResults(index).equals("***book field end***")) 	//finding field stop signal in book list
		        	{		        	
	           	 		b.add_book_field(b.getResults(index++));
		        	}
	           	 	/**
	           	 	 * after bookinfo initialized we set GUI fields to held this info
	           	 	 */
	           	 	getBookCatalogInfo();
	           	 	setBookInfo();	
	           	 	truncButtos();
	            }
	        });
		}// end if
		return table;
	}// end getTable
	/**
	 * this method sets catalog information of the selected book
	 * it request from server information
	 * and gets a string of book fields and objects
	 * then we analyze the answer and set book catalog info
	 */
	public void getBookCatalogInfo()
	{
		int index=0;
		if (cb.getIsbn() != null)	/** if book exists **/
		{
			requestBookCatalog();
			CatalogBook cb = new CatalogBook();
			try
			{
				System.out.println("ILAN" + cb.getCatalogResults(1));
			} catch (NullPointerException ex)
			{
				System.out.println("ERR, Null at catalog results");
			}
			if (cb.isInCatalog())
			{
				enableCatalog();
				catalog_btn1.setText("Remove From Catalog");
				cb.clearCatalogFields();
				try
				{
					while (!cb.getCatalogResults(index).equals("***catalog book field start***")) 	//finding catalog field signal in book list
					{
						index++;
						while (cb.getCatalogResults(index) == null)
						{
							index++;
						}
					}
				}catch (NullPointerException ex)
				{
					System.out.println("NULL Exception at finding **catalog book field start***, ERROR:" + ex.getMessage());
				}
				index++;
           	 	try
           	 	{
	           	 	while (!cb.getCatalogResults(index).equals("***catalog book field end***")) 	//finding catalog field stop signal in list
		        	{		        
	           	 		cb.add_catalog_field(cb.getCatalogResults(index++));
		        	}
           	 	} catch (NullPointerException e2)
           	 	{
           	 		System.out.println("NULL Exception at finding ***catalog book field end***, ERROR:" + e2.getMessage());
           	 	}
           	 	ctlFields_jComboBox.removeAllItems();
           	 	for (int i=0;i<cb.getCatalog_fields_count();i++)
	     		{
	     			ctlFields_jComboBox.addItem(cb.getCatalogField(i));
	     		}
           	 	
           	 	/**
           	 	 * subject
           	 	 */
           	 	cb.clearCatalogSubjects();
           	 	try
        	 	{
	           	 	while (!cb.getCatalogResults(index).equals("***catalog book subject start***")) 	//finding subject start signal in list
		        	{		        		
		        		index++;
		        		while (cb.getCatalogResults(index) == null)
		        		{
		        			index++;
		        		}
		        	}
        	 	} catch (NullPointerException e3)
        	 	{
        	 		System.out.println("NULL Exception at finding ***catalog book subject start***, ERROR:" + e3.getMessage());
        	 	}
        	 	index++;
        	 	while (!cb.getCatalogResults(index).equals("***catalog book subject end***")) 	//finding subject stop signal in list
	        	{		        	
        	 		cb.addCatalogSubject(cb.getCatalogResults(index++));
	        	}
	     		ctlSubject_jComboBox.removeAllItems();
	     		for (int i=0;i<cb.getCatalog_subjects_count();i++)
	     		{
	     			ctlSubject_jComboBox.addItem(cb.getCatalogSubject(i));
	     		}
			}//if in catalog
			else
			{
				disableCatalog();
			}
		}//if book axists
		else
			System.out.println("ERR, no book selected, cant get info");
	}
	public void requestBookCatalog()
	{
    	obj logdt1 = new obj();
    	logdt1.setType("***get book catalog info***");
    	logdt1.setParam(0,cb.getIsbn()); // set input from text field
        if (connected == 1)
			client.handleMessageFromClientUI(logdt1);
		try
		{
			Thread.sleep(100);
		} catch (InterruptedException ex)
		{
			Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);			
		}
	}
	/**
	 * setting table columns
	 * @return
	 */
	public TableModel getModel(){
		if (model == null) {
		    String[] columnNames = {"ISBN","Title",};    
	        Object[][] data = {{"",""},{"",""}};
	        model = new DefaultTableModel(data,columnNames);
			}// end if
			return model;
		}// end getModel
	/**
	 * This method initializes author_ComboBox	
	 * 	
	 * @return javax.swing.JComboBox	
	 */
	private JComboBox getAuthor_ComboBox() 
	{
		if (author_ComboBox == null) {
			author_ComboBox = new JComboBox();
			author_ComboBox.setBounds(new Rectangle(467, 164, 124, 22));
		}
		return author_ComboBox;
	}
	/**
	 * This method initializes price_jTextField	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getPrice_jTextField() {
		if (price_jTextField == null) {
			price_jTextField = new JTextField();
			price_jTextField.setText("0");
			price_jTextField.setBounds(new Rectangle(863, 540, 77, 21));
			price_jTextField.addFocusListener(new java.awt.event.FocusAdapter() {
				public void focusLost(java.awt.event.FocusEvent e) 
				{
					b.setPrice(price_jTextField.getText());
				}
			});
		}
		return price_jTextField;
	}
	/**
	 * This method initializes add_author_jButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getAdd_author_jButton() {
		if (add_author_jButton == null) {
			add_author_jButton = new JButton();
			add_author_jButton.setBounds(new Rectangle(597, 164, 43, 21));
			add_author_jButton.setToolTipText("Add Author To List");
			add_author_jButton.setText("+");
			add_author_jButton.addActionListener(new java.awt.event.ActionListener() {
				/**
				 * this button adds author to the book
				 * when pressed it pops a dialog box with textfield
				 * the user fills the text and press ok
				 * after that the methos updates DB with new author
				 * if user press cancel nothing happens
				 */
				public void actionPerformed(java.awt.event.ActionEvent evt) 
				{
					String str;
					System.out.println("add author actionPerformed()"); 
					str = JOptionPane.showInputDialog("Enter Author Name");
					if ((str != null) && (!str.isEmpty()))
					{
						if (b.add_author(str))	//if add author success
						{
							author_ComboBox.addItem(str);
							author_ComboBox.setSelectedItem(str);
							Add_authorActionPerformed(evt,str);
						}
						else
						{
							JOptionPane.showMessageDialog(null,"Author Already Exists","NOTHING HAPPENED!",JOptionPane.CLOSED_OPTION);
							System.out.println("Author exists");
						}
					}//if str not empty
				}
			});
		}
		return add_author_jButton;
	}
	private void Add_authorActionPerformed(java.awt.event.ActionEvent evt, String str)
	{
		int i;
		obj bookdt = new obj();
		
		bookdt.setType("***add book author***");
		bookdt.setParam(0, b.getIsbn());
		bookdt.setParam(1, str);
		if (connected == 1)
			client.handleMessageFromClientUI(bookdt);
		try
		{
			Thread.sleep(100);
		} catch (InterruptedException ex)
		{
			Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);			
		}	
	}
	/**
	 * This method initializes remove_author_jButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getRemove_author_jButton() 
	{
		if (remove_author_jButton == null) 
		{
			remove_author_jButton = new JButton();
			remove_author_jButton.setBounds(new Rectangle(645, 164, 43, 22));
			remove_author_jButton.setToolTipText("Remove Selected Author");
			remove_author_jButton.setText("-");
			remove_author_jButton.addActionListener(new java.awt.event.ActionListener() 
			{
				/**
				 * this button removes author to the book
				 * when pressed it pops yes no question
				 * if user press yes it deletes from DB the current selected author
				 * if user press cancel nothing happens
				 */
				public void actionPerformed(java.awt.event.ActionEvent evt) 
				{
					String str;
					System.out.println("Remove author actionPerformed()"); 
					int answer;
					answer = JOptionPane.showConfirmDialog(null, "Really Delete?", "Input",
			                                      				JOptionPane.YES_NO_OPTION);
			            
			        if(answer == JOptionPane.YES_OPTION);
			        {
			        	str = (String) author_ComboBox.getSelectedItem();
			        	author_ComboBox.removeItem(str);
			        	try
			        	{
				        	if (b.remove_author(str))
				        		remove_authorActionPerformed(evt,str);
				        	else
				        	{
				        		JOptionPane.showMessageDialog(null,"Author Wasn't Found","NOTHING HAPPENED!",JOptionPane.CLOSED_OPTION);
								System.out.println("Author not exists");
				        	}
			        	} catch(NullPointerException ex)
			        	{
			        		System.out.println("Null Exception - Remove author MSG: " + ex.getMessage());
			        	}
			        }
				}
			});
		}
		return remove_author_jButton;
	}
	/**
	 * Remove book author request from server
	 * @param evt - event of click
	 * @param str - author to delete
	 */
	private void remove_authorActionPerformed(java.awt.event.ActionEvent evt, String str)
	{
		int i;
		obj bookdt = new obj();
		
		bookdt.setType("***remove book author***");
		bookdt.setParam(0, b.getIsbn());
		bookdt.setParam(1, str);
		if (connected == 1)
			client.handleMessageFromClientUI(bookdt);
		try
		{
			Thread.sleep(100);
		} catch (InterruptedException ex)
		{
			Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);			
		}
	}
	/**
	 * This method initializes add_field_jButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getAdd_field_jButton() {
		if (add_field_jButton == null) {
			add_field_jButton = new JButton();
			add_field_jButton.setBounds(new Rectangle(599, 200, 43, 22));
			add_field_jButton.setToolTipText("Add field To List");
			add_field_jButton.setText("+");
			add_field_jButton.addActionListener(new java.awt.event.ActionListener() {
				/**
				 * this button adds field to the book
				 * when pressed it pops a dialog box with combobox
				 * the combobox includes the complete book field list
				 * the user selects a field and press ok
				 * after that the method updates DB with new field
				 * if user press cancel nothing happens
				 */
				public void actionPerformed(java.awt.event.ActionEvent evt) 
				{
					String str;
					int field_amount=0,i;
					String[] Fields;
					JFrame Dialogueframe = new JFrame("Input Dialog Example 3");
					System.out.println("add field actionPerformed()");
					Fields = getFieldList();
					field_amount = bf.get_no_of_fields();
					
					String[] all_book_fields_list = new String[field_amount];
					for (i=0;i<field_amount;i++)
					{
						all_book_fields_list[i] = Fields[i];
					}
					try
					{
						str = (String) JOptionPane.showInputDialog(Dialogueframe, 
						        "Select Field To Add?",
						        "Add Book Field",
						        JOptionPane.QUESTION_MESSAGE, 
						        null, 
						        all_book_fields_list, 
						        all_book_fields_list[0]);
						if ((str != null) && (!str.isEmpty()))
						{
							if (b.add_book_field(str))
							{
								field_ComboBox1.addItem(str);
								field_ComboBox1.setSelectedItem(str);
								Add_fieldActionPerformed(evt,str);
							}
							else
							{
								JOptionPane.showMessageDialog(null,"Field Already Exists","NOTHING HAPPENED!",JOptionPane.CLOSED_OPTION);
								System.out.println("Author exists");
							}
						}
					} catch (NullPointerException ex)
					{
						System.out.println("Null Exception: " + ex.getMessage());
					}
					

				}
			});
		}
		return add_field_jButton;
	}
	/**
	 * send to server add field request
	 * @param evt - event of button press
	 * @param str - field to add
	 */
	private void Add_fieldActionPerformed(java.awt.event.ActionEvent evt, String str)
	{
		int i;
		obj bookdt = new obj();
		
		bookdt.setType("***add book field***");
		bookdt.setParam(0, b.getIsbn());
		bookdt.setParam(1, str);
		if (connected == 1)
			client.handleMessageFromClientUI(bookdt);
		try
		{
			Thread.sleep(100);
		} catch (InterruptedException ex)
		{
			Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);			
		}	
	}
	/**
	 * This method initializes remove_field_jButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getRemove_field_jButton() {
		if (remove_field_jButton == null) {
			remove_field_jButton = new JButton();
			remove_field_jButton.setBounds(new Rectangle(646, 201, 43, 22));
			remove_field_jButton.setToolTipText("Remove Selected field");
			remove_field_jButton.setText("-");
			remove_field_jButton.addActionListener(new java.awt.event.ActionListener() {
				/**
				 * this button removes field to the book
				 * when pressed it pops yes no question
				 * if user press yes it deletes from DB the current selected field
				 * if user press cancel nothing happens
				 */
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					String str;
					System.out.println("Remove field actionPerformed()"); 
					int answer;
					answer = JOptionPane.showConfirmDialog(null, "Really Delete?", "Input",
			                                      				JOptionPane.YES_NO_OPTION);
			            
			        if(answer == JOptionPane.YES_OPTION);
			        {
			        	str = (String) field_ComboBox1.getSelectedItem();
			        	field_ComboBox1.removeItem(str);
			        	try
			        	{
				        	if (b.remove_field(str))
				        		remove_fieldActionPerformed(evt,str);
				        	else
				        	{
				        		JOptionPane.showMessageDialog(null,"Field Wasn't Found","NOTHING HAPPENED!",JOptionPane.CLOSED_OPTION);
								System.out.println("Field not exists");
				        	}
			        	} catch(NullPointerException ex)
			        	{
			        		System.out.println("Null Exception - Remove Field MSG: " + ex.getMessage());
			        	}
			        }
				}
			});
		}
		return remove_field_jButton;
	}
	/**
	 * remove the selected book field from DB
	 * @param evt
	 * @param str	- Field desc to delete
	 */
	private void remove_fieldActionPerformed(java.awt.event.ActionEvent evt, String str)
	{
		int i;
		obj bookdt = new obj();
		
		bookdt.setType("***remove book field***");
		bookdt.setParam(0, b.getIsbn());
		bookdt.setParam(1, str);
		if (connected == 1)
			client.handleMessageFromClientUI(bookdt);
		try
		{
			Thread.sleep(100);
		} catch (InterruptedException ex)
		{
			Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);			
		}
	}
	/**
	 * Update Current instance of bookField in DB according to ISBN 
	 * @param evt	- Event of Click
	 */
	private void save_bookActionPerformed(java.awt.event.ActionEvent evt)
	{
		if (b.getIsbn() != null)
		{
			int i;
			obj bookdt = new obj();
			
			bookdt.setType("***save book***");
			bookdt.setParam(0, b.getIsbn());
			bookdt.setParam(1, b.getTitle());
			bookdt.setParam(2, b.getLanguage());
			bookdt.setParam(3, b.getSummery());
			bookdt.setParam(4, b.getContents());
			bookdt.setParam(5, b.getPrice());
			
			if (connected == 1)
				client.handleMessageFromClientUI(bookdt);
			try
			{
				Thread.sleep(100);
			} catch (InterruptedException ex)
			{
				Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);			
			}
			Border green_line;
			green_line = BorderFactory.createLineBorder(Color.green);
			save_btn.setBorder(green_line);
		}
		else
		{
			JOptionPane.showMessageDialog(null,"Error: No Book Selected","Error",JOptionPane.CLOSED_OPTION);
		}
	}
	/**
	 * This Method gets file type and file filter
	 * after the file is selected the method returns it's path
	 */
	private File select_file(String files, String type)
	{
		JFileChooser fileopen = new JFileChooser();
		FileFilter filter = new FileNameExtensionFilter(files, type);
	    fileopen.addChoosableFileFilter(filter);
	    int ret = fileopen.showDialog(null, "Open file");
	    
	    if (ret == JFileChooser.APPROVE_OPTION) 
	    {
	      File file = fileopen.getSelectedFile();
	      System.out.println(file);
	      return file;
	    }
	    else return null;
	}
	/**
	 * this method get file type and file and send it to server
	 */
	private void uplaod_file(String type,File f)
	{
		ArrayList filedt = new ArrayList();
        byte[] mybytearray = new byte[(int) f.length()];
        FileInputStream fis;
		try 
		{
			fis = new FileInputStream(f);
			BufferedInputStream bis = new BufferedInputStream(fis);
			try 
	        {
				bis.read(mybytearray,0,mybytearray.length);
			} catch (IOException e1) {
				e1.printStackTrace();
				System.out.println("IOException MSG: " + e1.getMessage());
			}
			
			filedt.add("***Uplaod File***");
			filedt.add(b.getIsbn());
			filedt.add(type);
			filedt.add(f.getName()); //Name of the file
	        
			filedt.add(mybytearray); //byte && SIZE
			filedt.add(mybytearray.length);
	        if (connected == 1)
				client.handleMessageFromClientUI(filedt);
			try
			{
				Thread.sleep(100);
			} catch (InterruptedException ex)
			{
				Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);			
			}
		} catch (FileNotFoundException e1) 
		{
			JOptionPane.showMessageDialog(null,f.getName(),"No File Found",JOptionPane.CLOSED_OPTION);
			e1.printStackTrace();
			System.out.println("Exception No File Found MSG:" + e1.getMessage()); 
		} //Main try
	}

	/**
	 * This method initializes ctlFields_jComboBox	
	 * 	
	 * @return javax.swing.JComboBox	
	 */
	private JComboBox getCtlFields_jComboBox() {
		if (ctlFields_jComboBox == null) {
			ctlFields_jComboBox = new JComboBox();
			ctlFields_jComboBox.setBounds(new Rectangle(270, 575, 122, 23));
		}
		return ctlFields_jComboBox;
	}

	/**
	 * This method initializes addCtlField_jButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getAddCtlField_jButton() {
		if (addCtlField_jButton == null) {
			addCtlField_jButton = new JButton();
			addCtlField_jButton.setBounds(new Rectangle(270, 603, 60, 23));
			addCtlField_jButton.setText("+");
			addCtlField_jButton.addActionListener(new java.awt.event.ActionListener()
			{
				/**
				 * this button adds field to the catalog book
				 * when pressed it pops a dialog box with combobox
				 * the combobox includes the complete catalog field list
				 * the user selects a field and press ok
				 * after that the method updates DB with new field
				 * if user press cancel nothing happens
				 */
				public void actionPerformed(java.awt.event.ActionEvent e) 
				{
					if (cb.getIsbn() != null)
					{
						System.out.println("Add Catalog Field Started: actionPerformed()"); 
						String str;
						int field_amount=0,i;
						String[] Fields;
						JFrame Dialogueframe = new JFrame("Input Book Catalog Fields");
						cl.ClearList();
						try
						{
							Fields = getCatalogFieldList();
							field_amount = cl.get_no_of_fields();
							String[] all_catalog_fields_list = new String[field_amount];
							for (i=0;i<field_amount;i++)
							{
								all_catalog_fields_list[i] = Fields[i];
							}
							try
							{
								str = (String) JOptionPane.showInputDialog(Dialogueframe, 
										"Select Field To Add?",
										"Add Book Field",
										JOptionPane.QUESTION_MESSAGE, 
										null, 
										all_catalog_fields_list, 
										all_catalog_fields_list[0]);
								if ((str != null) && (!str.isEmpty()))
								{
									if (cb.add_catalog_field(str))
									{
										ctlFields_jComboBox.addItem(str);
										ctlFields_jComboBox.setSelectedItem(str);
										Add_CatalogfieldActionPerformed(e,str);
									}
									else
									{
										JOptionPane.showMessageDialog(null,"Field Already Exists In Catalog","NOTHING HAPPENED!",JOptionPane.CLOSED_OPTION);
										System.out.println("Catalog Field exists");
									}
								}
							} catch (NullPointerException ex)
							{
								System.out.println("Null Exception: " + ex.getMessage());
							}
						}
						catch (NullPointerException ex)
						{
							System.out.println("Null Exception: Fields" + ex.getMessage());
						}
					} //if isbn exist
				}
			});
		}
		return addCtlField_jButton;
	}
	/** 
	 * Adding new field of book to catalog 
	 * sending to DB
	 * @param evt - event of click
	 * @param str - catalog field we want to add to book
	 **/
	private void Add_CatalogfieldActionPerformed(java.awt.event.ActionEvent evt, String str)
	{
		int i;
		obj bookdt = new obj();
		
		bookdt.setType("***add book catalog field***");
		bookdt.setParam(0, cb.getIsbn());
		bookdt.setParam(1, str);
		if (connected == 1)
			client.handleMessageFromClientUI(bookdt);
		try
		{
			Thread.sleep(100);
		} catch (InterruptedException ex)
		{
			Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);			
		}	
	}
	/**
	 * requesting from server full catalog field list
	 * @return
	 */
	public String[] getCatalogFieldList()
	{
		obj fielddt = new obj();
		int k=0;
		fielddt.setType("***get catalog field list***");
		if (connected == 1)
			client.handleMessageFromClientUI(fielddt);
		try
		{
			Thread.sleep(100);
		} catch (InterruptedException ex)
		{
			Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);			
		}	
		CatalogList cl = new CatalogList();		
		System.out.println("Got catalog fields");
		return CatalogList.getcatalogResults();
	}
	/**
	 * This method initializes removeCtlField_jButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getRemoveCtlField_jButton() {
		if (removeCtlField_jButton == null) {
			removeCtlField_jButton = new JButton();
			removeCtlField_jButton.setBounds(new Rectangle(337, 602, 55, 25));
			removeCtlField_jButton.setText("-");
			removeCtlField_jButton.addActionListener(new java.awt.event.ActionListener() {
				/**
				 * this button removes catalog field of the book
				 * when pressed it pops yes no question
				 * if user press yes it deletes from DB the current selected field
				 * if user press cancel nothing happens
				 */
				public void actionPerformed(java.awt.event.ActionEvent evt) 
				{
					if (cb.getIsbn() != null)
					{
						String str;
						System.out.println("Add Catalog Field Started: actionPerformed()"); 
						int answer;
						answer = JOptionPane.showConfirmDialog(null, "Really Delete?", "Input",
				                                      				JOptionPane.YES_NO_OPTION);
				            
				        if(answer == JOptionPane.YES_OPTION)
				        {
				        	str = (String) ctlFields_jComboBox.getSelectedItem();
				        	ctlFields_jComboBox.removeItem(str);
				        	try
				        	{
					        	if (cb.removeCatalogField(str))
					        		remove_BookCatalogfieldActionPerformed(evt,str);
					        	else
					        	{
					        		JOptionPane.showMessageDialog(null,"Field Wasn't Found","NOTHING HAPPENED!",JOptionPane.CLOSED_OPTION);
									System.out.println("Field not exists");
					        	}
				        	} catch(NullPointerException ex)
				        	{
				        		System.out.println("Null Exception - Remove Field MSG: " + ex.getMessage());
				        	}
				        }
					}
					else
					{
						JOptionPane.showMessageDialog(null,"No Book Selected","NOTHING HAPPENED!",JOptionPane.CLOSED_OPTION);
					}
				}
			});
		}
		return removeCtlField_jButton;
	}
	/**
	 * this methods deletes selected book field from catalog
	 * @param evt - event
	 * @param str - field to be deleted
	 */
	private void remove_BookCatalogfieldActionPerformed(java.awt.event.ActionEvent evt, String str)
	{
		int i;
		obj bookdt = new obj();
		
		bookdt.setType("***remove catalog book field***");
		bookdt.setParam(0, cb.getIsbn());
		bookdt.setParam(1, str);
		if (connected == 1)
			client.handleMessageFromClientUI(bookdt);
		try
		{
			Thread.sleep(100);
		} catch (InterruptedException ex)
		{
			Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);			
		}
	}
	/**
	 * This method initializes ctlSubject_jComboBox	
	 * 	
	 * @return javax.swing.JComboBox	
	 */
	private JComboBox getCtlSubject_jComboBox() {
		if (ctlSubject_jComboBox == null) {
			ctlSubject_jComboBox = new JComboBox();
			ctlSubject_jComboBox.setBounds(new Rectangle(416, 577, 116, 27));
		}
		return ctlSubject_jComboBox;
	}
	/**
	 * This Methods Shows on screen All The Catalog related controls
	 */
	public void enableCatalog()
	{
		catalog_btn1.setText("Remove From Catalog");
		ctlSubject_jComboBox.removeAllItems();
		ctlFields_jComboBox.removeAllItems();
		ctlSubject_jComboBox.setVisible(true);
		ctlFields_jComboBox.setVisible(true);
		addCtlField_jButton.setVisible(true);
		addCtlSubject_jButton.setVisible(true);
		removeCtlField_jButton.setVisible(true);
		removeCtlSubject_jButton.setVisible(true);
		CtlField_jLabel.setVisible(true);
		CtlSubject_jLabel.setVisible(true);
	}
	/**
	 * This Methods Hides All The Catalog related controls
	 */
	public void disableCatalog()
	{
		catalog_btn1.setText("Add To Catalog");
		ctlSubject_jComboBox.removeAllItems();
		ctlFields_jComboBox.removeAllItems();
		ctlSubject_jComboBox.setVisible(false);
		ctlFields_jComboBox.setVisible(false);
		addCtlField_jButton.setVisible(false);
		addCtlSubject_jButton.setVisible(false);
		removeCtlField_jButton.setVisible(false);
		removeCtlSubject_jButton.setVisible(false);
		CtlField_jLabel.setVisible(false);
		CtlSubject_jLabel.setVisible(false);
	}

	/**
	 * This method initializes addCtlSubject_jButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getAddCtlSubject_jButton() {
		if (addCtlSubject_jButton == null) {
			addCtlSubject_jButton = new JButton();
			addCtlSubject_jButton.setBounds(new Rectangle(417, 607, 52, 22));
			addCtlSubject_jButton.setText("+");
			addCtlSubject_jButton.addActionListener(new java.awt.event.ActionListener() {
				/**
				 * this button adds subject to the book
				 * when pressed it pops a dialog box with combobox
				 * the combobox includes the complete catalog subject list
				 * the user selects a subject and press ok
				 * after that the method updates DB with new subject
				 * if user press cancel nothing happens
				 */
				public void actionPerformed(java.awt.event.ActionEvent e) 
				{
					if (cb.getIsbn() != null)
					{
						System.out.println("Add Catalog Subject Started: actionPerformed()"); 
						String str;
						int subject_amount=0,i;
						String[] Subjects;
						JFrame Dialogueframe = new JFrame("Input Book Catalog Fields");
						cl.ClearList();
						Subjects = getCatalogSubjectList();
						subject_amount = cl.get_no_of_fields();
						String[] all_catalog_subject_list = new String[subject_amount];
						for (i=0;i<subject_amount;i++)
						{
							all_catalog_subject_list[i] = Subjects[i];
						}
						try
						{
							str = (String) JOptionPane.showInputDialog(Dialogueframe, 
							        "Select Subject To Add?",
							        "Add Book Subject",
							        JOptionPane.QUESTION_MESSAGE, 
							        null, 
							        all_catalog_subject_list, 
							        all_catalog_subject_list[0]);
							if ((str != null) && (!str.isEmpty()))
							{
								if (cb.addCatalogSubject(str))
								{
									ctlSubject_jComboBox.addItem(str);
									ctlSubject_jComboBox.setSelectedItem(str);
									Add_CatalogsubjectActionPerformed(e,str);
								}
								else
								{
									JOptionPane.showMessageDialog(null,"Subject Already Exists In Catalog","NOTHING HAPPENED!",JOptionPane.CLOSED_OPTION);
									System.out.println("Catalog Subject exists");
								}
							}
						} catch (NullPointerException ex)
						{
							System.out.println("Null Exception: " + ex.getMessage());
						}
					}
				}
			});
		}
		return addCtlSubject_jButton;
	}
	/** 
	 * Adding new subject of book to catalog 
	 * sending to DB
	 * @param evt - event of click
	 * @param str - catalog subject we want to add to book
	 **/
	private void Add_CatalogsubjectActionPerformed(java.awt.event.ActionEvent evt, String str)
	{
		int i;
		obj bookdt = new obj();
		
		bookdt.setType("***add catalog book subject***");
		bookdt.setParam(0, cb.getIsbn());
		bookdt.setParam(1, str);
		if (connected == 1)
			client.handleMessageFromClientUI(bookdt);
		try
		{
			Thread.sleep(100);
		} catch (InterruptedException ex)
		{
			Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);			
		}	
	}
	/**
	 * this method returns the list of all the subjects in the catalog
	 * @return
	 */
	public String[] getCatalogSubjectList()
	{
		obj fielddt = new obj();
		int k=0;
		fielddt.setType("***get catalog subject list***");
		if (connected == 1)
			client.handleMessageFromClientUI(fielddt);
		try
		{
			Thread.sleep(100);
		} catch (InterruptedException ex)
		{
			Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);			
		}	
		CatalogList cl = new CatalogList();		
		System.out.println("Got catalog subjects");
		return CatalogList.getcatalogResults();
	}
	/**
	 * This method initializes removeCtlSubject_jButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getRemoveCtlSubject_jButton() {
		if (removeCtlSubject_jButton == null) {
			removeCtlSubject_jButton = new JButton();
			removeCtlSubject_jButton.setBounds(new Rectangle(475, 608, 56, 23));
			removeCtlSubject_jButton.setText("-");
			removeCtlSubject_jButton.addActionListener(new java.awt.event.ActionListener() {
				/**
				 * this button removes catalog subject of the book
				 * when pressed it pops yes no question
				 * if user press yes it deletes from DB the current selected subject
				 * if user press cancel nothing happens
				 */
				public void actionPerformed(java.awt.event.ActionEvent e) 
				{
					if (cb.getIsbn() != null)
					{
						String str;
						System.out.println("Add Catalog Subject Started: actionPerformed()"); 
						int answer;
						answer = JOptionPane.showConfirmDialog(null, "Really Delete?", "Input",
				                                      				JOptionPane.YES_NO_OPTION);
				        if(answer == JOptionPane.YES_OPTION)
				        {
				        	str = (String) ctlSubject_jComboBox.getSelectedItem();
				        	ctlSubject_jComboBox.removeItem(str);
				        	try
				        	{
					        	if (cb.removeCatalogSubject(str))
					        		remove_BookCatalogsubjectActionPerformed(e,str);
					        	else
					        	{
					        		JOptionPane.showMessageDialog(null,"Subject Wasn't Found","NOTHING HAPPENED!",JOptionPane.CLOSED_OPTION);
									System.out.println("Subject not exists");
					        	}
				        	} catch(NullPointerException ex)
				        	{
				        		System.out.println("Null Exception - Remove Subject MSG: " + ex.getMessage());
				        	}
				        }
					}
					else
					{
						JOptionPane.showMessageDialog(null,"No Book Selected","NOTHING HAPPENED!",JOptionPane.CLOSED_OPTION);
					}
				}
			});
		}
		return removeCtlSubject_jButton;
	}
	/**
	 * this methods deletes selected book field from catalog
	 * @param evt - event
	 * @param str - field to be deleted
	 */
	private void remove_BookCatalogsubjectActionPerformed(java.awt.event.ActionEvent evt, String str)
	{
		int i;
		obj bookdt = new obj();
		
		bookdt.setType("***remove catalog book subject***");
		bookdt.setParam(0, cb.getIsbn());
		bookdt.setParam(1, str);
		if (connected == 1)
			client.handleMessageFromClientUI(bookdt);
		try
		{
			Thread.sleep(100);
		} catch (InterruptedException ex)
		{
			Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);			
		}
	}
}  //  @jve:decl-index=0:visual-constraint="-53,30"// end Class
