package Gui;

import java.awt.Dimension;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Rectangle;

import javax.swing.ButtonGroup;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Rectangle;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import Gui.*;
import logic.*;
import client.*;
import javax.swing.BorderFactory;
import java.awt.Color;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import common.ChatIF;
import javax.swing.JCheckBox;


/**
 * The class create a JPanel of Search Book
 *  
 * @author VivikRaT
 *
 */
public class SearchBook extends JPanel {
	  private JLabel jLabel666 = null;
	    /**
		 * Instance of JLabel with to Title of the panel (Search Book) 
		 */
	 public javax.swing.JLabel jLabel51;
	    /**
		 *  Instance of JLabel for the Search Form Panel
		 *  sets the defult icon of the program
		 */
	private javax.swing.JLabel jLabel1;
	public javax.swing.JButton jButton1;
    public javax.swing.JButton jButton2;
    public javax.swing.JButton jButton3;
    public javax.swing.JButton jButton4;
    public javax.swing.JButton jButton5;
    public javax.swing.JButton jButton6;
    public javax.swing.JButton jButton7;
    public javax.swing.JButton jButton8;
    /**
     * Instance of JButton for Advance search
     */
    public javax.swing.JButton jButton9;
    /**
     * Instance of JButton for Search Button
     */
    private javax.swing.JButton jButton21;
	/**
	 *  Instance of JLabel with text Title 
	 *  Represents filter the jCheckBox near Enable
	 */
	private JLabel jLabel = null;
	/**
	 *  Instance of JLabel with text Author 
	 *  Represents filter the jCheckBox near Enable
	 */
	private JLabel jLabel2 = null;
	/**
	 *  Instance of JLabel with text Field 
	 *  Represents filter the jCheckBox near Enable
	 */
	private JLabel jLabel3 = null;
	/**
	 *  Instance of JLabel with text Keyword 
	 *  Represents filter the jCheckBox near Enable
	 */
	private JLabel jLabel4 = null;
	/**
	 *  Instance of JLabel with text Subject 
	 *  Represents filter the jCheckBox near Enable
	 */
	private JLabel jLabel5 = null;
	/**
	 *  Instance of JLabel with text Lenguage 
	 *  Represents filter the jCheckBox near Enable
	 */
	private JLabel jLabel7 = null;
	/**
	 *  Instance of JLabel with text All 
	 *  Represents filter the jCheckBox near Enable
	 */	
	private JLabel jLabel8 = null;
	/**
	 * Instance of JLabel 
	 * to show if there are results in the search
	 */
	private JLabel jLabel99 = null;
    /**
    * Instance of JTextField type the text to search
    */
	public JTextField jTextField1 = null;
		/**
	 *  Instance of JCheckBox 
	 *  search by Title filter
	 *  If checked the search will include this filter
	 */
    public JCheckBox jCheckBox = null;
    /**
     *  Instance of JCheckBox 
     *  search by Author filter
     *  If checked, the search will include this filter
     */
    public JCheckBox jCheckBox1 = null;
	/**
     *  Instance of JCheckBox 
     *  search by Field filter
     *  If checked, the search will include this filter
     */
    public JCheckBox jCheckBox2 = null;
	/**
     *  Instance of JCheckBox 
     *  search by Keyword filter
     *  If checked, the search will include this filter
     */
    public JCheckBox jCheckBox3 = null;
	/**
     *  Instance of JCheckBox 
     *  search by Subject filter
     *  If checked, the search will include this filter
     */
    public JCheckBox jCheckBox4 = null;
	/**
     *  Instance of JCheckBox 
     *  search by Language filter
     *  If checked, the search will include this filter
     */
    public JCheckBox jCheckBox5 = null;
	/**
     *  Instance of JCheckBox 
     *  search by All filters
     *  If checked, the search will include All filter
     */
    public JCheckBox jCheckBox6 = null;
	
	/**
	 *  Instance of int instance, 
	 *  to set the nubmer of rows in the result table
	 */
	private int rows=0;
	/**
	 *  Instance of Defult model 
	 *  to set the model of the result table
	 */
	 public javax.swing.table.DefaultTableModel model = null;
	
	/**
	 *  Instance of JTable 
	 *  sets the result table of the search
	 */	
	public javax.swing.JTable table=null;

	/**
	 *  Instance of JScrollPane 
	 *  sets Scroll the results table
	 */	
	private JScrollPane jScrollPane1 = null;
	 
	/**
	 *  Instance of int instance, 
	 *  to save the connection to the server status 
	 *  1 = connected , else not connected
	 */    
    public int connected;
    /**
	 * Instance of ChatClient 
	 * To run the Chat Client functions
	 */   
    public static ChatClient client;  //  @jve:decl-index=0:
    /**
	 * Instance of ChatIF 
	 * To check the connection
	 */   
	private  ChatIF ChatIF = null;
    /**
     * Instance of User Class in logic
     * to save the user details
     */
	User u = new User();  //  @jve:decl-index=0:
	
	
	/**
	 * This is the default constructor
	 */
	public SearchBook() {
		//super();
		initialize();
	}

	/**
	 * This method initializes SearchBook Form
	 */
	public void initialize() {
		
	
	        
		model = null;
		jLabel99 = new JLabel();
		jLabel99.setBounds(new Rectangle(568, 291, 134, 17));
		jLabel99.setText("");
		
		jLabel8 = new JLabel();
		jLabel8.setBounds(new Rectangle(700, 260, 19, 17));
		jLabel8.setText("All");
		jLabel51 = new javax.swing.JLabel();
		jLabel7 = new JLabel();
        jLabel7.setBounds(new Rectangle(700, 230, 71, 17));
        jLabel7.setText("Lenguage");
        jLabel5 = new JLabel();
        jLabel5.setBounds(new Rectangle(600, 230, 67, 17));
        jLabel5.setText("Subject");
        jLabel4 = new JLabel();
        jLabel4.setBounds(new Rectangle(500, 230, 76, 17));
        jLabel4.setText("Keyword");
        jLabel3 = new JLabel();
        jLabel3.setBounds(new Rectangle(700, 200, 65, 17));
        jLabel3.setText("Field");
        jLabel2 = new JLabel();
        jLabel2.setBounds(new Rectangle(600, 200, 54, 17));
        jLabel2.setText("Author");
        jLabel = new JLabel();
        jLabel.setBounds(new Rectangle(500, 200, 38, 22));
        jLabel.setText("Title");
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jButton21 = new javax.swing.JButton();
    
        this.setSize(1000, 700);
        this.setLayout(null);
        
      
        
    	//jButton21 = new JButton();
    	jButton21.setBounds(new Rectangle(475, 260, 199, 23));
    	jButton21.setText("Search");
    	jButton21.setBorder(new SoftBevelBorder(BevelBorder.RAISED));
    	jButton21.addActionListener(new java.awt.event.ActionListener() {
    		public void actionPerformed(java.awt.event.ActionEvent e) {
    			jButton21ActionPerformed();
    		}
    	    });
    	        
        jButton1.setText("Search Book");
        jButton1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 0, 0), 3));
        this.add(jButton1,null);
        jButton1.setBounds(30, 20, 140, 40);
		
        jButton2.setText("Search Review");
        jButton2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton2,null);
        jButton2.setBounds(30, 60, 140, 40);

        jButton3.setText("Manage Books");
        jButton3.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton3,null);
        jButton3.setBounds(30, 100, 140, 40);

        jButton4.setText("Manage Catalog");
        jButton4.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton4,null);
        jButton4.setBounds(30, 140, 140, 40);

        jButton5.setText("Manage Reviews");
        jButton5.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton5,null);
        jButton5.setBounds(30, 180, 140, 40);

        jButton6.setText("Logout");
        jButton6.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton6,null);
        jButton6.setBounds(30, 220, 140, 40);

        jButton7.setText("jButton7");
        jButton7.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton7,null);
        jButton7.setBounds(30, 260, 140, 40);

        jButton8.setText("Logout");
        jButton8.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton8,null);
        jButton8.setBounds(30, 300, 140, 40);	
        

     	
        
        jTextField1 = new JTextField();
    	jTextField1.setBounds(new Rectangle(475, 169, 288, 21));
    	        
        jButton9.setText("Advance Search");
        jButton9.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        this.add(jButton9,null);
        this.add(jLabel99);
        this.add(jLabel, null);
        this.add(jLabel2, null);
        this.add(jLabel3, null);
        this.add(jLabel4, null);
        this.add(jLabel5, null);
        this.add(jLabel7, null);
        this.add(jButton21, null);
        this.add(jTextField1, null);
        this.add(getJCheckBox(), null);
        this.add(getJCheckBox1(), null);
        this.add(getJCheckBox2(), null);
        this.add(getJCheckBox3(), null);
        this.add(getJCheckBox4(), null);
        this.add(getJCheckBox5(), null);
      
        
        jButton9.setBounds(169, 20, 140, 40);	
        
        jLabel51.setFont(new java.awt.Font("Tahoma", 1, 18));
        jLabel51.setText("Search Book");
        this.add(jLabel51);
        this.add(getJCheckBox6(), null);
        this.add(jLabel8, null);
        
        this.add(getJScrollPane1(), null);
        
        
        jLabel51.setBounds(550, 50, 120, 22);

        privinit();
        
        jLabel1 = new javax.swing.JLabel();
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Blue1.jpg"))); // NOI18N
        jLabel1.setText("jLabel1");
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
       
        
   	        
	} // end initialize

	/**
	 * verify the connection with the server and sets the button to view
	 * sets the menu button of the form depend on the user privilege 
	 */
	void privinit(){
    	/* Setup Connection to server */
        
	       connected = 1;
	   	    try  // trying to connect to server 
	   	    {
	   	    	 client= new ChatClient(u.getHost(),5555,ChatIF);
	   	    } // end try
	   	    catch(IOException exception)
	   	    {
	   	    	JOptionPane.showMessageDialog(null,"Error: Can't setup connection!","Error",JOptionPane.CLOSED_OPTION);
	   	    	connected = 0; // failed to connect
	   	    } // end catch
	        
	   	    /* end setup connection */
	   	 /* Reader */
	   	if(u.getPriv() == 0){  
	   		jButton3.setText("Account Settings");
	   		jButton4.setText("Logout");
	   		jButton5.setVisible(false);
	   		jButton6.setVisible(false);
	   		jButton7.setVisible(false);
	   		jButton8.setVisible(false);

	   	}
	   	/* Librarian */
	   	else if(u.getPriv() == 1){
	   		jButton7.setVisible(false);
	   		jButton8.setVisible(false);

	   	
	   	
	   	}
	   	/* Library Manager */
	   	else if(u.getPriv() == 2){
	   		jButton6.setText("Manage Reports");
	   		jButton7.setText("Logout");
	   		jButton8.setVisible(false);

	   	
	   	}
	   	/* System Manager */
	   	else if(u.getPriv() == 3){
	      		jButton6.setText("Manage Reports");
	   		jButton7.setText("Account Management");
	   		jButton8.setText("Logout");

	   	}
	}// end privinit
	/**
	 * Search button action performer
	 * Search the typed text by the chosen filters and show results
	 * When press the search button  :
	 * 1. remove old results from the table and set search result not visible
	 * 2. if there is text in the search text box run the search
	 *   2.1. create Instance of Book to get the search results
	 *   2.2. if there is no result in the search
	 *      2.2.1. write NO Result in the lable
	 *   2.3. if there is results
	 *      2.3.1. set the row number
	 *      2.3.2. write the results in the table
	 *      
	 * @param evt
	 */
	public void jButton21ActionPerformed() {// search button was pressed
		
	        
		    int i;
			int j=0;
			int index=1;
	   
			/*
	         *    remove old results
	         */
			
			jScrollPane1.setVisible(false);
			jLabel99.setVisible(false);
			// when searching without text second time , exaption happened
			//not need to clear table seacond time
	        
			for(i=this.rows-1;i>=0;i--)
	        	model.removeRow(i);
			//to fix the bug, need to set row = 0 after clearing table
			this.rows = 0;
           
			if(!jTextField1.getText().isEmpty()){ // if there is any input in text field
			
			searchBook(); // search reviews
			
			Book b = new Book();
			
			
			if(b.getResults(0).equals("no results")){
				jLabel99.setText("No Results Found:");
				jLabel99.setVisible(true); // show no results found
				this.rows=0; // reset last result
			} // end if
			else{ // display results
		
				Object[] row = new Object[5];
				jLabel99.setText("Results Found:");
				jLabel99.setVisible(true);
	       
		        /*
		         * get number of rows in result table
		         */
		             
		        for(i=1;!b.getResults(i).equals("end1");i++);
		        this.rows=(--i)/5;
		       
		        /*
		         * set results in table
		         */
		        
		        for(i=0;i<rows;i++){//for1     
		        	for(j=0;j<5;j++,index++){//for2
		        		//"<html><font color=red>".concat
		        		String str = jTextField1.getText();
		        		if (b.getResults(index).contains(str))
		        		{
		        			row[j] = "<html>".concat(b.getResults(index).replaceAll(str, "<font color=red>".concat(str).concat("<font color=black>")));
		        		}
		        		else if  (b.getResults(index).contains(str = str.substring(0,1).toUpperCase() + str.substring(1)))
		        		{
		        			row[j] = "<html>".concat(b.getResults(index).replaceAll(str, "<font color=red>".concat(str).concat("<font color=black>")));
		        		}
		        		else
		        		row[j]=(b.getResults(index));	        	      	
		        	}//end for2        	
		            model.addRow(row);            
		        }//end for1
		        jScrollPane1.setVisible(true);
		        jScrollPane1.setBounds(400, 330, 440,17*(rows+1));	// set table size
			}// end else
		 }// end if (!jText1...
			else
			{
				jLabel99.setText("No Text Pressed !!! Please enter text.");
				jLabel99.setVisible(true); // show no results found	
			}
			
	   }//end event Search button
		

		
		
		
		
		
	/**
	 * Search the book in the DB I-Book
	 * send message to the server over the client
	 * sets the massage type Search Book
	 * sets the 0 parameter to be the text to search from the jTextField1
	 * sets 1-7 parameters to be the checked choice of filters (by whom to search)
	 */
	public void searchBook(){ // this function will connect to server to search reviews
		
    	obj logdt1 = new obj();
    	logdt1.setType("Search Book");
    	logdt1.setParam(0,jTextField1.getText()); // set input from text field
    	
    	logdt1.setParam(1,Boolean.toString(jCheckBox.isSelected())   ); // set filter
    	logdt1.setParam(2,Boolean.toString(jCheckBox1.isSelected())   ); // set filter
    	logdt1.setParam(3,Boolean.toString(jCheckBox2.isSelected())   ); // set filter
    	logdt1.setParam(4,Boolean.toString(jCheckBox3.isSelected())   ); // set filter
    	logdt1.setParam(5,Boolean.toString(jCheckBox4.isSelected())   ); // set filter
    	logdt1.setParam(6,Boolean.toString(jCheckBox5.isSelected())   ); // set filter
    	logdt1.setParam(7,Boolean.toString(jCheckBox6.isSelected())   ); // set filter
    	
    	if(connected == 1)	
   	    	client.handleMessageFromClientUI(logdt1);
    	
	    try {
            Thread.sleep(1000); 
            } // end try
	    catch (InterruptedException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } // end catch
    
    	/* end sql  */
		
		
		
		
		
	}// end searchBook

	/**
	 * This method initializes jCheckBox	
	 * if checked set the all filter not checked	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBox() {
		if (jCheckBox == null) {
			jCheckBox = new JCheckBox();
			jCheckBox.setBounds(new Rectangle(480, 200, 21, 21));
			jCheckBox.setOpaque(false);
			jCheckBox.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
                  if (jCheckBox.isSelected()) jCheckBox6.setSelected(false);
				}
			});
		}
		return jCheckBox;
	}

	/**
	 * This method initializes jCheckBox1	
	 * if checked set the "all filter" not checked		
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBox1() {
		if (jCheckBox1 == null) {
			jCheckBox1 = new JCheckBox();
			jCheckBox1.setBounds(new Rectangle(580, 200, 21, 21));
			jCheckBox1.setOpaque(false);
			jCheckBox1.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					if (jCheckBox1.isSelected()) jCheckBox6.setSelected(false);				}
			});
		}
		return jCheckBox1;
	}

	/**
	 * This method initializes jCheckBox2	
	 * 	if checked set the "all filter" not checked	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBox2() {
		if (jCheckBox2 == null) {
			jCheckBox2 = new JCheckBox();
			jCheckBox2.setBounds(new Rectangle(680, 200, 21, 21));
			jCheckBox2.setOpaque(false);
			jCheckBox2.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					if (jCheckBox2.isSelected()) jCheckBox6.setSelected(false);				}
			});
		}
		return jCheckBox2;
	}

	/**
	 * This method initializes jCheckBox3	
	 * 	if checked set the "all filter" not checked	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBox3() {
		if (jCheckBox3 == null) {
			jCheckBox3 = new JCheckBox();
			jCheckBox3.setBounds(new Rectangle(480, 230, 21, 21));
			jCheckBox3.setOpaque(false);
			jCheckBox3.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					if (jCheckBox3.isSelected()) jCheckBox6.setSelected(false);				}
			});
		}
		return jCheckBox3;
	}

	/**
	 * This method initializes jCheckBox4	
	 * 	if checked set the all "filter not" checked	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBox4() {
		if (jCheckBox4 == null) {
			jCheckBox4 = new JCheckBox();
			jCheckBox4.setBounds(new Rectangle(580, 230, 21, 21));
			jCheckBox4.setOpaque(false);
			jCheckBox4.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					if (jCheckBox4.isSelected()) jCheckBox6.setSelected(false);				}
			});
		}
		return jCheckBox4;
	}

	/**
	 * This method initializes jCheckBox5	
	 * 	if checked set the all "filter not" checked	
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBox5() {
		if (jCheckBox5 == null) {
			jCheckBox5 = new JCheckBox();
			jCheckBox5.setBounds(new Rectangle(680, 230, 21, 21));
			jCheckBox5.setOpaque(false);
			jCheckBox5.addChangeListener(new javax.swing.event.ChangeListener() {
				public void stateChanged(javax.swing.event.ChangeEvent e) {
					if (jCheckBox5.isSelected()) jCheckBox6.setSelected(false);				}
			});
		}
		return jCheckBox5;
	}

	/**
	 * This method initializes jCheckBox6	
	 * 	if checked set the all other jCheckBox not checked(Only all)
	 * @return javax.swing.JCheckBox	
	 */
	private JCheckBox getJCheckBox6() {
		if (jCheckBox6 == null) {
			jCheckBox6 = new JCheckBox();
			jCheckBox6.setBounds(new Rectangle(680, 260, 21, 21));
			jCheckBox6.setOpaque(false);
			jCheckBox6.setSelected(true);
			jCheckBox6.addItemListener(new java.awt.event.ItemListener() {
				public void itemStateChanged(java.awt.event.ItemEvent e) {
					if (jCheckBox6.isSelected())
					{
						jCheckBox.setSelected(false);
						jCheckBox1.setSelected(false);
						jCheckBox2.setSelected(false);
						jCheckBox3.setSelected(false);
						jCheckBox4.setSelected(false);
						jCheckBox5.setSelected(false);
						
					}
					if (!jCheckBox.isSelected() && !jCheckBox1.isSelected()&& !jCheckBox2.isSelected()&& !jCheckBox3.isSelected()&& !jCheckBox4.isSelected()&& !jCheckBox5.isSelected())
					{
						jCheckBox6.setSelected(true);
					}
				}
			});
		}
		return jCheckBox6;
	}

	/**
	 * This method initializes jScrollPane1	
	 * 	set the table to be the getJTable()
	 * @return javax.swing.JScrollPane	
	 */
	private JScrollPane getJScrollPane1() {
		if (jScrollPane1 == null) {
			jScrollPane1 = new JScrollPane();
			jScrollPane1.setBounds(new Rectangle(360, 400, 453, 420));
			jScrollPane1.setViewportView(getJTable());
			jScrollPane1.setVisible(false);
		}
		return jScrollPane1;
	}

	/**
	 * This method initializes jTable1	
	 * 	sets new table with getModel()
	 * sets the name of the column to "ISBN","Title","Language","Rating","Price"
	 * @return javax.swing.JTable	
	 */
	public JTable getJTable() {
		if (table == null) {
	        table = new JTable(getModel());	
	        table.setPreferredScrollableViewportSize(new Dimension(300, 200));
	        table.setFillsViewportHeight(true);
	        model.removeRow(1);
	        model.removeRow(0);
	        
	        /*
	         *     table alignment 
	         */
	        
	        DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
	        dtcr.setHorizontalAlignment(SwingConstants.CENTER);
	        table.getColumn("ISBN").setCellRenderer(dtcr);
	        table.getColumn("Title").setCellRenderer(dtcr);
	        table.getColumn("Language").setCellRenderer(dtcr);
	        table.getColumn("Rating").setCellRenderer(dtcr);
	        table.getColumn("Price").setCellRenderer(dtcr);	        
	        
	        
		}// end if
		return table;
	}// end getTable

	/**
	 *  This method initializes model
	 *  set the column names to be "ISBN","Title","Language","Rating","Price"
	 * return javax.swing.table.DefaultTableModel
	 */
	public TableModel getModel(){
	if (model == null) {
	    String[] columnNames = {"ISBN","Title","Language","Rating","Price"};    
        Object[][] data = {{"","","","",""},{"","","","",""}};
        model = new DefaultTableModel(data,columnNames);
		}// end if
		return model;
	}// end getModel
	
}  //  @jve:decl-index=0:visual-constraint="146,78"// end Class
