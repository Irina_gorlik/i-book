package logic;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import Gui.*;
import logic.*;
/**
  *  Create an instance of Histogram, add a Gaussian generated
  *  distribution of data to it and display it.
 **/
public class DrawHistApplet extends JApplet
{
  public Histogram fHistogram = null;
  public static String[] str = new String[13];
  public static int first=1;
  
  public DrawHistApplet() {
//	  for(int i=0;i<13;i++)
//		  str[i]="0";
	  
	 if(first==0)
		 init();
  }

  public DrawHistApplet(String[] months) {
	  this.str = months;
	  this.first=0;
	  //	  str[0]="5";
	  init();
  }
  
//  
//  public void addInfo(String[] months) {
//	  this.str = months;
//	  str[0]="5";
//	  initTmp(months);
//  }
  
  public void init ()  {

    Container content_pane = getContentPane ();

    // Create a histogram with Gaussian distribution.
    makeHist ();

    // Create an instance of a JPanel sub-class
    HistPanel hist_panel = new HistPanel (fHistogram);

    // And add one or more panels to the JApplet panel.
    content_pane.add (hist_panel);

  } // init

  
  public void loadData(String[] data){
	  
	  this.str=data;
	  this.first=0;
	  
  }//end loadData()
  
  /**
    *  Create the histogram and a set of data drawn
    *  from a Gaussian random number generator.
   **/
  void makeHist () {
    // Create an instance of the Random class for
    // producing our random values.
	  
	  java.util.Random r = new java.util.Random ();

    // Them method nextGaussian in the class Random produces
    // a value centered at 0.0 and a standard deviation
    // of 1.0.

    // Create an instance of our basic gHistogram class.
    // Make it wide enough enough to include most of the
    // gaussian values.
    fHistogram = new Histogram ("", "Months",12,0,12);
    System.out.println("DrawHist str.length: "+str.length);
    System.out.println("DrawHist First: "+first);
    String s= new String();
    
    
//    for( int i=0;i<13;i++){
//    s[i]="1";
//    }	
  
    System.out.println("First s:"+first);

//    fHistogram.add ((double)4);
//	fHistogram.add ((double)5);
//  	fHistogram.add ((double)8);
//  	fHistogram.add ((double)9);
// 
//    if(first==0)
//    	fHistogram.add ((double)2);
//    if(first==1)
//    	fHistogram.add ((double)9);
    
    
    if(first==0){
    for( int i=0;i<str.length-1;i++)
    	System.out.println("DrawHist: str:"+str[i]);
    
    for( int i=0;i<str.length;i++){
//    	fHistogram.add ((double)i);

    	if(!str[i].equals("0")){

    		for(int j=0;j<((Integer.parseInt(str[i]))/i);j++){
//    			fHistogram.add ((double)8);
    			s=(String)str[i];
//    			fHistogram.add ((double)(Integer.parseInt(s)));
    			fHistogram.add ((double)i);
    	System.out.println("DrawHist: s:"+s);
    		}// end for
    	}// end if
  }
    
    }// end first
    
    
    
    
//    fHistogram.add ((double)Integer.parseInt(s[2]));
//    fHistogram.add ((double)Integer.parseInt(s[3]));
//    fHistogram.add ((double)Integer.parseInt(s[4]));
//    fHistogram.add ((double)Integer.parseInt(s[5]));
//    fHistogram.add ((double)Integer.parseInt(s[6]));
//    fHistogram.add ((double)Integer.parseInt(s[7]));
//    fHistogram.add ((double)Integer.parseInt(s[8]));
//    fHistogram.add ((double)Integer.parseInt(s[9]));
//    fHistogram.add ((double)Integer.parseInt(s[10]));
//    fHistogram.add ((double)Integer.parseInt(s[11]));
//    fHistogram.add ((double)Integer.parseInt(s[12]));
    // Fill histogram with Gaussian distribution
//    for (int i=0; i<10; i++) {
//         double val = r.nextGaussian ();
//         fHistogram.add (val);
//    }
  } // makeHist
 


} // class DrawHistApplet 
