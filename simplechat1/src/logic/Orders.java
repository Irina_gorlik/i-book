
package logic;

public class Orders
{
	private int max=200;
	private static String[] users; // all users
	private static String[] orders; // user orders
	private static String[] books; // all books
	private static String[] counter; // for all orders
	private static String isbn; // current isbn viewed
	
	
    public Orders(){	

    } 
    
    public Orders(String[] u){	// to get all users
    	
    	this.users =(String[]) u;
    	
    } 
    public Orders(String[] o,int num){	// to get all books
    	
    	this.books =(String[]) o;
    	
    } 
    public Orders(String[] o,int num1,int num2){	// to get all books
    	
    	this.counter =(String[]) o;
    	
    } 
    public Orders(String isbn,int num1,int num2,int num3){
    
    	this.isbn = isbn;
    }
    
    

    /* getters */
    
	public String getUser(int index) {
		return users[index];
		}
	
	public String[] getUsers() {
		return users;
		}
	
	public String[] getBooks() {
		return books;
		}
	
	public String getOrder(int index) {
		return orders[index];
		}
    
	public String[] getOrders() {
		return orders;
		}
	public String[] getCounter() {
		return counter;
		}
	public String getCount(int index) {
		return counter[index];
		}
	public String getIsbn() {
		return isbn;
		}
	
    /* setter */

	public void setUser(int index, String user) {
		users[index] = user;
		}
	
	public void setUsers(String[] u) {
		users =(String[]) u;
		}
	
	public void setOrders(String[] o) {
		this.orders = o;
		}
	
	
	
} // end class Review