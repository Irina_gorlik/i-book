
package logic;

import java.io.Serializable;


public class obj implements Serializable
{
	private static final long serialVersionUID = 2367322619647822268L;
	
	private int index=20;
	private String Type; // type of object
	private String[] Param = new String[index];


	
	public obj(){
		
		this.Type=null;
		int i;
		for(i=0;i<index;i++){
			Param[i] = null;

		}
		
	
	}
   
    
	public String getType() {  // return type of object
		return Type;
		}
	
	public String getParam(int index) { // return one parameter
	    return Param[index];
		}
	
  public void setType(String type){ // set type of object
	  this.Type = type; 
  }
  
	public void setParam(int index, String param){
		this.Param[index] = param;
	}
	
} // end class obj