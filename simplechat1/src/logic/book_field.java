/**
 * 
 */
package logic;

/**
 * @author Ilan
 *	this class holds Book fields 
 *	when we want to add field
 *	this class will hold the entire field list
 */
public class book_field 
{
	static private String[] bookField;
	static private int index;
	
	public book_field()
	{
	} 
	public book_field(String[] str,int i)
	{
		this.bookField = str;
		this.index = i;
	}

	public static String[] getBookField() 
	{
		return bookField;
	}
	
	public static String getBookFieldbyIndex(int i) 
	{
		return bookField[i];
	}

	public static void addBookField(String str) 
	{
		int i=0;
		if (bookField == null)
		{
			bookField = new String[200];
			index = 0;
		}
		if (index < 1000)
		{
			for (i=0;i<index;i++)
			{
				if (bookField[i].equals(str))
						return; 				
			}
			bookField[index++] = str;
		}
	}

	public static int get_no_of_fields() 
	{
		/*index = 0;
		while (Fields[field_amount] != null)
		{
			field_amount++;		//finding amount of fields in list
		}*/
		return index;
	}

	public static void setIndex(int index) 
	{
		index = index;
	}
	
}
