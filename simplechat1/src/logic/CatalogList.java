package logic;
/**
 * 
 * @author Ilan
 *	this class holds catalog fields OR catalog subject list
 *	when we want to add catalog field or subject
 *	this class will hold the entire subject/field list
 *
 */
public class CatalogList 
{
	static public String[] catalog_field;
	static public int index=0;
	
	public CatalogList()
	{	}
	
	/**
	 * Setters Getters
	 * @param catalog_fields - list of fields
	 */
	public static String[] getcatalogResults() 
	{
		return catalog_field;
	}
	/**
	 * return specific field from the list
	 * @param i - the index of the element we want to get
	 * @return
	 */
	public static String getCatalogFieldbyIndex(int i) 
	{
		return catalog_field[i];
	}
	/**
	 * add element to the list
	 * if the list isn't full
	 * we add the element to the end of the list
	 * @param str - element we want to add
	 */
	public static void addCataloglist(String str) 
	{
		int i=0;
		if (catalog_field == null)
		{
			catalog_field = new String[500];
			index = 0;
		}
		if (index < 500)
		{
			for (i=0;i<index;i++)
			{
				if (catalog_field[i].equals(str))
						return; 				
			}
			catalog_field[index++] = str;
		}
	}
	/**
	 * return number of elements in list
	 * @return
	 */
	public static int get_no_of_fields() 
	{
		return index;
	}
	/**
	 * set manually number of elements
	 * @param index
	 */
	public static void ClearList() 
	{
		index = 0;
	}
}
