package logic;






public class Errors{

	
	 private int ErrconfirmReview=0; // error on confirm review
	 private int ErrZeroReviews=0; // no more reviews to get
	
	
	
	public Errors(){
		
	}
	
    /* getters */
    
	public int getConfirmErr() {
		return ErrconfirmReview;
		}
	public int getZeroReviews(){
		return ErrZeroReviews;
	}
	
    /* setter */
    
    public void setConfirmErr(int Err1){
    	this.ErrconfirmReview=Err1;
	
    }
    public void setZeroReviews(int Err2){
    	this.ErrZeroReviews=Err2;
    }
	
	
	
}// end class Errors