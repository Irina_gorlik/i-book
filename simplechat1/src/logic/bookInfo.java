/**
 * 
 */
package logic;

import com.mysql.jdbc.Field;

/**
 * @author ilan
 *
 */
public class bookInfo {

	/**
	 * class for book information
	 */
	private int index=100;
	static private String isbn;
	static private String title;
	static private String language;
	static private String summery;
	static private String contents;
	static private String rating;
	static private String viewed;
	static private String purchased;
	static private String book_art;
	static private String pdf;
	static private String doc;
	static private String fb2;
	static private String price ="0";
	static private String[] author = new String[100];;
	static private int author_count=0;
	static private String[] book_fields = new String[100];
	static private int book_fields_count=0;
	static private String[] results;
	
	public bookInfo() 
	{
		// TODO Auto-generated constructor stub
	}
	/**
	 * Constructor 
	 * @param isbn - book number
	 * @param title - book title 
	 * @param lang - book language 
	 * @param summery - book sumemry
	 * @param contents - book contents
	 * @param rating - book rating among customers
	 * @param viewed - # of times a book was searched
	 * @param purchased - # of times book was bought
	 * @param pic - path to book front art
	 * @param pdf - path to book version in pdf
	 * @param doc - path to book version in doc
	 * @param fb2 - path to book version in fb2 
	 * @param price - book's price
	 */
	public bookInfo(String isbn,String title, String lang, String summery, String contents, 
					String rating, String viewed, String purchased, String pic, String pdf, String doc, String fb2, String price, String[] author, String[] book_fields)
	{
		this.isbn = isbn;
		this.title = title;
		this.language = lang;
		this.summery = summery;
		this.contents = contents;
		this.rating = rating;
		this.viewed = viewed;
		this.purchased = purchased;
		this.book_art = pic;
		this.pdf = pdf;
		this.doc = doc;
		this.fb2 = fb2;
		this.price = price;
		this.author = author;
		this.book_fields = book_fields;
	}
	/**
	 * @return the isbn
	 */
	public static String getIsbn() {
		return isbn;
	}
	/**
	 * @param isbn the isbn to set
	 */
	public static void setIsbn(String isbn) {
		bookInfo.isbn = isbn;
	}
	/**
	 * @return the title
	 */
	public static String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public static void setTitle(String title) {
		bookInfo.title = title;
	}
	/**
	 * @return the language
	 */
	public static String getLanguage() {
		return language;
	}
	/**
	 * @param language the language to set
	 */
	public static void setLanguage(String language) {
		bookInfo.language = language;
	}
	/**
	 * @return the summery
	 */
	public static String getSummery() {
		return summery;
	}
	/**
	 * @param summery the summery to set
	 */
	public static void setSummery(String summery) {
		bookInfo.summery = summery;
	}
	/**
	 * @return the contents
	 */
	public static String getContents() {
		return contents;
	}
	/**
	 * @param contents the contents to set
	 */
	public static void setContents(String contents) {
		bookInfo.contents = contents;
	}
	/**
	 * @return the rating
	 */
	public static String getRating() {
		return rating;
	}
	/**
	 * @param rating the rating to set
	 */
	public static void setRating(String rating) {
		bookInfo.rating = rating;
	}
	/**
	 * @return the viewed
	 */
	public static String getViewed() {
		return viewed;
	}
	/**
	 * @param viewed the viewed to set
	 */
	public static void setViewed(String viewed) {
		bookInfo.viewed = viewed;
	}
	/**
	 * @return the purchased
	 */
	public static String getPurchased() {
		return purchased;
	}
	/**
	 * @param purchased the purchased to set
	 */
	public static void setPurchased(String purchased) {
		bookInfo.purchased = purchased;
	}
	/**
	 * @return the book_art
	 */
	public static String getBook_art() {
		return book_art;
	}
	/**
	 * @param book_art the book_art to set
	 */
	public static void setBook_art(String book_art) {
		bookInfo.book_art = book_art;
	}
	/**
	 * @return the pdf
	 */
	public static String getPdf() {
		return pdf;
	}
	/**
	 * @param pdf the pdf to set
	 */
	public static void setPdf(String pdf) {
		bookInfo.pdf = pdf;
	}
	/**
	 * @return the doc
	 */
	public static String getDoc() {
		return doc;
	}
	/**
	 * @param doc the doc to set
	 */
	public static void setDoc(String doc) {
		bookInfo.doc = doc;
	}
	/**
	 * @return the fb2
	 */
	public static String getFb2() {
		return fb2;
	}
	/**
	 * @param fb2 the fb2 to set
	 */
	public static void setFb2(String fb2) {
		bookInfo.fb2 = fb2;
	}
	/**
	 * @return the price
	 */
	public static String getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public static void setPrice(String price) {
		bookInfo.price = price;
	}
	public static void setAuthors(String[] authors) {
		bookInfo.author = authors;
		while (author[author_count++] != null);	//setting number of authors
	}
	public static String[] getAuthors() {
		return author;
	}
	public static String getAuthor(int i) 
	{
		return author[i];
	}
	public static int getAuthorCount() {
		return author_count;
	}
	public static int getFieldCount() {
		return book_fields_count;
	}
	public static boolean add_author(String str)
	{
		/**
		 * exists: 0 - not in list
		 * 		   1 - found in list
		 */
		int i=0,exists=0;		
		while (i<author_count)
		{		
			if (author[i].equals(str))
			{
				exists = 1;
				System.out.println("author already exists");
				i = author_count;
			}
			i++;	
		}
		if (exists == 0)
		{
			author[author_count++] = str;
			return true;
		}
		else return false;
	}
	public static boolean remove_author(String str)
	{
		int i=0,exists = 0,j;
		while (i<author_count)
		{
			if (author[i].equals(str))
			{
				exists = 1;
				System.out.println("author found");
				j=i+1;
				while (j < author_count)
				{
					author[i++] = author[j++];
				}
				author_count--;
				return true;
			}
			i++;
		}
		return false;	//not found
	}
	
	public static void clearauthors() 
	{
		author_count = 0;
	}
	public static void setFields(String[] book_fields) {
		bookInfo.book_fields = book_fields;
		while (book_fields[book_fields_count++] != null);	//setting number of fields
	}
	public static String[] getFields() 
	{
		return book_fields;
	}
	public static void clearFields() 
	{
		book_fields_count = 0;
	}
	public static String getfield(int i) {
		return book_fields[i];
	}
	public static boolean add_book_field(String str)
	{
		/**
		 * exists: 0 - not in list
		 * 		   1 - found in list
		 */
		int i=0,exists=0;		
		while (i<book_fields_count)
		{		
			if (book_fields[i].equals(str))
			{
				exists = 1;
				System.out.println("Field already exists");
				i = book_fields_count;
			}
			i++;	
		}
		if (exists == 0)
		{
			book_fields[book_fields_count++] = str;
			return true;
		}
		else return false;
	}
	public static boolean remove_field(String str)
	{
		int i=0,exists = 0,j;
		while (i<book_fields_count)
		{
			if (book_fields[i].equals(str))
			{
				exists = 1;
				System.out.println("field found");
				j=i+1;
				while (j < book_fields_count)
				{
					book_fields[i++] = book_fields[j++];
				}
				book_fields_count--;
				return true;
			}
			i++;
		}
		return false;	//not found
	}
	public static String[] getResults() 
	{
		return results;
	}
	public static String getResults(int i) 
	{
		return results[i];
	}
	public static void setResults(String[] Results) 
	{
		bookInfo.results = Results;
	}
	public static void setResults(String Results,int index) 
	{
		bookInfo.results[index] = Results;
	}
}
