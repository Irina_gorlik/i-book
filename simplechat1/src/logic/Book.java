package logic;

public class Book
{
	
	static private String title; // book title
	static private String lang;
	static private String isbn; // isbn
	static private float price;
	static private String summery;
	static private String contents;
	static private int rating;
	static private int  viewed;
	static private int  purchaced;
	static private String  Formfrom;
	
	
	static private String[] author; // book title
	static private int authorNum; // book title
	
	static private String[] field; // book title
	static private int fieldNum; // book title	
	

	static private String[] Results ;  // results from search 
	
	public Book(){		
	}
	
	public Book(String one, String two, String three){
	
		this.title= one;
		this.isbn = two;
		this.price=Float.parseFloat(three);
	}
    public Book(String[] ShowNew)
    	{	
    	int index = 10;
        Book.isbn = ShowNew[1];
        Book.title= ShowNew[2];
        Book.lang = ShowNew[3];
        Book.summery = ShowNew[4];
        Book.contents = ShowNew[5]; 
        Book.rating = Integer.parseInt(ShowNew[6]);
        Book.viewed = Integer.parseInt(ShowNew[7]);
        Book.rating = Integer.parseInt(ShowNew[8]);
        Book.price = Float.parseFloat(ShowNew[9]);
        
        Book.author = new String[10];
        int i=0;
        while (ShowNew[index]!="endBookAutorTable")
        {
         	Book.author[i] = ShowNew[index];
         	index++;
         	i++;
        }
        Book.authorNum = i + 1;
        i=0;
        index++;
        Book.field = new String[20];
        while (ShowNew[index]!="endBookFieldTable")
        {
         	Book.field[i] = ShowNew[index];
         	index++;
         	i++;
        }
        Book.fieldNum = i+1;
    	} 
    
    /* getters */
    public String getContents() {
		return Book.contents;
		}
    public String getSummery() {
		return Book.summery;
		}
    public int getViewed() {
		return Book.viewed;
		}
    public int getRating() {
		return Book.rating;
		} 
    public int getPurchaced() {
		return Book.purchaced;
		}
    
    
    public float getPrice() {
		return Book.price;
		}
	public String getIsbn() {
		return Book.isbn;
		}
	public String getLang() {
		return Book.lang;
		}
	public String[] getAuthor() {
	    return Book.author;
		}

    public String getTitle() {
		return Book.title;
	}
    public String[] getField(){
    	return Book.field;
    }
  
    public String getResults(int index){
    	return Results[index];
    }
    
    public String getFormFrom(String where) {
		return Book.Formfrom;
	}
    /* setters */
    
    public void setFormFrom(String where) {
		Book.Formfrom = where;
	}
    public void setPrice(float Price) {
		Book.price = Price;
		}
	public void setIsbn(String Isdn) {
		Book.isbn = Isdn;
		}
	public void setLang(String Lang) {
		Book.lang = Lang;
		}
	public void setAuthor(String[] Author) {
	    Book.author = Author;
		}

    public void setTitle(String Title) {
		Book.title = Title;
	}
    public void setField(String[] Field){
    	Book.field = Field;
    }
    
    public void setResults(String[] res){
    	Book.Results=res;
	}
    
    public void setResults(String res, int index){ // on no result case
    	Book.Results[index]=res;
	}
    public void noResults(){ // on no result case
    	Book.Results = new String[1];
    	Book.Results[0]=new String("no results");
	}
} // end class Review
