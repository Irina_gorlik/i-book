package logic;


public class Accounts {
	
	static private boolean hasTheBook = false;
	static private boolean hasAccounts = false;
	static private int count = 0;

	static private String  Type ;
	static private String  Detail ;
	static private String  BookLeft ;
	
	public Accounts(){		
	}
	public Accounts(String[] ShowNew){	
	        if (ShowNew[1].equals("end1"))
	        {
	        	    Accounts.hasTheBook = false;
	        	    Accounts.hasAccounts = false;
		    } 
	        else if (ShowNew[1].equals("end2")){
	        	   Accounts.hasTheBook = true;
	               Accounts.hasAccounts = true;
	               }
	        else
	              {
	        	Accounts.Type = ShowNew[1];
				Accounts.Detail = ShowNew[2];
				Accounts.BookLeft = ShowNew[3];
				Accounts.hasAccounts = true;
				Accounts.hasTheBook = false;
	            	
	              }
	        
	}
		
	
	public int  getCounter()
	{
		return Accounts.count;
	}

	public String  getType()
	{
		return Accounts.Type;
	}
	public String  getDetail()
	{
		return Accounts.Detail;
	}
	public String  getBookLeft()
	{
		return Accounts.BookLeft;
	}
	
	public boolean  IshasAccounts()
	{
		return Accounts.hasAccounts;
	}
	
	public boolean  IshasTheBook ()
	{
		return Accounts.hasTheBook ;
	}
	
}
