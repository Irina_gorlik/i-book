
package logic;

public class Review
{
	private int index=100; // number of maximum search reviews
	static private String isbn; // isbn
	static private String username; // username
	static private String review; // review
	static private String date; // date of review
	static private int left=0; // reviews left to confirm
	static private String title; // book title
    static private int rating; // rating of user
	static private String[] Results;  // results from search reviews
	
	
	
	public Review(){
		
	}
	
    public Review(String isbn, String user, String review, String date, int count, String title,int rate)
    	{	
        this.isbn = isbn;
        this.username = user; 
        this.review=review;
        this.date=date;
        this.left = count;
        this.title=title;
        this.rating=rate;
    	} 
    
    /* getters */
    
	public String getIsbn() {
		return isbn;
		}
	
	public String getUsername() {
	    return username;
		}

    public String getReview() {
		return review;
	}
    public String getDate(){
    	return date;
    }
    public int getLeft(){
    	return left;
    }
    
    public String getTitle(){
    	return title;
    }
    
    public int getRating(){
    	return rating;
    }
    
    public String getResults(int index){
    	return Results[index];
    }
    
    
    /* setter */
    
    public void setLeft(int count){
    	this.left=count;
    }
    
    public void setResults(String[] res){
    	this.Results=res;
	}
    
    public void setResults(String res, int index){ // on no result case
    	this.Results[index]=res;
	}
    public void setUsername(String u){
    	this.username = u;
    }
    public void setDate(String d){
    	this.date = d;
    }
    public void setTitle(String t){
    	this.title = t;
    }
    public void setIsbn(String t){
    	this.title = t;
    }
    public void setRating(String s){
    	this.rating = Integer.parseInt(s);
    }
    public void setReview(String re){
    	this.review = re;
    }

} // end class Review