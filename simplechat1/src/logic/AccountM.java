package logic;

public class AccountM {

	static private String[] Results ;  // results from search 
    
	static private String act = null;

	
	public AccountM()
	{
		
	}
	
	public void setResults(String[] res){
		AccountM.Results=res;
	}
	
	public void setAct(String res){
		AccountM.act=res;
	}
    
    public void setResults(String res, int index){ // on no result case
    	AccountM.Results[index]=res;
	}
    public void noResults(){ // on no result case
    	AccountM.Results = new String[1];
    	AccountM.Results[0]=new String("no results");
	}
    
    public String getResults(int index){
    	return AccountM.Results[index];
    }
    public String getAct(){
		return AccountM.act;
	}
}
