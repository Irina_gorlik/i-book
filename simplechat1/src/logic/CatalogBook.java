/**
 * 
 */
package logic;

/**
 * @author ilan
 *	This Class Holds book catalog information
 *	if incatalog is true then book in catalog else not in catalog
 *	all field list
 *	all subject list
 */
public class CatalogBook 
{
	static private String Catalogisbn;
	static public boolean inCatalog;
	static public String[] catalogResults;
	static public String[] catalog_fields = new String[100];
	static public int catalog_fields_count=0;
	static public String[] catalog_subjects = new String[100];
	static public int catalog_subjects_count=0;
	
	/**
	 * constructor
	 */
	public CatalogBook()
	{
	}
	/**
	 * @return the isbn
	 */
	public static String getIsbn() {
		return Catalogisbn;
	}
	/**
	 * @param isbn the isbn to set
	 */
	public static void setIsbn(String isbn) 
	{
		CatalogBook.Catalogisbn = isbn;
	}
	/**
	 * 
	 * @param inCatalog - set the book to be in or out the catalog
	 */
	public static void setInCatalog(boolean inCatalog) {
		CatalogBook.inCatalog = inCatalog;
	}
	/**
	 * returning in catalog flag
	 * @return inCatalog
	 */
	public static boolean isInCatalog() {
		return inCatalog;
	}
	
	/**
	 * ***********************Results*************************************
	 */
	
	/**
	 * set element in results at specific location
	 */
	public static void setCatalogResults(String str,int i) 
	{
		if (catalogResults == null)
		{
			catalogResults = new String[300];
		}
		CatalogBook.catalogResults[i] = str;
	}
	/**
	 * set elemnt from results by position
	 * @param index - position
	 * @return String
	 */
	public static String getCatalogResults(int index) 
	{
		return CatalogBook.catalogResults[index];
	}
	
	public static void setCatalogResults(String[] catalogResults) 
	{
		CatalogBook.catalogResults = catalogResults;
	}
	public static String[] getCatalogResults() 
	{
		return catalogResults;
	}
	
	/**
	 * ********************** Field ***********************************
	 */
	/**
	 * set the book's fields in string[]
	 * @param catalog_fields
	 */
	public static void setCatalog_fields(String[] catalog_fields) 
	{
		CatalogBook.catalog_fields = catalog_fields;
	}
	/**
	 * get the book's fields in string[]
	 * @return
	 */
	public static String[] getCatalog_fields() {
		return catalog_fields;
	}
	/**
	 * initialize fields
	 */
	public static void clearCatalogFields() 
	{
		catalog_fields_count = 0;
	}
	/**
	 * return specific element
	 * @param i - index of element
	 * @return
	 */
	public static String getCatalogField(int i) {
		return catalog_fields[i];
	}
	/**
	 * add element to book's field list
	 * @param str - field to add
	 * @return - true for add succefull or false for failure
	 */
	public static boolean add_catalog_field(String str)
	{
		/**
		 * exists: 0 - not in list
		 * 		   1 - found in list
		 */
		int i=0,exists=0;		
		while (i<catalog_fields_count)
		{		
			if (catalog_fields[i].equals(str))
			{
				exists = 1;
				System.out.println("Catalog Field already exists");
				i = catalog_fields_count;
			}
			i++;	
		}
		if (exists == 0)
		{
			catalog_fields[catalog_fields_count++] = str;
			return true;
		}
		else return false;
	}
	/**
	 * remove element from book's field list
	 * @param str - field to remove
	 * @return - true for remove succefull or false for failure
	 */
	public static boolean removeCatalogField(String str)
	{
		int i=0,j;
		while (i<catalog_fields_count)
		{
			if (catalog_fields[i].equals(str))
			{
				System.out.println("catalog field found");
				j=i+1;
				while (j < catalog_fields_count)
				{
					catalog_fields[i++] = catalog_fields[j++];
				}
				catalog_fields_count--;
				return true;
			}
			i++;
		}
		return false;	//not found
	}
	public static void setCatalog_fields_count(int catalog_fields_count) {
		CatalogBook.catalog_fields_count = catalog_fields_count;
	}
	public static int getCatalog_fields_count() {
		return catalog_fields_count;
	}
	
	/**
	 * ********************** Subject ***********************************
	 */
	/**
	 * set the book's fields in string[]
	 * @param catalog_fields
	 */
	public static void setCatalog_subjects(String[] catalog_subjects) {
		CatalogBook.catalog_subjects = catalog_subjects;
	}
	/**
	 * get the book's fields in string[]
	 * @return
	 */
	public static String[] getCatalog_subjects() {
		return catalog_subjects;
	}
	/**
	 * initialize fields
	 */
	public static void clearCatalogSubjects() 
	{
		catalog_subjects_count = 0;
	}
	/**
	 * return specific element
	 * @param i - index of element
	 * @return
	 */
	public static String getCatalogSubject(int i) {
		return catalog_subjects[i];
	}
	/**
	 * add element to book's subject list
	 * @param str - subject to add
	 * @return - true for add succefull or false for failure
	 */
	public static boolean addCatalogSubject(String str)
	{
		/**
		 * exists: 0 - not in list
		 * 		   1 - found in list
		 */
		int i=0,exists=0;		
		while (i<catalog_subjects_count)
		{		
			if (catalog_subjects[i].equals(str))
			{
				exists = 1;
				System.out.println("Catalog subject already exists");
				i = catalog_subjects_count;
			}
			i++;	
		}
		if (exists == 0)
		{
			catalog_subjects[catalog_subjects_count++] = str;
			return true;
		}
		else return false;
	}
	/**
	 * remove element from book's subject list
	 * @param str - subject to remove
	 * @return - true for remove succefull or false for failure
	 */
	public static boolean removeCatalogSubject(String str)
	{
		int i=0,j;
		while (i<catalog_subjects_count)
		{
			if (catalog_subjects[i].equals(str))
			{
				System.out.println("catalog subject found");
				j=i+1;
				while (j < catalog_subjects_count)
				{
					catalog_subjects[i++] = catalog_subjects[j++];
				}
				catalog_subjects_count--;
				return true;
			}
			i++;
		}
		return false;	//not found
	}
	
	
	public static void setCatalog_subjects_count(int catalog_subjects_count) {
		CatalogBook.catalog_subjects_count = catalog_subjects_count;
	}
	public static int getCatalog_subjects_count() {
		return catalog_subjects_count;
	}
	/**
	 * get number of object
	 * @return int
	 */
	public static int getCatalog_subject_count() 
	{
		return catalog_subjects_count;
	}
}
