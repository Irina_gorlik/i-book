package Gui;

import fit.ActionFixture;

public class AdvanceSearchBookTest extends ActionFixture {
	
	AdvanceSearch asb1 = null;
	
	public void startSearchForm() {
		asb1 = new AdvanceSearch(); 
		
	}


	public void choiceSet0(int Option) {
		asb1.choice0.select(Option);
	}
	public void choiceSet1(int Option) {
		asb1.choice1.select(Option);
	}
	public void choiceSet2(int Option) {
		asb1.choice2.select(Option);
	}
	public void choiceSet3(int Option) {
		asb1.choice3.select(Option);
	}
	public void choiceSet4(int Option) {
		asb1.choice4.select(Option);
	}
    
	public void searchField(String txt) {
		asb1.SearchField.setText(txt);
	}
	public void searchField1(String txt) {
		asb1.SearchField1.setText(txt);
	}
	public void searchField2(String txt) {
		asb1.SearchField2.setText(txt);
	}
	public void searchField3(String txt) {
		asb1.SearchField3.setText(txt);
	}
	public void searchField4(String txt) {
		asb1.SearchField4.setText(txt);
	}

	
	public void orSet1(boolean or) {
		asb1.jRadioOR1.setSelected(or);
	}
	public void orSet2(boolean or) {
		asb1.jRadioOR2.setSelected(or);
	}
	public void orSet3(boolean or) {
		asb1.jRadioOR3.setSelected(or);
	}
	public void orSet4(boolean or) {
		asb1.jRadioOR4.setSelected(or);
	}
	
	

	public void searchBook() {
		asb1.jButtonAdSearchActionPerformed();
	}
	
	public int resultTable () {
		return asb1.model.getRowCount();
	}
}
