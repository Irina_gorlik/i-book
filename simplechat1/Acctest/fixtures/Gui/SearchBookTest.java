package Gui;

import fit.ActionFixture;

public class SearchBookTest extends ActionFixture {
	
	SearchBook sb1 = null;
	
	public void startSearchForm() {
		sb1 = new SearchBook(); 
	}

	public void textField(String txt) {
		sb1.jTextField1.setText(txt);
	}

	public void choiceTitle(boolean Checked) {
		sb1.jCheckBox.setSelected(Checked);
	}

	public void choiceAuthor(boolean Checked) {
		sb1.jCheckBox1.setSelected(Checked);
	}
	public void choiceField(boolean Checked) {
		sb1.jCheckBox2.setSelected(Checked);
	}
	public void choiceKeyword(boolean Checked) {
		sb1.jCheckBox3.setSelected(Checked);
	}
	public void choiceSubject(boolean Checked) {
		sb1.jCheckBox4.setSelected(Checked);
	}
	public void choiceLanguage(boolean Checked) {
		sb1.jCheckBox5.setSelected(Checked);
	}
	public void choiceAll(boolean Checked) {
		sb1.jCheckBox6.setSelected(Checked);
	}
	

	public void searchBook() {
		sb1.jButton21ActionPerformed();
	}
	
	public int resultTable () {
		return sb1.model.getRowCount();
	}
}
