The project developed in java and MySql. 
It's a client/server program that allows registered users 
to search for books that are available on the server. 
A user can download books and read/leave reviews on books.